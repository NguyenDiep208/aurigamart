/* Theme JS */
jQuery(document).ready(function( $ ) {
    $('.same-shop .owl-carousel').owlCarousel({
        loop:true,
        margin: 10,
        nav: true,
        navText : ["<i class='far fa-chevron-left'></i>", "<i class='far fa-chevron-right'></i>"],
        dots: false,
        rewind: true,
        autoplay: false,
        autoplayTimeout: 3000,
        smartSpeed: 2500,
        autoplayHoverPause: true,
        responsive:{
            0:{
                items:2,
            },
            576:{
                items:3,
            },
            992:{
                items:4,
            },
            1400:{
                items:5,
            }
        }
    });

    $('.similar-pd .owl-carousel').owlCarousel({
        loop:true,
        margin: 10,
        nav: true,
        navText : ["<i class='far fa-chevron-left'></i>", "<i class='far fa-chevron-right'></i>"],
        dots: false,
        rewind: true,
        autoplay: false,
        autoplayTimeout: 3000,
        smartSpeed: 2500,
        autoplayHoverPause: true,
        responsive:{
            0:{
                items:2,
            },
            576:{
                items:3,
            },
            992:{
                items:4,
            },
            1400:{
                items:5,
            }
        }
    });

    $('.also-like .owl-carousel').owlCarousel({
        loop:true,
        margin: 10,
        nav: true,
        navText : ["<i class='far fa-chevron-left'></i>", "<i class='far fa-chevron-right'></i>"],
        dots: false,
        rewind: true,
        autoplay: false,
        autoplayTimeout: 3000,
        smartSpeed: 2500,
        autoplayHoverPause: true,
        responsive:{
            0:{
                items:2,
            },
            576:{
                items:3,
            },
            992:{
                items:4,
            },
            1400:{
                items:5,
            }
        }
    });

    // Carousel Brand
    $('.slider-brand .owl-carousel').owlCarousel({
        loop:true,
        margin: 0,
        nav: true,
        navText : ["<i class='far fa-chevron-left'></i>", "<i class='far fa-chevron-right'></i>"],
        dots: false,
        rewind: true,
        autoplay: false,
        autoplayTimeout: 3000,
        smartSpeed: 2500,
        autoplayHoverPause: true,
        responsive:{
            0:{
                items:2,
            },
            576:{
                items:4,
            },
            992:{
                items:5,
            },
            1400:{
                items:6,
            }
        }
    });

    $( document.body ).on( 'updated_checkout', function (data){
        var price_total = $('table.woocommerce-checkout-review-order-table tfoot tr.order-total span.amount').html();
        $('table.shop_table_responsive tr.order-total span.amount').html(price_total).show(400);
    });

    // woocommerce-ordering Orderby
    $( '.woocommerce-ordering' ).on( 'click', 'input[type="button"]', function() {
        var value = $(this).data('value');
        $('input[name="orderby"]').val( value);
        $( this ).closest( 'form' ).submit();
    });

    $( '.woocommerce-ordering' ).on( 'change', 'select.orderby-price', function() {
        var status = $('.orderby-options select.orderby-price option:selected').val();
        $('.woocommerce-ordering select.orderby-price').addClass('selected');
        $('input[name="orderby"]').val( status);
        $( this ).closest( 'form' ).submit();
    });


    // Off Canvas
    $('.ic-canvas').on('click', function(e){
        e.preventDefault();

        $('body').toggleClass('off-canvas menu-is-active left-is-active');
        $('html').toggleClass('fixed');
    });

    // Click close off-canvas
    $('.exit-offcanvas, .icon-close').on('click', function(e){
        e.preventDefault();

        $('body').removeClass('left-is-active');
        $('html').removeClass('fixed');

        //Slow background black remove
        setTimeout(function(){
            $('body').removeClass('off-canvas menu-is-active');
        },200);
    });

    // Share social
    $('.ic-share').each(function(index){
        $(this).click(function(){
            $(this).parent('.share-button-wrapper').toggleClass('show');
        });
    });

    // Login
    $('.icons-user').on('click', function(e){
        $('.drop-content').slideToggle('slow');
        $('.block-user').toggleClass('show');
    });

    $('.cart-item').hover(function(){
        $('.drop-content').slideUp('slow');
        $('.block-user').removeClass('show');
    });

    setTimeout(function(){
        $('.slide-home .product-small').css('height','100%');
        $('.also-like .product-small').css('height','100%');
    },3500);

    $('.price-sale >span.amount').closest('.price-sale').addClass('horizontal');

    // Number product count
    var numPro = $('.cart-icon strong').html();
    $('.cart-bottom .nav-top-link').after('<span class="num-product">'+numPro+'</span>');

    // Footer
    ft_content();

    $(window).resize(function() {
      ft_content();
    });

    function ft_content(){
        if ($(window).width() < 550) {
            $('.footer-widgets .widget').each(function(e){

                $(this).find('.widget-title').after('<i class="fal fa-chevron-down"></i>');

                $(this).click(function(){
                    $(this).find('.textwidget').slideToggle();
                    $(this).find('[class*="menu-footer-"]').slideToggle();
                });
            });
        }
    }

    if ($(window).width() < 768) {
        $('.menu-us-wrap .menu-item-has-children >a:first-child').after('<i class="fal fa-chevron-down"></i>');

        $('.menu-us-wrap >li').each(function(e){
            $(this).click(function(){
                $(this).find('.sub-menu').slideToggle();
            });
        });
    }
});

