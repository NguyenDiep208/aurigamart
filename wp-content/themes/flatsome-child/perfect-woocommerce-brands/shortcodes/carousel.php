<?php

/**
 * The template for displaying the carousels
 * @version 1.0.0
 */

defined('ABSPATH') or die('No script kiddies please!');
?>

<div class="pwb-carousel slider-brand">
	<div class="owl-carousel owl-theme">
	  	<?php 
	  	$postNumber = 1;
	  	$numItems = count($brands);

	  	foreach ($brands as $brand) : 
	  		if (($postNumber == 1) || (($postNumber-1) % 3 == 0)) {
		        echo '<div class="item">';
		    } ?>

			    <div class="ele-wrap">
			      <a href="<?php echo esc_url($brand['link']); ?>" title="<?php echo esc_html($brand['name']); ?>">
			        <?php echo wp_kses_post($brand['attachment_html']); ?>
			      </a>
			    </div>
		  	<?php 
		  	if ($postNumber % 3 == 0 || ($postNumber == 3) || ($postNumber == $numItems)) {
		        echo '</div>'; // close row tag
		    }

		    $postNumber++;

	  	endforeach; ?>
	</div>

  <div class="pwb-carousel-loader"><?php esc_html_e('Loading', 'perfect-woocommerce-brands'); ?>...</div>

</div>