<?php

//require get_theme_file_path() . '/assets/inc/our-post.php';

register_nav_menus(
	array(
		'footer-1' => __( 'Footer 1', 'aurigamart' ),
		'footer-2' => __( 'Footer 2', 'aurigamart' ),
		'footer-3' => __( 'Footer 3', 'aurigamart' ),
		'footer-4' => __( 'Footer 4', 'aurigamart' ),
		'footer-5' => __( 'Footer 5', 'aurigamart' ),
		'menu-canvas' => __( 'Menu Canvas', 'aurigamart' ),
		'menu-user' => __( 'Menu User', 'aurigamart' ),
		'menu-post' => __( 'Menu Post', 'aurigamart' ),
		'menu-us' => __( 'Menu Us', 'aurigamart' ),
	)
);

// Add Theme js

function aurigamart_register_scripts() {
	// Load fontawesome css
	wp_enqueue_style( 'awesome', get_stylesheet_directory_uri() . '/fonts/awesome/css/all.css', '5.7.0' );

	// Load Owl Carousel CSS
	wp_enqueue_style( 'owl-carousel-style', get_stylesheet_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '2.3.4' );
	wp_enqueue_style( 'owl-carousel-theme', get_stylesheet_directory_uri() . '/assets/css/owl.theme.default.min.css', array(), '2.3.4' );

	// Load custom css
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), '1.0.0' );

	wp_enqueue_script( 'bootstrap-script', get_stylesheet_directory_uri() . '/assets/scss/lib/bootstrap/js/bootstrap.min.js', array('jquery'));

	// Add Owl carousel js
	wp_enqueue_script( 'owl.carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '2.3.4', true );

	// Add Theme js
	wp_enqueue_script( 'theme-script', get_stylesheet_directory_uri() . '/js/theme.js', array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'aurigamart_register_scripts' );


// Short code selling product
function shortcode_selling_product() { ?>
	<div class="sell-product">
		<div class="slider-wrap">
			<div class="owl-carousel owl-theme">
			    <?php
				    $args = array(
				        'post_type' => 'product',
				        'meta_key' => 'total_sales',
				        'orderby' => 'meta_value_num',
				        'posts_per_page' => 10,
				    );

				    $loop = new WP_Query( $args );
				    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        $product = wc_get_product( get_the_ID());
                        wc_get_template_part('product-selling');
					endwhile;
                    wp_reset_postdata();
				?>
			</div>
		</div>
	</div>

<?php
}
add_shortcode( 'shc_deals_product', 'shortcode_deals_product' );

function shortcode_deals_product() { ?>
    <div class="sell-product">
        <div class="slider-wrap">
            <div class="owl-carousel owl-theme">
                <?php
                $args = array(
                    'post_type'      => 'product',
                    'posts_per_page' => 8,
                    'meta_query'     => array(
                        'relation' => 'OR',
                        array( // Simple products type
                               'key'           => '_sale_price',
                               'value'         => 0,
                               'compare'       => '>',
                               'type'          => 'numeric'
                        ),
                        array( // Variable products type
                               'key'           => '_min_variation_sale_price',
                               'value'         => 0,
                               'compare'       => '>',
                               'type'          => 'numeric'
                        )
                    )
                );

                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
                    global $product;
                    $product = wc_get_product( get_the_ID());
                    wc_get_template_part('product-selling');
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>

    <?php
}
add_shortcode( 'shc_selling_product', 'shortcode_selling_product' );


// Short code same product
function shortcode_same_product($product_id, $number) { ?>
	<div class="slider-wrap">
		<div class="owl-carousel owl-theme">
		    <?php
                $vendor_id = get_post_field( 'post_author', $product_id );
                $product_query = dokan()->product->all( [ 'author' => $vendor_id, 'posts_per_page' => $number ] );
			    while ( $product_query->have_posts() ) : $product_query->the_post();
                    global $product;
                    $product = wc_get_product( get_the_ID());
                    wc_get_template_part('product-item');
				endwhile;
                wp_reset_postdata();
			?>
		</div>
	</div>
<?php
}


// Short code similar product
function shortcode_single_product_similar($product_id,$number) { ?>
	<div class="single-product">
		<div class="slider-wrap">
			<div class="owl-carousel owl-theme">
			    <?php
                    $products = wc_get_related_products( $product_id, $number );
                    foreach ( $products as $product ) :
                        $post_object = get_post( $product );
                        setup_postdata( $GLOBALS['post'] =& $post_object );
                        wc_get_template_part('product-item');
                    endforeach;
                ?>
			</div>
		</div>
	</div>

<?php
}

// Short code upsell product
function shortcode_single_product_upsell($product_id, $number) { ?>
    <div class="single-product">
        <div class="slider-wrap">
            <div class="owl-carousel owl-theme">
                <?php
                $terms = get_the_terms ( $product_id, 'product_cat' );
                $cat_ids = [];

                foreach ( $terms as $term ) {
                    $cat_ids[] = $term->term_id;
                }

                $tax_query = array(
                     array(
                         'taxonomy'      => 'product_cat',
                         'field'        => 'term_id', //This is optional, as it defaults to 'term_id'
                         'terms'         => $cat_ids,
                         'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                     )
                );

                // The query
                $query = new WP_Query( array(
                    'post_type'           => 'product',
                    'post_status'         => 'publish',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page'      => $number,
                    'tax_query'           => $tax_query,
                    'post__in'            => wc_get_featured_product_ids(),
                ) );

                while ( $query->have_posts() ) : $query->the_post();
                    global $product;
                    $product = wc_get_product( get_the_ID());
                    wc_get_template_part('product-item');
                endwhile;
                wp_reset_postdata();

                ?>
            </div>
        </div>
    </div>
 <?php
}


/*Share Link Social*/
function social_buttons($args) {
	?>
    <div class="share-button-wrapper">
    	<div class="ic-share"><i class="fas fa-share-alt"></i></div>
	    	<div class="wrap-ic">
	        <a target="_blank" class="share-button share-twitter" href="https://twitter.com/intent/tweet?url=<?php echo $args['url']; ?>&text=<?php $args['title']; ?>&via=<?php the_author_meta( 'twitter' ); ?>" title="Tweet this"><i class="fab fa-twitter"></i></a>
	        <a target="_blank" class="share-button share-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $args['url']; ?>" title="Share on Facebook"><i class="fab fa-facebook"></i></a>
	    </div>
    </div>
    <?php
};
add_shortcode('share-social','social_buttons');

// Code Off Canvas
function code_offcanvas() {
?>
<div class="block-canvas">
	<a class="exit-offcanvas"></a>
	<div class="wrap-canvas">
		<div class="wrap-ct">
			<div class="icon-close">
				<i class="fal fa-times"></i>
			</div>

			<div class="head-top">
				<div class="ic-user"><i class="fas fa-user"></i></div>
				<div class="ct-right">
                    <?php if (!is_user_logged_in()):?>
                    	<h4>Hello, <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Login</a></h4>
                    	<div class="link-sign">New? <a href="<?php echo get_permalink(get_page_by_path( 'register' )); ?>"><?php esc_html_e('Sign up', 'woocommerce');?></a></div>
                    <?php else: ?>
                        <?php
                            $user = wp_get_current_user();
                        ?>
                        <h4>Hello, <?php echo esc_html($user->display_name); ?></h4>
                        <span><a title="<?php esc_html_e('Log out', 'flatsome'); ?>" class="tips"
                                 href="<?php echo esc_url(wp_logout_url(home_url())); ?>"><?php esc_html_e('Log Out', 'flatsome'); ?></a></span>
                    <?php endif; ?>
				</div>
			</div>
				<div class="wrap-menu">
					<?php
					   	wp_nav_menu([
							//'menu'            => 'menu-canvas',
							'theme_location' => 'menu-canvas',
							'container'       => 'div',
							'container_id'    => 'navbar-block',
							'container_class' => 'menu-canvas',
							'menu_id'         => true,
							'menu_class'      => 'menu-canvas-wrap'
					   	]);
					?>
				</div>
		</div>
	</div>
</div>
<?php
}
add_action('wp_body_open', 'code_offcanvas');

remove_action( 'woocommerce_register_form', 'dokan_seller_reg_form_fields' );
remove_action('wp_footer', 'flatsome_account_login_lightbox', 10);

add_action('wp_footer', 'aurigamart_account_login_lightbox', 10);

function aurigamart_account_login_lightbox(){
    // Show Login Lightbox if selected
    if ( !is_user_logged_in() && get_theme_mod('account_login_style','lightbox') == 'lightbox' && !is_checkout() && !is_account_page() ) {
        $is_facebook_login = is_nextend_facebook_login();
        $is_google_login = is_nextend_google_login();

        if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) {
            wp_enqueue_script( 'wc-password-strength-meter' );
        }

        ?>
        <div id="login-form-popup" class="lightbox-content mfp-hide">
            <?php if(get_theme_mod('social_login_pos','top') == 'top' && ($is_facebook_login || $is_google_login)) wc_get_template('myaccount/header.php'); ?>
            <?php wc_get_template_part('myaccount/login-form'); ?>
            <?php if(get_theme_mod('social_login_pos','top') == 'bottom' && ($is_facebook_login || $is_google_login)) wc_get_template('myaccount/header.php'); ?>
        </div>
    <?php }
}
add_action('wp_footer', 'flatsome_account_login_lightbox', 10);

// Code Favorite single product
function add_to_wishlist_button() {
	global $product;

	if ( ! $product instanceof WC_Product ) {
		return;
	}

	printf(
		'<a href="%s" class="button"><i class="fa fa-heart"></i>%s (%s)</a>',
		esc_url( add_query_arg( 'add_to_wishlist', $product->get_id() ) ),
		esc_html__( 'Favorite', 'aurigamart' ),
        yith_wcwl_count_add_to_wishlist($product->get_id())
	);
}


function aurigamart_get_max_off_sale_product(){
    global $product;
    if($product->is_type( 'variable' )) {
        $prices      = $product->get_variation_prices(true);
        $max_saleoff = 0;
        $id_max_sale = '';

        foreach ($prices['regular_price'] as $id => $regular_price) {
            foreach ($prices['sale_price'] as $product_id => $sale_price) {
                if ($id == $product_id) {
                    $saleoff = (($regular_price - $sale_price) / $regular_price * 100);
                    if ($saleoff > $max_saleoff) {
                        $max_saleoff = $saleoff;
                        $id_max_sale = $product_id;
                    }
                }
            }
        }

        return round(($prices['regular_price'][$id_max_sale] - $prices['sale_price'][$id_max_sale]) / $prices['regular_price'][$id_max_sale] * 100, 0);
    }elseif($product->is_type( 'simple' ) || $product->is_type( 'external' )){
        $regular_price = (int) $product->get_regular_price(); // Regular price
        $sale_price = $product->get_sale_price();

        if($product->is_on_sale()):
            return round( ( $regular_price - $sale_price ) / $regular_price * 100, 0 );
        endif;
    }
}

// Number Star ratings
if (!function_exists('display_ratings_reviews')) {
	function display_ratings_reviews(){
		global $product;

		//Pagination setup
		$reviews_per_page = 10;
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$offset = ($paged - 1) * $reviews_per_page;

		$args = array(
			'status' => 'approve',
			'type' => 'review',
			'post_id' => $product->get_id(),
			'number' => $reviews_per_page,
			'offset' => $offset,
		);

		// The Query
		$comments_query = new WP_Comment_Query;
		$comments = $comments_query->query( $args );

		// Comment Loop
		if ( $comments ) {
			echo "<ol>";
			foreach ( $comments as $comment ):
				$num_star = esc_attr( get_comment_meta( $comment->comment_ID, 'rating', true ) );
				?>


			<?php if ( $comment->comment_approved == '0') : ?>
				<p class="meta waiting-approval-info">
					<em><?php _e( 'Thanks, your review is awaiting approval', 'woocommerce' ); ?></em>
				</p>
			<?php endif;  ?>

			<?php if ($num_star != '') : ?>
				<li itemprop="reviews" itemscope itemtype="http://schema.org/Review" <?php comment_class(); ?> id="li-review-<?php echo $comment->comment_ID; ?>">
					<div id="review-<?php echo $comment->comment_ID; ?>" class="review_container">
						<div class="review-avatar">
							<?php echo get_avatar( $comment->comment_author_email, $size = '50' ); ?>
						</div>
						<div class="review-author">
							<div class="review-author-name" itemprop="author"><?php echo $comment->comment_author; ?></div>
							<div class='star-rating-container'>
								<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="star-rating" title="<?php echo esc_attr( get_comment_meta( $comment->comment_ID, 'rating', true ) ); ?>">
									<span style="width:<?php echo get_comment_meta( $comment->comment_ID, 'rating', true )*20; ?>%"><span itemprop="ratingValue"><?php echo get_comment_meta( $comment->comment_ID, 'rating', true ); ?></span> <?php _e('out of 5', 'woocommerce'); ?></span>

										<?php
											$timestamp = strtotime( $comment->comment_date ); //Changing comment time to timestamp
											$date = date('F d, Y', $timestamp);
										?>
								</div>
								<em class="review-date">
									<time itemprop="datePublished" datetime="<?php echo $comment->comment_date; ?>"><?php echo $date; ?></time>
								</em>
							</div>
						</div>
						<div class="clear"></div>
						<div class="review-text">
							<div itemprop="description" class="description">
								<?php echo $comment->comment_content; ?>
							</div>

							<div class="reply comment-child">
						        <?php
						        	$cm_child =  $comment->get_children();

						        	if ($cm_child):
									    foreach ($cm_child as $ct_child) :?>
									    	<div class="review-author-name" itemprop="author"><?php echo $ct_child->comment_author; ?></div>
									    	<div itemprop="description" class="description">
												<?php echo $ct_child->comment_content; ?>
											</div>
									   	<?php endforeach;
									endif;
						        ?>
						    </div>
							<div class="clear"></div>
						</div>
					<div class="clear"></div>
				</div>
			</li>

			<?php
			endif;
			endforeach;
			echo "</ol>";
			//Pagination output
			echo "<div class='navigation'>";
				$all_approved_product_review_count = get_comments(array(
					'status'   => 'approve',
					'type' => 'review',
					'count' => true
				));
				if( is_float($all_approved_product_review_count / $reviews_per_page) ){
					//In case the value is float, we need to make sure there is an additional page for the final results
					$additional = 1;
				}else{
					$additional = 0;
				}
				$max_num_pages = intval( $all_approved_product_review_count / $reviews_per_page ) + $additional;
				$current_page = max(1, get_query_var('paged'));

				echo paginate_links(array(
					'total' => $max_num_pages
				));
			echo "</div>";

		} else {
			echo "This product hasn't been rated yet.";
		}
	}
}

function dokan_single_product_info(){
    wc_get_template_part('dokan-info');
}


// Short Code Login Header
function shortcode_login() { ?>
<div class="block-user">
	<div class="icons-user">
		<div class="ic-user"><i class="far fa-user"></i></div>
		<div class="ic-down"><i class="fas fa-chevron-down"></i></div>
		<div class="ic-close"><i class="fas fa-times"></i></div>
	</div>

	<div class="drop-content">
		<div class="wrap-ct">
			<div class="head-top <?php echo is_user_logged_in()?'sign-in':'sign-up'; ?>">
				<div class="ct-wrap">
                    <?php if (!is_user_logged_in()):?>
                        <?php wc_get_template_part('myaccount/login-form'); ?>
                    <?php else: ?>
                        <?php
                        $user = wp_get_current_user();
                        ?>
                        <h4>Hello, <?php echo esc_html($user->display_name); ?></h4>
                        <span><a title="<?php esc_html_e('Log out', 'flatsome'); ?>" class="tips"
                                 href="<?php echo esc_url(wp_logout_url(home_url())); ?>"><?php esc_html_e('Log Out', 'flatsome'); ?></a></span>
                    <?php endif; ?>
				</div>
			</div>

            <?php if (is_user_logged_in()):?>
                <div class="txt-info">
                    <p>Aurigamart Rewards Benefits</p>
                </div>
                <div class="wrap-menu">
                    <?php
                        wp_nav_menu([
                            //'menu'            => 'menu-canvas',
                            'theme_location' => 'menu-user',
                            'container'       => 'div',
                            'container_id'    => 'navbar-block',
                            'container_class' => 'menu-user',
                            'menu_id'         => true,
                            'menu_class'      => 'menu-user-wrap'
                        ]);
                    ?>
                </div>
            <?php endif; ?>
		</div>
	</div>
</div>
<?php
}
add_shortcode('shc_login', 'shortcode_login');


function dokan_products_upsell($number, $user_id){
    $products = dokan_get_featured_products($number,$user_id );

    if($products->have_posts()):
?>
 	<div class="also-like">
        <div class="title-head">
            <div class="title-txt">
                <h3>You may also like</h3>
            </div>
        </div>
        <div class="list-product single-product">
        	<div class="products large-columns-4 medium-columns-3 small-columns-2">
			    <div class="dokan-product">
			        <?php
			            while ( $products->have_posts() ) : $products->the_post();
			                global $product;
			                $product = wc_get_product( get_the_ID());
			                echo '<div class="col-inner">';
			                    wc_get_template_part('product-item');
			                echo '</div>';
			            endwhile;
			            wp_reset_postdata();
			        ?>
			    </div>
			</div>
    	</div>
    </div>
<?php
	endif;
}


function dokan_products_best_selling($number, $user_id){
    $products = dokan_get_best_selling_products($number,$user_id );

    if($products->have_posts()):
?>
<div class="best-selling">
    <div class="title-head">
        <div class="title-txt">
            <h3>Best Selling</h3>
        </div>
    </div>
    <div class="list-product single-product">
    	<div class="products large-columns-4 medium-columns-3 small-columns-2">
		    <div class="dokan-product col">
		        <?php
		            while ( $products->have_posts() ) : $products->the_post();
		                global $product;
		                $product = wc_get_product( get_the_ID());
		                echo '<div class="col-inner">';
		                    wc_get_template_part('product-item');
		                echo '</div>';
		            endwhile;
		            wp_reset_postdata();
		        ?>
		    </div>
		</div>
    </div>
</div>

    <?php
    endif;
}


add_shortcode('shc_register_form', 'woocommerce_form_register_shortcode');
function woocommerce_form_register_shortcode(){
     wc_get_template_part('myaccount/register-form');
}

add_shortcode('shc_register_vendor', 'woocommerce_form_register_shortcode_vendor');
function woocommerce_form_register_shortcode_vendor(){
     wc_get_template_part('myaccount/vendor-register');
}


function dokan_custom_seller_field_form() {
    $postdata   = wc_clean( $_POST ); //phpcs:ignore
    $role       = isset( $postdata['role'] ) ? $postdata['role'] : 'customer';
    $role_style = ( $role === 'customer' ) ? 'display:none' : '';

    dokan_get_template_part(
        'seller-registration-form', '', array(
			'postdata' => $postdata,
			'role' => $role,
			'role_style' => $role_style,
        )
    );
}

function dokan_live_chat_button_product_page() {
    
    $product_id = get_the_ID();
    $seller_id  = get_post_field( 'post_author', $product_id );
    $store      = dokan()->vendor->get( $seller_id )->get_shop_info();
    $page_id = ! empty( $store['fb_page_id'] ) ? $store['fb_page_id'] : '';

    // if ( empty( $store['live_chat'] ) || $store['live_chat'] !== 'yes' || ! $page_id ) {
    //     return;
    // }

    echo sprintf( '<button style="margin-left: 5px;" class="dokan-live-chat dokan-live-chat-messenger button alt">%s</button>', __( 'Chat Now', 'dokan' ) );
    echo do_shortcode( sprintf( '[dokan-live-chat-messenger page_id="%s"]', $page_id ) );
}


function dokan_render_live_chat_button( $vendor_id ) {
   
    $store   = dokan()->vendor->get( $vendor_id )->get_shop_info();
    $page_id = ! empty( $store['fb_page_id'] ) ? $store['fb_page_id'] : '';

    if ( empty( $store['live_chat'] ) || $store['live_chat'] !== 'yes' || ! $page_id ) {
        return;
    }
    ?>
    <li class="dokan-store-support-btn-wrap dokan-right">
        <button class="dokan-btn dokan-btn-theme dokan-btn-sm dokan-live-chat dokan-live-chat-messenger" style="position: relative; top: 3px">
            <?php echo esc_html__( 'Chat Now', 'dokan' ); ?>
        </button>
        <?php echo do_shortcode( sprintf( '[dokan-live-chat-messenger page_id="%s"]', $page_id ) ); ?>
    </li>
    <?php
}

function dokan_add_follow_button_after_store_tabs( $vendor_id ) {
    $vendor = dokan()->vendor->get( $vendor_id );

    ob_start();
    Dokan_Follow_Store_Follow_Button::add_follow_button( $vendor->data, array( 'dokan-btn-sm' ) );
    $button = ob_get_clean();

    $args = array(
        'button' => $button,
    );

    dokan_follow_store_get_template( 'follow-button-after-store-tabs', $args );
}


function custom_mycurrentlevel() {
	$user_ID = get_current_user_ID();
	if ( isset( $user_ID ) && ! empty( $user_ID ) ) {
		$user_level = get_user_meta( $user_ID, 'membership_level', true );
		if ( isset( $user_level ) && ! empty( $user_level ) ) {
			return $user_level.' Member';
		}else{
			return 'Basic Member';
		}
	}
}

function aurigamart_cart_link() {?>
    <a class="cart-contents" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php esc_attr_e('View cart', 'flatsome'); ?>">
        <span class="count"><?php echo wp_kses_data(sprintf(_n('%d', '%d', WC()->cart->get_cart_contents_count(), 'flatsome'), WC()->cart->get_cart_contents_count())); ?></span>
        <i class="icon-cart"></i>Cart
  	</a>
<?php
}


function aurigamart_cart_link_fragment($fragments) {
    ob_start();
    aurigamart_cart_link();
    $fragments['a.cart-contents'] = ob_get_clean();
    return $fragments;
}
add_filter('woocommerce_add_to_cart_fragments', 'aurigamart_cart_link_fragment');

function aurigamart_setup() {
	register_nav_menus( array(
		'my_order'     => __( 'My Order Menu', 'flatsome' ),
	) );
}
add_action( 'after_setup_theme', 'aurigamart_setup' );


function aurigamart_widgets_init() {
	$title_after  = '<div class="is-divider small"></div>';

	register_sidebar( array(
		'name'          => __( 'Recently viewed products', 'flatsome' ),
		'id'            => 'recently-viewed',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<span class="widget-title ' . $title_class . '"><span>',
		'after_title'   => '</span></span>',
	) );
}
add_action( 'widgets_init', 'aurigamart_widgets_init' );

function aurigamart_fnc_products_upsell_cart(){
?>
 	<div class="also-like">
        <div class="title-head">
            <div class="title-txt">
                <h3>You may also like</h3>
            </div>
        </div>
        <div class="list-product single-product">
        	<div class="products large-columns-4 medium-columns-3 small-columns-2">
			    <div class="dokan-product">
			        <?php
			           echo do_shortcode('[ux_featured_products products="10" columns="6"]');
			        ?>
			    </div>
			</div>
    	</div>
    </div>
<?php
}

function aurigamart_fnc_get_product_cart_by_vendor($carts){
	$data = [];
	foreach ($carts as $cart) {
		$vendor_id = get_post_field( 'post_author', $cart['product_id'] );
		$data[$vendor_id][] = $cart;
	}
	return $data;
}

//add_filter('mwb_wpr_allowed_user_roles_points_features', 'aurigamart_allowed_user_roles_points_features', 10, 1);
function aurigamart_allowed_user_roles_points_features($value){
	$value = true;
	return $value;
}

function aurigamart_override_checkout_fields( $fields ) {

	$fields['billing']['billing_address_1']['label'] = '<i class="far fa-map-marker"></i>';
	$fields['billing']['billing_address_1']['placeholder'] = 'Address';

	unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_state']);	
	
	unset($fields['billing']['billing_city']);
	
	unset($fields['billing']['billing_email']);
	unset($fields['billing']['billing_postcode']);

	$fields['billing']['billing_first_name']['label'] = '<i class="far fa-user"></i>';
	$fields['billing']['billing_first_name']['placeholder'] = 'Full name';

	$fields['billing']['billing_phone']['label'] = '<i class="far fa-phone"></i>';
	$fields['billing']['billing_phone']['placeholder'] = 'Phone number';
	
    return $fields;	
}
add_filter( 'woocommerce_checkout_fields' , 'aurigamart_override_checkout_fields', 1000, 1 );

remove_action( 'woocommerce_checkout_terms_and_conditions', 'flatsome_terms_and_conditions' );


// Code Off Canvas
function add_menu_us() {
?>
<div class="menu-us">
	<?php
	   	wp_nav_menu([
			//'menu'            => 'menu-canvas',
			'theme_location' => 'menu-us',
			'container'       => 'div',
			'container_id'    => 'navbar-block',
			'container_class' => 'menu-canvas',
			'menu_id'         => true,
			'menu_class'      => 'menu-us-wrap'
	   	]);
	?>
</div>
<?php
}
add_shortcode('menu-us', 'add_menu_us');

// Code Off Canvas
function list_post() {
?>
<div class="list_post">
	<div id="wrap-content-site" class="row large-columns-2 medium-columns-2 small-columns-2 row-small">
			<?php
				$args = array(
				    'posts_per_page' => 4,
				    'order' => 'DESC',
				    'orderby'=> 'post_date', 
				);

				$the_query  = new WP_Query( $args );
			?>

				<?php if ( $the_query ->have_posts() ) : ?>
					<?php while ( $the_query ->have_posts() ) : $the_query ->the_post(); ?>
						<div class="col">
							<!-- Picture -->
		            		<div class="pic-article">
		            			<?php
		            				echo get_the_post_thumbnail( $post->ID ,'full');
			                	?>
		            		</div>

							<div class="body-item">
								<div class="date-article"><?php echo get_the_date( 'F j, Y' ); ?></div>

			            		<!-- Title -->
			        			<div class="post-title">
			            			<h4><a href="<?php the_permalink($post->ID); ?>"><?php echo roadlimitStringByWord(get_the_title($post->ID), 60,'...'); ?></a></h4>
			            		</div>

			            		<div class="link-view">
			            			<a href="<?php the_permalink($post->ID); ?>"><?php echo "View post >"; ?></a>
			            		</div>
							</div>
						</div>
					<?php endwhile; ?>
					

					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
	</div>
</div>
<?php
}
add_shortcode('list-post', 'list_post');

function roadlimitStringByWord ($string, $maxlength, $suffix = '') {
	if(function_exists( 'mb_strlen' )) {
		// use multibyte functions by Iysov
		if(mb_strlen( $string )<=$maxlength) return $string;
		$string = mb_substr( $string, 0, $maxlength );
		$index = mb_strrpos( $string, ' ' );
		if($index === FALSE) {
			return $string;
		} else {
			return mb_substr( $string, 0, $index ).$suffix;
		}
	} else { // original code here
		if(strlen( $string )<=$maxlength) return $string;
		$string = substr( $string, 0, $maxlength );
		$index = strrpos( $string, ' ' );
		if($index === FALSE) {
			return $string;
		} else {
			return substr( $string, 0, $index ).$suffix;
		}
	}
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );