<?php
$store_user               = dokan()->vendor->get( get_query_var( 'author' ) );
$store_info               = $store_user->get_shop_info();
$social_info              = $store_user->get_social_profiles();
$store_tabs               = dokan_get_store_tabs( $store_user->get_id() );
$social_fields            = dokan_get_social_profile_fields();

$dokan_appearance         = get_option( 'dokan_appearance' );
$profile_layout           = empty( $dokan_appearance['store_header_template'] ) ? 'default' : $dokan_appearance['store_header_template'];
$store_address            = dokan_get_seller_short_address( $store_user->get_id(), false );

$dokan_store_time_enabled = isset( $store_info['dokan_store_time_enabled'] ) ? $store_info['dokan_store_time_enabled'] : '';
$store_open_notice        = isset( $store_info['dokan_store_open_notice'] ) && ! empty( $store_info['dokan_store_open_notice'] ) ? $store_info['dokan_store_open_notice'] : __( 'Store Open', 'dokan-lite' );
$store_closed_notice      = isset( $store_info['dokan_store_close_notice'] ) && ! empty( $store_info['dokan_store_close_notice'] ) ? $store_info['dokan_store_close_notice'] : __( 'Store Closed', 'dokan-lite' );
$show_store_open_close    = dokan_get_option( 'store_open_close', 'dokan_appearance', 'on' );

$general_settings         = get_option( 'dokan_general', [] );
$banner_width             = dokan_get_option( 'store_banner_width', 'dokan_appearance', 625 );

if ( ( 'default' === $profile_layout ) || ( 'layout2' === $profile_layout ) ) {
    $profile_img_class = 'profile-img-circle';
} else {
    $profile_img_class = 'profile-img-square';
}

if ( 'layout3' === $profile_layout ) {
    unset( $store_info['banner'] );

    $no_banner_class      = ' profile-frame-no-banner';
    $no_banner_class_tabs = ' dokan-store-tabs-no-banner';

} else {
    $no_banner_class      = '';
    $no_banner_class_tabs = '';
}
$ratings = $store_user->get_rating();
$product_views = $store_user->get_product_views();
$total_sales = $store_user->get_total_sales();

if ( $store_user->get_banner() ) {
    $style_baner = 'style="background-image: url('. esc_url( $store_user->get_banner() ) .');"';
}else{
    $style_baner = '';
}
?>
<div class="dokan-profile-frame-wrapper">
    <div class="profile-frame<?php echo esc_attr( $no_banner_class ); ?>">

        <div class="profile-info-box profile-layout-<?php echo esc_attr( $profile_layout ); ?>">
            <div class="profile-info-summery-wrapper dokan-clearfix">
                <div class="profile-info-summery">
                    <div class="seller-leading">
                        <div class="banner-shop" <?php echo $style_baner; ?>></div>

                        <div class="profile-img <?php echo esc_attr( $profile_img_class ); ?>">
                            <div class="logo-shop">
                                <?php if( $store_user->get_avatar()): ?>
                                    <img src="<?php echo esc_url( $store_user->get_avatar() ) ?>" alt="<?php echo esc_attr( $store_user->get_shop_name() ) ?>" size="150">
                                <?php else: ?>
                                     <img src="<?php echo wc_placeholder_img_src(); ?>" alt="<?php echo esc_attr( $store_user->get_shop_name() ) ?>" size="150">
                                <?php endif; ?>
                            </div>
                            <div class="shop-info">
                                <h4 class="store-name"><?php echo esc_html( $store_user->get_shop_name() ); ?></h4>
                                <!-- <?php if ( ! empty( $store_user->get_shop_name() ) && 'default' === $profile_layout ) { ?>
                                    <h1 class="store-name"><?php echo esc_html( $store_user->get_shop_name() ); ?></h1>
                                <?php } ?> -->
                            </div>

                            <ul class="link-store">
                                <?php dokan_render_live_chat_button($store_user->get_id()); ?>
                                <?php dokan_add_follow_button_after_store_tabs($store_user->get_id()); ?>
                            </ul>
                        </div>
                    </div>

                    <div class="profile-info">
                        <ul class="dokan-store-info">

                            <?php if ( ! dokan_is_vendor_info_hidden( 'address' ) && isset( $store_address ) && !empty( $store_address ) ) { ?>
                                <li class="first-col">
                                    <div class="dokan-store-address"><i class="fal fa-map-marker-alt"></i>
                                        <?php echo wp_kses_post( $store_address ); ?>
                                    </div>
                                </li>
                            <?php } ?>
                            <li class="second-col">
                                <?php if ( ! dokan_is_vendor_info_hidden( 'phone' ) && ! empty( $store_user->get_phone() ) ) { ?>
                                    <div class="dokan-store-phone">
                                        <div class="section-seller-overview__item-text-name"><i class="fal fa-phone-square-alt"></i> Phone number:&nbsp;</div>
                                        <a href="tel:<?php echo esc_html( $store_user->get_phone() ); ?>"><span class="txt-info"><?php echo esc_html( $store_user->get_phone() ); ?></span></a>
                                    </div>
                                <?php } ?>

                                <div class="dokan-store-rating">
                                    <div class="section-seller-overview__item-text-name"><i class="fal fa-star"></i> Rating:&nbsp;</div>
                                    <span class="txt-info"><?php echo $ratings['rating'].'  ('.$ratings['count'].' Rating)'; ?></span>
                                </div>

                                <div class="dokan-store-open-close">
                                    <div class="section-seller-overview__item-text-name"><i class="fal fa-calendar-check"></i> Joined:&nbsp;</div>
                                    <span class="txt-info"><?php echo human_time_diff( strtotime($store_user->get_register_date()), current_time( 'timestamp' ) ).' '.__( 'ago' ); ?></span>
                                </div>
                            </li>

                            <li class="second-col">
                                <div class="dokan-store-products">
                                    <div class="section-seller-overview__item-text-name"><i class="fal fa-cart-plus"></i> Products:&nbsp;</div>
                                    <span class="txt-info"><?php echo $store_user->get_products()->post_count; ?></span>
                                </div>

                                <div class="dokan-store-total-sales">
                                    <div class="section-seller-overview__item-text-name"><i class="fal fa-store"></i> Total sales:&nbsp;</div>
                                    <span class="txt-info"><?php echo ($total_sales !=0) ? $total_sales : 0; ?></span>
                                </div>

                                <div class="dokan-store-product-views">
                                    <div class="section-seller-overview__item-text-name"><i class="fal fa-user-chart"></i> Total views:&nbsp;</div>
                                    <span class="txt-info"><?php echo ($product_views !=0) ? $product_views : 0; ?></span>
                                </div>
                            </li>

                            <?php do_action( 'dokan_store_header_info_fields',  $store_user->get_id() ); ?>
                        </ul>

                        <?php if ( $social_fields ) { ?>
                            <div class="store-social-wrapper">
                                <ul class="store-social">
                                    <?php foreach( $social_fields as $key => $field ) { ?>
                                        <?php if ( !empty( $social_info[ $key ] ) ) { ?>
                                            <li>
                                                <a href="<?php echo esc_url( $social_info[ $key ] ); ?>" target="_blank"><i class="fa fa-<?php echo esc_attr( $field['icon'] ); ?>"></i></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>

                    </div> <!-- .profile-info -->
                </div><!-- .profile-info-summery -->
            </div><!-- .profile-info-summery-wrapper -->
        </div> <!-- .profile-info-box -->
    </div> <!-- .profile-frame -->
</div>
