<?php

$store_user  = dokan()->vendor->get( get_query_var( 'author' ) );
$store_info  = $store_user->get_shop_info();

?>

<div class="shop-page__section">
	<div class="title-head">
        <div class="title-txt">
            <h3>ABOUT SHOP</h3>
        </div>
    </div>
    <div class="shop-banner__content">
    	<div class="shop-banner <?php echo (!$store_info['vendor_biography'])? 'full-banner' : ''; ?>">
			 <?php if ( $store_user->get_banner() ) { ?>
	            <img src="<?php echo esc_url( $store_user->get_banner() ); ?>"
	                alt="<?php echo esc_attr( $store_user->get_shop_name() ); ?>"
	                title="<?php echo esc_attr( $store_user->get_shop_name() ); ?>"
	                class="profile-info-img">
	        <?php } else { ?>
	            <div class="profile-info-img dummy-image"><img src="<?php echo wc_placeholder_img_src(); ?>" alt="<?php echo esc_attr( $store_user->get_shop_name() ) ?>"></div>
	        <?php } ?>
    	</div>
    	<?php if($store_info['vendor_biography']): ?>
	    	<div class="shop-page-shop-description">
	    		<?php echo $store_info['vendor_biography']; ?>
	    	</div>
    	<?php endif; ?>
    </div>
</div>