<?php
/**
 * The Template for displaying all single posts.
 *
 * @package dokan
 * @package dokan - 2014 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$store_user   = dokan()->vendor->get( get_query_var( 'author' ) );
$store_info   = $store_user->get_shop_info();
$map_location = $store_user->get_location();
$layout       = get_theme_mod( 'store_layout', 'left' );

get_header( 'shop' );

if ( function_exists( 'yoast_breadcrumb' ) ) {
    yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
}
?>
<?php do_action( 'woocommerce_before_main_content' ); ?>

<div class="dokan-store-wrap layout-<?php echo esc_attr( $layout ); ?>">



    <div id="dokan-primary" class="dokan-single-store">
        <div id="dokan-content" class="store-page-wrap woocommerce" role="main">

            <?php dokan_get_template_part( 'store-header' ); ?>

            <?php dokan_get_template_part( 'store-description' ); ?>

            <?php do_action( 'dokan_store_profile_frame_after', $store_user->data, $store_info ); ?>

            <!-- You may also like -->
            <?php dokan_products_upsell(8, $store_user->get_id()); ?>

            <!-- Best Selling -->
            <?php dokan_products_best_selling(8, $store_user->get_id()); ?>
            </div>
            <div class="products-data row">
                <div class="left-sidebar col medium-3 small-12 large-2">

                    <?php dokan_store_category_widget(); ?>

                </div>
                <div class="right-sidebar col medium-9 small-12 large-10">

                    <?php if ( have_posts() ) { ?>

                        <div class="seller-items">

                            <div class="products row row-small large-columns-5 medium-columns-2 small-columns-2">

                            <?php while ( have_posts() ) : the_post(); ?>

                                <?php wc_get_template_part( 'content', 'product' ); ?>

                            <?php endwhile; // end of the loop. ?>

                            </div>

                        </div>

                        <?php dokan_content_nav( 'nav-below' ); ?>

                    <?php } else { ?>

                        <p class="dokan-info"><?php esc_html_e( 'No products were found of this vendor!', 'dokan-lite' ); ?></p>

                    <?php } ?>

                </div>
            </div>
        </div>

    </div><!-- .dokan-single-store -->

</div><!-- .dokan-store-wrap -->

<?php do_action( 'woocommerce_after_main_content' ); ?>

<?php get_footer( 'shop' ); ?>
