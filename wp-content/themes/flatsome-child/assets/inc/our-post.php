<?php
add_action('init', 'aurigamart_our_post');
function aurigamart_our_post(){
	$labels = array(
		'name'               => _x( 'Our Post', 'post type general name', 'aurigamart' ),
		'singular_name'      => _x( 'Our Post', 'post type singular name', 'aurigamart' ),
		'menu_name'          => _x( 'Our Post', 'admin menu', 'aurigamart' ),
		'name_admin_bar'     => _x( 'Our Post', 'add new on admin bar', 'aurigamart' ),
		'add_new'            => _x( 'Add New', 'Our Post', 'aurigamart' ),
		'add_new_item'       => __( 'Add New Our Post', 'aurigamart' ),
		'new_item'           => __( 'New Our Post', 'aurigamart' ),
		'edit_item'          => __( 'Edit Our Post', 'aurigamart' ),
		'view_item'          => __( 'View Our Post', 'aurigamart' ),
		'all_items'          => __( 'Our Post', 'aurigamart' ),
		'search_items'       => __( 'Search Our Posts', 'aurigamart' ),
		'parent_item_colon'  => __( 'Parent Our Post:', 'aurigamart' ),
		'not_found'          => __( 'No Our Post found.', 'aurigamart' ),
		'not_found_in_trash' => __( 'No Our Post found in Trash.', 'aurigamart' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'aurigamart' ),
        'supports' => array('title','editor','author','excerpt','comments','revisions'),
        'taxonomies' => array( 'category', 'post_tag' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		//'rewrite'            => array( 'slug' => 'book','with_front' => false ),
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'has_archive' => true,
		'supports'           => array( 'title', 'editor', 'thumbnail')
	);

	register_post_type( 'our_post', $args );


	$labels = array(
		'name'              => _x('Categories', 'taxonomy general name', 'base'),
		'singular_name'     => _x('Category', 'taxonomy singular name', 'base'),
		'search_items'      => __('Search Categories', 'base'),
		'all_items'         => __('All Categories', 'base'),
		'parent_item'       => __('Parent Category', 'base'),
		'parent_item_colon' => __('Parent Category:', 'base'),
		'edit_item'         => __('Edit Category', 'base'),
		'update_item'       => __('Update Category', 'base'),
		'add_new_item'      => __('Add New Category', 'base'),
		'new_item_name'     => __('New Category Name', 'base'),
		'menu_name'         => __('Category', 'base'),
	);

	$args = array(
		'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'has_archive' => true,
        'rewrite'           => array( 'slug' => 'our_category', 'with_front' => false, ),
	);

	register_taxonomy('our-category', array('our_post'), $args);
}
