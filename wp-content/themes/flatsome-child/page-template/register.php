<?php
/**
 * Template Name: Register Page
 *
 */

get_header();

?>

<?php wc_get_template_part('woocommerce/myaccount/register-form'); ?>

<?php get_footer(); ?>
