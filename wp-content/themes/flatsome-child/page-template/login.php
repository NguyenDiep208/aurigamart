<?php
/**
 * Template Name: Login Page
 *
 */

get_header();

?>

<?php wc_get_template_part('woocommerce/myaccount/form-login'); ?>

<?php get_footer(); ?>
