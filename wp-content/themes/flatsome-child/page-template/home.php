<?php
/**
 * Template Name: Home Page
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header();

?>
<div class="main-home home-page">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">

			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
