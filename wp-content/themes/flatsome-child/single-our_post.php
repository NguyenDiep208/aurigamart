<?php

/**

 * The template for displaying all single posts

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post

 *

 * @package WordPress

 * @subpackage Twenty_Nineteen

 * @since 1.0.0

 */


get_header();

?>

<div class="page-wrapper page-vertical-nav">
	<div class="row">
		<div class="large-3 col col-border">
			<?php
		        wp_nav_menu([
		            //'menu'            => 'menu-canvas',
		            'theme_location' => 'menu-post',
		            'container'       => 'div',
		            'container_id'    => 'navbar-block',
		            'container_class' => 'menu-post',
		            'menu_id'         => true,
		            'menu_class'      => 'menu-user-wrap'
		        ]);
		    ?>
		</div>

		<div class="large-9 col">
			<div class="tabs-inner active">
				<?php while ( have_posts() ) : the_post(); ?>
					<div id="wrap-content-site" class="">
						<div id="page-single" class="page-content">
							<!-- Image -->
							<div class="pic-thumb">
								<?php  echo get_the_post_thumbnail($post->ID, 'full');  ?>
							</div>

							<!-- Content article -->
							<div class="entry-content">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();



