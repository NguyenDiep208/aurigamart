<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$wrapper_classes = array();
$row_classes     = array();
$main_classes    = array();
$sidebar_classes = array();

$layout = get_theme_mod( 'checkout_layout' );

if ( ! $layout ) {
	$sidebar_classes[] = 'has-border';
}

if ( $layout == 'simple' ) {
	$sidebar_classes[] = 'is-well';
}

$wrapper_classes = implode( ' ', $wrapper_classes );
$row_classes     = implode( ' ', $row_classes );
$main_classes    = implode( ' ', $main_classes );
$sidebar_classes = implode( ' ', $sidebar_classes );

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

// Social login.
if ( flatsome_option( 'facebook_login_checkout' ) && get_option( 'woocommerce_enable_myaccount_registration' ) == 'yes' && ! is_user_logged_in() ) {
	wc_get_template( 'checkout/social-login.php' );
}
?>

<form name="checkout" method="post" class="checkout woocommerce-checkout <?php echo esc_attr( $wrapper_classes ); ?>" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div class="row pt-0 <?php echo esc_attr( $row_classes ); ?>">
		<div class="large-8 col  <?php echo esc_attr( $main_classes ); ?>">

			<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>

				<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>

				<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 ); ?>
					<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				</div>


		</div>

		<div class="large-4 col">
			<?php flatsome_sticky_column_open( 'checkout_sticky_sidebar' ); ?>

				<div class="col-inner <?php echo esc_attr( $sidebar_classes ); ?>">
					<div class="checkout-sidebar sm-touch-scroll">

						<?php if ( $checkout->get_checkout_fields() ) : ?>

							<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

							<div id="customer_details">
								<div class="clear">
									<?php do_action( 'woocommerce_checkout_billing' ); ?>
								</div>

								<div class="clear">
									<?php do_action( 'woocommerce_checkout_shipping' ); ?>
								</div>
							</div>

							<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

						<?php endif; ?>

						<table cellspacing="0" class="shop_table shop_table_responsive">

							<tr class="cart-subtotal">
								<th><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?> (<?php  echo WC()->cart->get_cart_contents_count(); ?> items)</th>
								<td data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>"><?php wc_cart_totals_subtotal_html(); ?></td>
							</tr>

							<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
								<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
									<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
									<td data-title="<?php echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?>"><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
								</tr>
							<?php endforeach; ?>

							<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

								<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>

								<?php wc_cart_totals_shipping_html(); ?>

								<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>

							<?php elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>

								<tr class="shipping">
									<th><?php esc_html_e( 'Shipping', 'woocommerce' ); ?></th>
									<td data-title="<?php esc_attr_e( 'Shipping', 'woocommerce' ); ?>"><?php woocommerce_shipping_calculator(); ?></td>
								</tr>

							<?php endif; ?>

							<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
								<tr class="fee">
									<th><?php echo esc_html( $fee->name ); ?></th>
									<td data-title="<?php echo esc_attr( $fee->name ); ?>"><?php wc_cart_totals_fee_html( $fee ); ?></td>
								</tr>
							<?php endforeach; ?>

							<?php
							if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) {
								$taxable_address = WC()->customer->get_taxable_address();
								$estimated_text  = '';

								if ( WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping() ) {
									/* translators: %s location. */
									$estimated_text = sprintf( ' <small>' . esc_html__( '(estimated for %s)', 'woocommerce' ) . '</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] );
								}

								if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) {
									foreach ( WC()->cart->get_tax_totals() as $code => $tax ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
										?>
										<tr class="tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
											<th><?php echo esc_html( $tax->label ) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></th>
											<td data-title="<?php echo esc_attr( $tax->label ); ?>"><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
										</tr>
										<?php
									}
								} else {
									?>
									<tr class="tax-total">
										<th><?php echo esc_html( WC()->countries->tax_or_vat() ) . $estimated_text; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></th>
										<td data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>"><?php wc_cart_totals_taxes_total_html(); ?></td>
									</tr>
									<?php
								}
							}
							?>

							<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>

							<tr class="order-total">
								<th><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
								<td data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>"><?php wc_cart_totals_order_total_html(); ?></td>
							</tr>

							<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>

						</table>

						<?php 
							add_action( 'woocommerce_checkout_after_order_review', 'woocommerce_checkout_payment', 20 );
							do_action( 'woocommerce_checkout_after_order_review' ); 
						?>
					</div>
				</div>
		</div>

	</div>
</form>
<div class="checkout-product">
	<div class="recently-viewed-products">
		<?php dynamic_sidebar('recently-viewed'); ?> 
	</div>
	<div class="featured-products">
		<?php aurigamart_fnc_products_upsell_cart(); ?>
	</div>
	
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
