<?php
	function flatsome_checkout_breadcrumb_title(){
		$classes = array();
		if(is_cart()){
			$title = 'Shopping Cart';
		}elseif ( is_checkout() ) {
			$title = 'Checkout details';
		}elseif ( is_wc_endpoint_url('order-received') ) {
			$title = 'Order Complete';
		}else{
			$title = 'Checkout';
		}

		return $title;
	}
?>

<div class="checkout-page-title page-title">
	<div class="page-title-inner flex-row medium-flex-wrap container">
	  <div class="flex-col flex-grow medium-text-center">
	 	 <nav class="breadcrumbs flex-row flex-row-center heading-font checkout-breadcrumbs text-center strong <?php echo get_theme_mod('cart_steps_size','h2'); ?> <?php echo get_theme_mod('cart_steps_case','uppercase'); ?>">
  	   	<?php echo flatsome_checkout_breadcrumb_title(); ?>
		 </nav>
	  </div>
	</div>
</div>