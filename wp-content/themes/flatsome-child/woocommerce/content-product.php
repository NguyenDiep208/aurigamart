<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( fl_woocommerce_version_check( '4.4.0' ) ) {
	if ( empty( $product ) || false === wc_get_loop_product_visibility( $product->get_id() ) || ! $product->is_visible() ) {
		return;
	}
} else {
	if ( empty( $product ) || ! $product->is_visible() ) {
		return;
	}
}

// Check stock status.
$out_of_stock = ! $product->is_in_stock();

// Extra post classes.
$classes   = array();
$classes[] = 'product-small';
$classes[] = 'col';
$classes[] = 'has-hover';

if ( $out_of_stock ) $classes[] = 'out-of-stock';

global $product;

if($product->is_on_sale()) $classes[] = 'price-on-sale';
if(!$product->is_in_stock()) $classes[] = 'price-not-in-stock';
?>

<div <?php wc_product_class( $classes, $product ); ?>>
	<div class="col-inner">
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
	<div class="product-wrap box <?php echo flatsome_product_box_class(); ?>">
		<div class="item">
			<a href="<?php the_permalink(); ?>" id="id-<?php the_id(); ?>" title="<?php the_title(); ?>">

				<!-- Picture -->
				<div class="item-media">
                    <?php if($product->is_on_sale() && !$product->is_type( 'grouped' )): ?>
                        <div class="percent-sale">
		                    <div class="number-percent">
		                        <?php echo aurigamart_get_max_off_sale_product().'% '.'<span>OFF</span>'; ?>
		                    </div>
		                </div>
                    <?php endif; ?>
					<?php
						if (has_post_thumbnail( $product->get_id() )) {
							echo get_the_post_thumbnail($product->get_id(), 'shop_catalog');
						} else {
							echo '<img src="'.wc_placeholder_img_src().'" alt="product placeholder Image" />';
						}
					?>
				</div>

				<div class="item-body">

					<!-- Title -->
					<div class="title-product">
						<h3><?php echo wp_trim_words(get_the_title(), 10, '...'); ?></h3>
					</div>

					<!-- Price -->
					<div class="price-sold">
						<div class="price product-page-price <?php echo implode(' ', $classes); ?>">
                            <?php echo $product->get_price_html(); ?></div>
                            
                            <!-- Free shipping -->
							<?php if ( $product->get_shipping_class() != '' ) : ?>
								<div class="free-shipping">
									<?php echo $product->get_shipping_class(); ?>
									</div>
							<?php endif; ?>
					</div>

					<div class="info-pd">
						<!-- Favorite -->
						<div class="favorite-pd"><?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?></div>

						<div class="ele-pd">
							<!-- Rating -->
							<div class="ratings">
								<?php
								$rating_count = $product->get_rating_count();
								$average      = $product->get_average_rating();

								if ( $rating_count > 0 ) : ?>
							        <?php echo wc_get_rating_html($average, $rating_count); ?>
								<?php endif; ?>
							</div>

							<!-- Sold -->
							<?php if($product->get_total_sales() > 0): ?>
							    <div class="number-sold"> <span><?php echo $product->get_total_sales(). ' Sold'; ?></span></div>
	                        <?php endif; ?>
	                    </div>
					</div>

				</div>
			</a>
		</div>
	</div>
	<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
	</div>
</div>
