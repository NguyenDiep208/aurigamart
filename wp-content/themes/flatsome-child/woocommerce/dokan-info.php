<?php

global $product;

$author_id  = get_post_field( 'post_author', $product->get_id() );
$store_user   = dokan()->vendor->get( $author_id );
$author     = get_user_by( 'id', $author_id );
$store_info = dokan_get_store_info( $author->ID );

$dokan_appearance         = get_option( 'dokan_appearance' );
$profile_layout           = empty( $dokan_appearance['store_header_template'] ) ? 'default' : $dokan_appearance['store_header_template'];
$store_address            = dokan_get_seller_short_address( $author_id, false );

if ( ( 'default' === $profile_layout ) || ( 'layout2' === $profile_layout ) ) {
    $profile_img_class = 'profile-img-circle';
} else {
    $profile_img_class = 'profile-img-square';
}

$ratings = $store_user->get_rating();

?>

<div class="shop-info">
    <div class="item-media">
        <div class="profile-img <?php echo esc_attr( $profile_img_class ); ?>">
            <?php if( $store_user->get_avatar()): ?>
                <img src="<?php echo esc_url( $store_user->get_avatar() ) ?>" alt="<?php echo esc_attr( $store_user->get_shop_name() ) ?>" size="150">
            <?php else: ?>
                 <img src="<?php echo wc_placeholder_img_src(); ?>" alt="<?php echo esc_attr( $store_user->get_shop_name() ) ?>" size="150">
            <?php endif; ?>
        </div>

        <div class="info-shop">
            <h5><?php printf( '<a href="%s">%s</a>', esc_url( dokan_get_store_url( $author->ID ) ), esc_attr( $author->display_name ) ); ?></h5>
            <div class="group-button">
                <?php dokan_live_chat_button_product_page(); ?>
                
                <a href="<?php echo esc_url( dokan_get_store_url( $author->ID ) ) ?>" title="<?php esc_attr( $author->display_name ); ?>">View Shop</a>
            </div>
        </div>
    </div>

    <div class="item-info">
        <ul>
            <?php if($ratings['count']) : ?>
            <li>
                <span class="txt">Ratings</span>
                <span class="num"><?php echo $ratings['count']; ?></span>
            </li>
            <?php endif; ?>

            <?php if($ratings['rating']) : ?>
            <li>
                <span class="txt">Ratings average</span>
                <span class="num"><?php echo $ratings['rating']; ?></span>
            </li>
            <?php endif; ?>
            <li>
                <span class="txt">Joined</span>
                <span class="num"><?php echo human_time_diff( strtotime($store_user->get_register_date()), current_time( 'timestamp' ) ).' '.__( 'ago' ); ?></span>
            </li>
            <li>
                <span class="txt">Products</span>
                <span class="num"><?php echo $store_user->get_products()->post_count; ?></span>
            </li>
        </ul>
    </div>
</div>
