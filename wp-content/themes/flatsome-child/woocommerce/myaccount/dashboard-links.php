<?php
  $current_user = wp_get_current_user();
  $user_id = $current_user->ID;

    
    if ( isset( $user_id ) && ! empty( $user_id ) ) {
      $get_points = (int) get_user_meta( $user_id, 'mwb_wpr_points', true );
    }else{
      $get_points = 0;
    }

?>
<div class="dashboard-links">
  <div class="row">
    <div class="large-4 col">
      <div class="item">
        <div class="txt-link"><a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account' ) ); ?>" title="Profile">Profile</a></div>
        <div class="display_name"><?php echo $current_user->display_name; ?></div>
        <div class="user_email"><?php echo $current_user->user_email; ?></div>
        <div class="phone-numer"><?php echo $current_user->billing_phone; ?></div>
      </div>
    </div>
     <div class="large-4 col">
      <div class="item">
        <div class="txt-link"><a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address' ) ); ?>" title="Address Book">Address Book</a></div>
        <div class="display_name">Billing Address</div>
        <div class="address"><?php echo wc_get_account_formatted_address(); ?></div>
      </div>
    </div>
     <div class="large-4 col">
      <div class="item">
        <div class="txt-link"><a href="<?php echo esc_url( wc_get_endpoint_url( 'points' ) ); ?>" title="My points">My points</a></div>
        <div class="points-number"><?php echo $get_points; ?> Points</div>
        <div class="points-leve"><?php echo custom_mycurrentlevel();?></div>
      </div>
    </div>
  </div>
</div>