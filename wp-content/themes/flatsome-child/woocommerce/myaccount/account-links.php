<?php if ( has_nav_menu( 'my_account' ) ) { ?>
  <ul id="my-account-nav" class="account-nav nav nav-line nav-uppercase nav-vertical mt-half">
  <label>My account</label>
  <?php
    echo wp_nav_menu(array(
      'theme_location' => 'my_account',
      'container' => false,
      'items_wrap' => '%3$s',
      'depth' => 0,
      'walker' => new FlatsomeNavSidebar
    ));
  ?>
   </ul>
<?php } ?>

<?php if ( has_nav_menu( 'my_order' ) ) { ?>
  <ul id="my-order-nav" class="order-nav nav nav-line nav-uppercase nav-vertical mt-half">
  <label>My order</label>
  <?php
    echo wp_nav_menu(array(
      'theme_location' => 'my_order',
      'container' => false,
      'items_wrap' => '%3$s',
      'depth' => 0,
      'walker' => new FlatsomeNavSidebar
    ));
  ?>
   </ul>
<?php } ?>

  <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout">
    <a href="<?php echo esc_url( wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' )) ); ?>"><?php _e('Logout','woocommerce'); ?></a>
  </li>
