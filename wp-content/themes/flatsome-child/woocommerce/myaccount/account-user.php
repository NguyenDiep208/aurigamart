<?php $current_user = wp_get_current_user();?>
<?php if ( in_array( 'customer', (array) $current_user->roles ) ): ?>
	<div class="mycurrent-level">
		<?php echo custom_mycurrentlevel(); ?>
	</div>
<div class="account-user circle">
	
	<span class="user-name inline-block">	
		<?php 
			echo $current_user->display_name;
		?>
	</span>

	<?php do_action('flatsome_after_account_user'); ?>
</div>
<?php endif; ?>