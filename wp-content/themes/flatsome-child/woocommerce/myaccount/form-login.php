<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}?>

<div class="woocommerce-inner">
    <div class="box-content">
<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( !is_user_logged_in()):?>
    <div class="form-login">
        <div class="logo-site">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( $site_title ); ?>" rel="home">
                <img width="200" height="80" src="<?php echo esc_url_raw( get_theme_mod('site_logo_dark')) ?>" alt="<?php echo esc_attr( $site_title ); ?>"/>
            </a>
        </div>
        <div class="wrap-form">
            <div class="header-form">
                <h2><?php esc_html_e( 'Already our member, please log in.', 'woocommerce' ); ?><a href="<?php echo get_permalink(get_page_by_path( 'register' )); ?>"><?php esc_html_e('New? Sign up', 'woocommerce');?></a></h2>
            </div>
            <form class="woocommerce-form woocommerce-form-login login" method="post">

                <?php do_action( 'woocommerce_login_form_start' ); ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
                </p>

                <?php do_action( 'woocommerce_login_form' ); ?>

                <p class="form-row">
                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                        <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
                    </label>
                    <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                    <button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
                </p>
                <p class="woocommerce-LostPassword lost_password">
                    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
                </p>
				 <p class="form-row">
					<?php echo do_shortcode('[nextend_social_login provider="facebook" style="icon"]'); ?>
					<?php echo do_shortcode('[nextend_social_login provider="google" style="icon"]'); ?>
					<?php echo do_shortcode('[nextend_social_login provider="twitter" style="icon"]'); ?>
					<?php echo do_shortcode('[nextend_social_login provider="linkedin" style="icon"]'); ?>
				</p>

                <?php do_action( 'woocommerce_login_form_end' ); ?>

            </form>
        </div>
    </div>
<?php else: ?>
    <?php $user = wp_get_current_user();?>
    <h4>Hello, <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php echo esc_html($user->display_name); ?></a></h4>
    <span><a title="<?php esc_html_e('Log out', 'flatsome'); ?>" class="tips"
             href="<?php echo esc_url(wp_logout_url(home_url())); ?>"><?php esc_html_e('Log Out', 'flatsome'); ?></a></span>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
</div>
</div>