<?php
    global $product;
    $classes = array();
    if($product->is_on_sale()){
        $classes[] = 'price-on-sale';
    }
    if(!$product->is_in_stock()){
        $classes[] = 'price-not-in-stock';
    }
?>
<div class="item">
    <a href="<?php the_permalink(); ?>" id="id-<?php the_id(); ?>" title="<?php the_title(); ?>">

        <!-- Picture -->
        <div class="item-media">
            <?php if($product->is_on_sale() && !$product->is_type( 'grouped' )): ?>
                <div class="percent-sale">
                    <div class="number-percent">
                        <?php echo aurigamart_get_max_off_sale_product().'% '.'<span>OFF</span>'; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php
                if (has_post_thumbnail( get_the_ID() )) {
                    echo get_the_post_thumbnail(get_the_ID(), 'shop_catalog');
                } else {
                    echo '<img src="'.wc_placeholder_img_src().'" alt="product placeholder Image" />';
                }
            ?>
        </div>

        <div class="item-body">

            <!-- Title -->
            <div class="title-product">
                <h3><?php echo wp_trim_words(get_the_title(), 10, '...'); ?></h3>
            </div>

            <!-- Price -->
            <div class="price-sold">
                <div class="price product-page-price <?php echo implode(' ', $classes); ?>">
                    <?php echo $product->get_price_html(); ?></div>
                <?php if($product->get_total_sales() > 0): ?>
                    <div class="number-sold"> <span><?php echo $product->get_total_sales(). ' Sold'; ?></span></div>
                <?php endif; ?>
            </div>
        </div>
    </a>
</div>
