<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$options1 = array(
    'relevance' => __( 'Relevance', 'woocommerce' ),
    'popularity' => __( 'Popularity', 'woocommerce' ),
    'rating'     => __( 'Average rating', 'woocommerce' ),
    'date'       => __( 'Latest', 'woocommerce' ),
);

$options2 = array(
    'price' => __( 'Price: low to high', 'woocommerce' ),
    'price-desc' => __( 'Price: high to low', 'woocommerce' ),
);

if(isset($_GET['orderby']) && !empty($_GET['orderby'])){
    $orderby = $_GET['orderby'];
}else{
    $orderby = '';
}


?>
<form class="woocommerce-ordering" method="get">
    <div class="sort-bar">
        <span class="sort-bar__label">Sort by</span>
        <div class="orderby-options">
            <?php foreach($options1 as $key=>$option){ ?>
                <input type="button" value="<?php echo $option; ?>" data-value="<?php echo $key; ?>" <?php echo ($orderby == $key)? 'checked' : ''; ?>>
            <?php } ?>
            <select class="orderby-price <?php echo array_key_exists($orderby, $options2) ? 'selected': '';?>" aria-label="<?php esc_attr_e( 'Shop order', 'woocommerce' ); ?>">
                <?php foreach ( $options2 as $id => $name ) : ?>
                    <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <input type="hidden" name="orderby"/>
        <input type="hidden" name="paged" value="1" />
        <?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
    </div>
</form>
