<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<div class="pg-single-product">
	<div class="container">
		<div class="row">
			<div class="col small-12 large-12">
				<!-- Breadcrumb -->
				<div class="breadcrumb-block">
					<div class="wrap-inner">
						<?php woocommerce_breadcrumb(); ?>
					</div>
				</div>

				<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

					<div class="section-info">
						<div class="item-media">

							<!-- Picture -->
							<div class="product-images">
								<?php
									/**
									 * Hook: woocommerce_before_single_product_summary.
									 *
									 * @hooked woocommerce_show_product_sale_flash - 10
									 * @hooked woocommerce_show_product_images - 20
									 */
									do_action( 'woocommerce_before_single_product_summary' );
								?>
							</div>

							<div class="share-product">
								
								<!-- Share Social -->
								<div class="share-social">
									<span>Share: </span>
									<?php woocommerce_template_single_sharing(); ?>
								</div>

								<!-- Favorite -->
								<div class="favorite-pd"><?php add_to_wishlist_button(); ?></div>
							</div>
						</div>

						<div class="item-info">

							<!-- Summary -->
							<div class="entry-summary">
								
								<!-- Title -->
								<div class="title-pd">
									<?php woocommerce_template_single_title(); ?>
								</div>

								<!-- Ratings Product -->
								<?php woocommerce_template_single_rating(); ?>

								<!-- Price -->
								<?php woocommerce_template_single_price(); ?>

								<!-- Vouchers -->
								<div class="shop-vouchers">
									
								</div>

								<!-- Bundle -->
								<div class="bundle-deals">
									
								</div>

								<!-- Ship -->
								<div class="shipping-wrap">
									
								</div>

								<!-- Color & Quanlity -->
								<div class="color-quanlity">
									<?php woocommerce_template_single_add_to_cart(); ?>
								</div>
							</div>
						</div>
					</div>

					<!-- Shop -->
					<div class="section-shop">
                        <?php dokan_single_product_info(); ?>
					</div>

					<div class="main-page">
						<div class="block-inner <?php echo is_active_sidebar( 'product-sidebar' )? 'small-12 large-9' : 'small-12 large-12'; ?>">
							
							<!-- Product Specifications -->
							<div class="specifications-product">
								<h4>Product Specifications</h4>

								<?php woocommerce_template_single_meta(); ?>
							</div>

							<!-- Product Description -->
							<div class="description-product">
								<div class="title-head">
									<div class="title-txt">
										<h3>Product Description</h3>
									</div>
								</div>
								<div class="ct-text">
									<?php the_content(); ?>
								</div>
							</div>

							<!-- Product Ratings -->
							<div class="ratings-product">
								<div class="title-head">
									<div class="title-txt">
										<h3>Product Ratings</h3>
									</div>
								</div>
								<div class="review-comment">
									<?php 
										echo do_shortcode('[cusrev_reviews]');
										//display_ratings_reviews();
									?>
								</div>
							</div>

							<!-- Same shop -->
							<div class="same-shop">
								<div class="title-head">
									<div class="title-txt">
										<h3>From the same shop</h3>
									</div>
								</div>
								<div class="list-product single-product"><?php shortcode_same_product($product->get_id(), 8); ?></div>
							</div>

							<!-- Similar Product -->
							<div class="similar-pd">
								<div class="title-head">
									<div class="title-txt">
										<h3>Similar Products</h3>
									</div>
								</div>
								<div class="list-product single-product">
									<?php shortcode_single_product_similar($product->get_id(), 8); ?>
								</div>
							</div>

							<!-- You may also like -->
							<div class="also-like">
								<div class="title-head">
									<div class="title-txt">
										<h3>You may also like</h3>
									</div>
								</div>
								<div class="list-product single-product"><?php shortcode_single_product_upsell($product->get_id(), 8); ?></div>
							</div>
						</div>
						<?php if ( is_active_sidebar( 'product-sidebar' ) ): ?>
							<div class="sidebar-page small-12 large-3">
								<div class="shop-voucher">
									<div class="title-sec">
										<h4>Shop Vouchers</h4>
									</div>
								</div>

								<?php dynamic_sidebar( 'product-sidebar' );?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>


