<?php

global $product;

// Ensure visibility.
if ( fl_woocommerce_version_check( '4.4.0' ) ) {
    if ( empty( $product ) || false === wc_get_loop_product_visibility( $product->get_id() ) || ! $product->is_visible() ) {
        return;
    }
} else {
    if ( empty( $product ) || ! $product->is_visible() ) {
        return;
    }
}

// Check stock status.
$out_of_stock = ! $product->is_in_stock();

// Extra post classes.
$classes   = array();
$classes[] = 'product-small';
$classes[] = 'col';
$classes[] = 'has-hover';

if ( $out_of_stock ) $classes[] = 'out-of-stock';

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

$vendor_id = get_post_field( 'post_author', get_the_id() );
//$vendor = new WP_User($vendor_id);
//$vendor_name = $vendor->display_name;

$store_info = dokan_get_store_info($vendor_id);
?>
<div <?php wc_product_class( $classes, $product ); ?>>
    <div class="item col-inner">
        <a href="<?php the_permalink(); ?>" id="id-<?php the_id(); ?>" title="<?php the_title(); ?>">
            <!-- Sale -->
            <?php if ( $product->is_on_sale() ) : ?>
                <?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale"><span class="sale-bg"></span><span class="sale-text">' . esc_html__( 'Sale', 'aurigamart' ) . '</span></span>', $product ); ?>
            <?php endif; ?>

            <!-- Picture -->
            <div class="pic-product">
                <?php
                if (has_post_thumbnail( get_the_ID() )) {
                    echo get_the_post_thumbnail(get_the_ID(), 'shop_catalog');
                } else {
                    echo '<img src="'.wc_placeholder_img_src().'" alt="product placeholder Image" />';
                }
                ?>
            </div>

            <div class="wrap-info">

                <!-- Name User -->
                <div class="name-user">
                    <span><?php echo $store_info['store_name']; ?></span>
                </div>

                <!-- Title -->
                <div class="title-pr">
                    <h3><?php echo wp_trim_words(get_the_title(), 10, '...'); ?></h3>
                </div>

                <!-- Price -->
                <div class="price-product">
                    <div class="price-sale"><?php echo get_woocommerce_currency_symbol().$product->get_price(); ?></div>

                    <div class="cent-sale">
                        <?php echo $product->get_price_html(); ?>

                        <div class="cent-txt">
                            <?php echo aurigamart_get_max_off_sale_product().'%'; ?>
                        </div>


                        <!-- Free shipping -->
                        <?php if ( $product->get_shipping_class() != '' ) : ?>
                            <div class="free-shipping">
                                <?php echo $product->get_shipping_class(); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="rate-area">
                    <!-- Rating -->
                    <div class="ratings">
                        <?php
                        if ( $rating_count > 0 ) : ?>
                            <?php echo wc_get_rating_html($average, $rating_count); ?>

                            <span class="num-review">(<?php echo esc_html( $review_count )?>)</span>
                        <?php endif; ?>
                    </div>

                    <!-- Area -->
                    <div class="are-txt">
                        <span><?php echo $store_info['address']['city']; ?></span>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
