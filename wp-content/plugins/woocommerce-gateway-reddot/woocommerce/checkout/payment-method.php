<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.0
 */

if (!defined('ABSPATH')) {
	exit;
}
?>
<li class="wc_payment_method payment_method_<?php echo esc_attr($gateway->id); ?>">

	<?php if (esc_attr($gateway->id) == 'reddot' && esc_attr($gateway->enabled) == 'yes'): ?>
		<input id="payment_method_<?php echo esc_attr($gateway->id); ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr($gateway->id); ?>" <?php checked($gateway->chosen, true);?> data-order_button_text="<?php echo esc_attr($gateway->order_button_text); ?>" />

		<label for="payment_method_<?php echo esc_attr($gateway->id); ?>" style="display: none;">
			<?php echo $gateway->get_title(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?> <?php //echo $gateway->get_icon(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?>
		</label>
		<?php
?>

		<div class="payment_method_<?php echo esc_attr($gateway->id); ?>" <?php if (!$gateway->chosen): /* phpcs:ignore Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace */?><?php endif; /* phpcs:ignore Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace */?>>
			<?php $gateway->payment_fields();?>
			<!-- Select card type and base on currency MID will be set.  -->

			<?php if (esc_attr($gateway->id) == 'reddot'): ?>
			<?php
$current_user = wp_get_current_user();
$currentCurrency = get_option('woocommerce_currency');
if ($gateway->settings['testmode'] == 'no') {
	$midsData = get_option('woocommerce_bacs_production_mids');
} else {
	$midsData = get_option('woocommerce_bacs_test_mids');
}
$creditCardType = PAYMENT_MODE_VALUE;

$reddotSettings = get_option('woocommerce_reddot_settings');

/* MID values base on current Currency */
$mids = array();
?>
			<?php if ($current_user->ID != 0): ?>
			<?php endif;?>
				<?php
$cardDropdownArray = array();

foreach ($midsData as $key => $value) {
	$midlabel = '';
	if ($value['currency'] == $currentCurrency) {
		$mids[] = $value['mid'];
		$cardDropdownArray[$value['mid_label']] = $value['mid'];
	}
}

$cnt = 1;
foreach ($cardDropdownArray as $midkey => $midvalue) {
	echo '<div class="reddot-card-option"><input id="cardType" type="radio" class="reddot-card-input input-radio cardType midval_' . $cnt . '" name="cardType" value="' . $cnt . '" data-order_button_text=""><label class="reddot-card-label midlbl" data-midlbl="' . $cnt . '" style="font-size:1.6rem;font-family:helvetica;display:inline;padding-right:10px;">' . $midkey . '</label></div>';
	$cnt++;
}

?>
			</select>
			<?php
global $wpdb;
$cardTable = REDDOT_CARD_DETAILS_TABLE;
$current_user = wp_get_current_user();

$cardData = $wpdb->get_results("SELECT * FROM " . $cardTable . " WHERE mid IN (" . implode(',', $mids) . ") AND customer_id = '" . $current_user->ID . "' AND payment_type = '" . $reddotSettings['payment_type'] . "'");
if (count($cardData) > 0) {
	?>
				<hr style="display: block;height: 1px;border: 0;border-top: 1px solid #ccc;margin: 0px 0em 1em 0px;padding: 0;position: inherit;" />
				<div class="reddot-saved-card reddot-card-option">
					<input id="savedcard" type="radio" class="input-radio card_radio" name="card_radio" value="savedcard" data-order_button_text="">
					<select name="saved_card" class="saved_card" id="saved_credit_card" disabled="disabled">
					<option value="">Select Saved Card</option>
					<?php
foreach ($cardData as $ckey => $cvalue) {
		echo '<option value="' . $cvalue->payer_id . '" data-mid="' . $cvalue->mid . '">' . $cvalue->first_6 . 'xxxxxx' . $cvalue->last_4 . '</option>';
	}
	?>
					</select>
					<input type="hidden" name="savedcard_mid" class="savedcard_mid" value="" />
				</div>
				<?php
}
?>
			<?php endif;?>
			<!-- Select card type and base on currency MID will be set.  -->


		</div>
	<?php else: ?>
		<input id="payment_method_<?php echo esc_attr($gateway->id); ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr($gateway->id); ?>" <?php checked($gateway->chosen, true);?> data-order_button_text="<?php echo esc_attr($gateway->order_button_text); ?>" />

		<label for="payment_method_<?php echo esc_attr($gateway->id); ?>">
			<?php echo $gateway->get_title(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?> <?php echo $gateway->get_icon(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?>
		</label>

		<?php if ($gateway->has_fields() || $gateway->get_description()): ?>

			<div class="payment_box payment_method_<?php echo esc_attr($gateway->id); ?>" <?php if (!$gateway->chosen): /* phpcs:ignore Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace */?>style="display:none;"<?php endif; /* phpcs:ignore Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace */?>>
				<?php $gateway->payment_fields();?>
			</div>
		<?php endif;?>
	<?php endif;?>

</li>
<style type="text/css">
.woocommerce .delayloaderimg:before {
height: 3em;
width: 3em;
position: absolute;
top: 50%;
left: 50%;
display: block;
content: "";
-webkit-animation: none;
-moz-animation: none;
animation: none;
background: url('<?php echo plugins_url('delayloader.gif', __FILE__); ?>') center center;
background-size: cover;
line-height: 1;
text-align: center;
font-size: 2em;
margin-left: -40px;
}

.woocommerce #overlay {
  /*position: fixed; */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5); /* Black background with opacity */
  z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
}

.wc_payment_method .reddot-saved-card {
	display: flex;
	align-items: center;
}
.wc_payment_method .reddot-saved-card input[type="radio"] {
    display: inline;
    margin-right: 10px;
}
.reddot-saved-card input[type="radio"] {
	width: 14px;
	height: 14px;
}
.reddot-saved-card input[type="radio"]:after {
	width: 14px;
	height: 14px;
	left: -4px;
	top: -4px;
}
.payment_method_reddot{
	margin-left: -10px;
	line-height: 2;
	font-size: 1em;
	font-weight: 400;
}
/*
.reddot-card-option {
  position: relative;
  padding: 0 6px;
  margin: 5px 0 10px 0;
}

.reddot-card-option input[type='radio'] {
  display:none;
}

.reddot-card-option label {
  color: #000;
  font-weight: normal;
}

.reddot-card-option label:before {
  content: " ";
  display: inline-block;
  position: relative;
  top: 5px;
  margin: 0 15px 0 5px;
  width: 20px;
  height: 20px;
  border-radius: 11px;
  border: 2px solid #000;
  background-color: transparent;
}

.reddot-card-option input[type=radio]:checked + label:after {
  border-radius: 11px;
  width: 12px;
  height: 12px;
  position: absolute;
  top: 11px;
  left: 27px;
  content: " ";
  display: block;
  background: #000;
} */
.reddot-card-option {
  display: block;
  position: relative;
  padding-left: 5px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.reddot-card-option input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}
.reddot-card-option label {
  color: #000;
  font-weight: normal;
}
.reddot-card-label{
	color:#ccc;
}
.reddot-card-option label:before {
  content: "";
  display: inline-block;
  position: relative;
  top: 5px;
  margin: 0 15px 0 0px;
  width: 15px;
  height: 15px;
  border-radius: 11px;
  background-color: #6D6D6D;

}

.reddot-card-option input[type=radio]:checked + label:after {
  border-radius: 11px;
  width: 5px;
  height: 5px;
  position: absolute;
  top: 25px;
  left: 20px;
  content: "";
  display: block;
  background: #fff;
}
.reddot-card-option label:after {
  background-color: violet;
}
/*.reddot-card-input {
    transform: scale(1.5);
    -webkit-transform: scale(1.5);
    border: 1px solid #000;
}*/
</style>
<div class="delayloader" style="margin-left:10px;display:none;"><div id="overlay"><div class="delayloaderimg"></div></div></div>
<script type="text/javascript">
jQuery(document).ready(function(){

	jQuery(".reddot-card-option").click(function(){
			jQuery(".input-radio").prop("checked", false);
			jQuery('#payment_method_reddot').prop("checked", true);
			jQuery('.payment_box').hide();
			//jQuery( ".reddot-card-input" ).trigger( "click" );
			jQuery('input[type=radio]', this).prop("checked",true);
			//jQuery(this).prop("checked",true);

	});

	jQuery(".reddot-card-input").click(function(){
			jQuery(".input-radio").prop("checked", false);
			jQuery('#payment_method_reddot').prop("checked", true);
			jQuery('.payment_box').hide();
			//$('input[type=radio]', this).prop("checked",true);
			jQuery(this).prop("checked",true);

	});

	jQuery(".input-radio").click(function(){
			jQuery(".reddot-card-input").prop("checked", false);
			jQuery('#payment_method_reddot').prop("checked", false);
			jQuery(this).prop("checked",true);

	});


	jQuery(".saved_card").change(function(){
	  // alert(jQuery(".saved_card option:selected").attr("data-mid"));
	  jQuery('.savedcard_mid').val(jQuery(".saved_card option:selected").attr("data-mid"));
	  jQuery('#cardType').val("1");
	});

	jQuery(".card_radio").change(function(){
	  console.log(jQuery(".card_radio:checked").val());
		if(jQuery(".card_radio:checked").val() == 'savedcard'){
			jQuery('.cardType').prop("checked", false);
			jQuery('#cardType').val("1");
			jQuery('#saved_credit_card').prop("disabled", false);
		}

	});

	jQuery(".cardType").change(function(){

		console.log(jQuery(".cardType:checked").val());
		if(jQuery(".cardType:checked").val() > 0){
			jQuery('.card_radio').prop("checked", false);
			jQuery('#cardType').prop("disabled", false);
			jQuery('#saved_credit_card').prop("disabled", true);
			jQuery('#saved_credit_card').val("");
		} else {
			jQuery("input[name=cardType][value=" + 1 + "]").attr('checked', 'checked');
		}
	});

	jQuery(".midlbl").click(function(){
		var mval = jQuery(this).attr('data-midlbl');
		// console.log(mval);
		jQuery('.cardType').prop("checked", false);
		jQuery('.midval_'+mval).prop("checked", true);
	});
});
</script>
<?php if (isset($_REQUEST['delay']) && $_REQUEST['delay'] == 'true'): ?>
<script type="text/javascript">
jQuery(document).ready(function(){
	checkNotifyFlag(<?php echo $_REQUEST['orderid']; ?>);
});
</script>
<?php endif;?>