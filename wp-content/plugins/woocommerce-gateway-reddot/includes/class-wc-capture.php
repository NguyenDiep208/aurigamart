<?php
class RD_Capture extends WC_Payment_Gateway
{
    // auto capture details
    private $_response_type = "json";
    private $_action_type = "capture";
    private $_order_number = "";
    private $_mid = "";
    private $_signature = "";
    private $_amount = "";
    private $_currency = "";
    private $_payment_type = "A";
    
    private $_secret_key = "";

    // sandbox
    private $_merchant_api_url = "https://test.reddotpayment.com/instanpanel/api/payment";
    // production
    // private $_payment_api_url = "https://connect.reddotpayment.com/instanpanel/api/payment";


    public function doCapture($order,$transactionId,$paymentGatewayObject,$trans_type='')
    {
        global $wpdb;
        try
        {
            $reddotTable = REDDOTTABLE;
            $sql = $wpdb->get_results("SELECT * FROM ".$reddotTable." WHERE (orderid = '".$order->get_id()."') ORDER BY id DESC LIMIT 0,1");
            $orderData = $sql[0];

            // $this->_mid = $paymentGatewayObject->get_option( 'merchant_id' );
            // $this->_secret_key = $paymentGatewayObject->get_option( 'secret_key' );
            $this->_mid = $orderData->mid;
            $this->_secret_key = $orderData->secret_key;
            $this->_amount = $order->get_total();
            $this->_currency = $order->get_currency();

            if(!$this->_action_type)
            {
                die("Invalid Action Type");
            }

            if(!$this->_amount)
            {
                die("Invalid Amount");
            }

            if(!$transactionId)
            {
                die("Invalid Transaction Id");
            }

            $this->_order_number = $order->get_id(); 
            $params = array(
                "response_type" => $this->_response_type,
                "action_type" => $this->_action_type,
                "order_number" => $this->_order_number,
                "mid" => $this->_mid,
                "amount" => $this->_amount,
                "currency" => $this->_currency,
                "transaction_id" => $transactionId
            );
            $this->_signature = $this->_calculateSignature($params);
            $params['signature'] = $this->_signature;
            
            $response = $this->_callApi($this->_merchant_api_url, $params);

            if($response) {
                $response = json_decode($response, true);
                $this->updateOrder($order, $transactionId, $response,$trans_type);
                return true;
            }
            
            
        } catch(Exception $e) {
            $this->_generateLog($e->getMessage());
            $this->_generateLog($e->getTraceAsString());
        }
        return false;
    }

    private function _calculateSignature(&$params, $chosenFields=NULL)
    {
        if ($chosenFields == NULL) {
            $chosenFields = array_keys($params);
        }

        // Sort the key fields //
        sort($chosenFields);
        $requestsString = '';

        // Assemble them by using '&' as a separator string //
        foreach ($chosenFields as $field) {
            if (isset($params[$field])) {
                $requestsString .= $field . '=' . ($params[$field]) .'&';
            }
        }

        // Embed the secret key //
        $requestsString .= 'secret_key=' . $this->_secret_key; 

        // Finally hash the request string //
        $signature = md5($requestsString);

        return $signature;
    }

    private function _checkSignatureForRDPConnectAPI(&$params)
    {
        // Calculate our-own the signature based on the received response parameters //
        $copyParams = $params;
        unset($copyParams['signature']);
        $calculatedSignature = $this->_calculateSignature($copyParams);

        // Compare our-own calculated signature and the one that received //
        if ($calculatedSignature != $params['signature']) {
            return false;
        }

        return true;
    }

    private function _generateLog($data, $fileName = "")
    {
        if(!$fileName)
        {
            $fileName = dirname( __FILE__ ) ."/log/RD_payment_log_".date("Ymd");
        }
        $file = fopen($fileName.".log", "a+");

        if(!is_scalar($data))
        {
            $data = print_r($data, true).PHP_EOL;
        }
        else {
            $data = $data.PHP_EOL;
        }

        fwrite($file, $data);
        fclose($file);
    }

    private function _callApi($url, $data = array())
    {
        // Get cURL resource //
        $curl = curl_init();

        $postfields = substr((http_build_query($data) . "&"), 0, -1);

        // Set some options - we are passing in a user agent too here //
        curl_setopt_array ($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => FALSE
        ));

        // Send the request & save response to $resp //
        $json_response = curl_exec($curl);
        $curl_errno = curl_errno($curl);
        $curl_err = curl_error($curl);

        // Close request to clear up some resources //
        curl_close($curl);

        /* Insert Request and response into the table For order */
        global $wpdb;
        $table_name = REDDOTTABLE;
        $reqdata = json_encode($data);        
        $orderResData = json_decode($json_response);

        $this->_generateLog('========Debug Call Api Capture fun=======');

        $pt = 'AC';    
        if(!empty($orderResData)){
            $tabledata=array(
                'orderid' => $orderResData->order_number, 
                'mid' => $this->_mid,
                'secret_key' => $this->_secret_key,
                'pt' => $pt,
                'request' => $reqdata,
                'response' => $json_response,
                'response_code' => $orderResData->reason_code,
                'response_msg' => $orderResData->result_status
            );
            $wpdb->insert( $table_name, $tabledata);
        }
        
        /* Insert Request and response into the table For order */


        $this->_generateLog("Capture Error No: ".$curl_errno);
        $this->_generateLog("Capture Curl Error: ".$curl_err);
        $this->_generateLog("Request :");
        $this->_generateLog($data);
        $this->_generateLog("Response :");
        $this->_generateLog(json_decode($json_response,true));

        return $json_response;
    }
    
    public function updateOrder($order, $transactionId, $response='',$trans_type='')
    {
        global $woocommerce;

        $orderId = $order->get_order_number();
        $RDP = new WC_Reddot();
        
        $message = array();
        if($response['result_status']=='failed') {
            
            if(!is_admin())
                wc_add_notice($errorMessage, 'error');

            if ($transactionId)
            {          
                $order->set_transaction_id($transactionId);          
                //$order->add_order_note("Payment Failed. <br/> Transaction Id: $transactionId");

                $order->add_order_note("                        
                    <b>Red Dot Payment</b><br>
                    Capture failed"
                );
                //$order->add_order_note('Failed Response: '.json_encode($response));
            }
            // $order->update_status('failed');
            $order->save();
        }
        else {
            $msg = 'Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be processing your order soon.';
            //$woocommerce->add_message($msg);
            if(!is_admin())
                wc_add_notice($msg, 'success');
            //if($trans_type!='A')
            if(!empty($response['result_status']) && $response['result_status']=='accepted') {     

                $order->add_order_note("                            
                    <b>Red Dot Payment</b><br>
                    Capture Success"
                );
            }
            else {
                $order->add_order_note("                            
                    <b>Red Dot Payment</b><br>
                    Capture Success"
                );
            }

            $this->saveSuccessMetaByMethod($orderId,'AC');

            $this->_generateLog("Current Status Before Capture => ".$order->get_status());

            if($order->get_status() == 'review'){
                $order->update_status( 'processing' );     
            }            
            $order->payment_complete($transactionId);
            $order->set_transaction_id($transactionId);
            //$order->add_order_note("RedDot payment successful <br/>Transaction Id: $transactionId");            

            if (isset($woocommerce->cart) === true)
            {
                $woocommerce->cart->empty_cart();
            }
        }
        
        if(!is_admin())
            $this->redirectUser($order);
        else 
            wp_redirect(wp_get_referer());
    }
    
    /* Function which save meta value for all Transaction Type (Direct,pre-auth,and capture) 
        Like for sale = sale_success , pre-auth = auth_success, auth-capture = capture_succcess */
    public function saveSuccessMetaByMethod($orderId,$transactionType)
    {
        $tType = '';
        if(isset($transactionType) && $transactionType == 'AC'){
            $tType = 'capture';
        } 
        update_post_meta( $orderId, $tType.'_success', 'success');
    }

    protected function redirectUser($order)
    {
        $redirectUrl = $this->get_return_url($order);

        wp_redirect($redirectUrl);
        exit;
    }
}

