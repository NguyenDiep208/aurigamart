<?php 

/* Show card details in My account page */
function rd_flush_rewrite_rules() {
    add_rewrite_endpoint( 'carddetails', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'removecard', EP_ROOT | EP_PAGES );
    flush_rewrite_rules();
  } // end function

  // register new endpoint to use for My Account page
  add_action( 'init', 'rd_carddetails_endpoints' );
  function rd_carddetails_endpoints() {
    add_rewrite_endpoint( 'carddetails', EP_ROOT | EP_PAGES );
  } // end function

  // add new query vars.
  add_filter( 'query_vars', 'rd_carddetails_query_vars', 0 );
  function rd_carddetails_query_vars( $vars ) {
    $vars[] = 'carddetails';
    return $vars;
  } // end function

  // insert the custom endpoints into the My Account menu
  add_filter( 'woocommerce_account_menu_items', 'rd_add_carddetails_endpoints',99, 1  );
  function rd_add_carddetails_endpoints( $items ) {
    // Remove the logout menu item.
    $logout = $items['customer-logout'];
    unset( $items['customer-logout'] );
    $items['carddetails'] = 'Saved Cards';
    // Insert back the logout item.
    $items['customer-logout'] = $logout;
    return $items;
  } // end function    

add_action( 'woocommerce_account_carddetails_endpoint', 'rd_carddetails_content' );
function rd_carddetails_content() {

$current_user = wp_get_current_user();
$cardTypeArray = PAYMENT_MODE_VALUE;
$cardType = '';

global $wpdb;
$cardTable = REDDOT_CARD_DETAILS_TABLE;

$cardData = $wpdb->get_results("SELECT * FROM ".$cardTable." WHERE (customer_id = '".$current_user->ID."')");

$cardDataHtml = '<table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
    <thead>
        <tr>
            <th class="woocommerce-orders-table__header card_type"><span class="nobr">Card Type</span></th>
            <th class="woocommerce-orders-table__header card_number"><span class="nobr">Card Number</span></th>
            <th class="woocommerce-orders-table__header card_add_date"><span class="nobr">Card Add Date</span></th>
            <th class="woocommerce-orders-table__header payment_type"><span class="nobr">Payment Type</span></th>
            <th class="woocommerce-orders-table__header actions"><span class="nobr">Actions</span></th>
        </tr>
    </thead>
    <tbody>';

    if(count($cardData) > 0){
        foreach ($cardData as $ckey => $cvalue) {
            // $paymentType = '';
            if(isset($cvalue->payment_type) && $cvalue->payment_type==WC_Reddot::SALE) {
                $paymentType = 'Direct Sales';
            }else {
                $paymentType = 'Pre-Authorization';
            }

            $cardDataHtml .= '<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-completed order">
                <td class="woocommerce-orders-table__cell " data-title="Card Type">
                    '.$cardTypeArray[$cvalue->payment_mode].'
                </td>
                <td class="woocommerce-orders-table__cell" data-title="Card Number">
                    '.$cvalue->first_6.'xxxxxx'.$cvalue->last_4.'
                </td>
                <td class="woocommerce-orders-table__cell" data-title="Card Add Date">
                    '.date('F d, Y ',strtotime($cvalue->created_date)).'
                </td>
                <td class="woocommerce-orders-table__cell " data-title="Payment Type">
                    '.$paymentType.'
                </td>
                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                    <a href="'.get_permalink( get_option('woocommerce_myaccount_page_id') ).'removecard/?id='.$cvalue->id.'" class="woocommerce-button button view removecard" >Delete</a>                                                  
                </td>
            </tr>';   
        }
    } else {
        $cardDataHtml .= '<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-completed order"><td class="woocommerce-orders-table__cell" colspan="5" style="text-align:center;padding:10px;">No saved cards yet</td></tr>';
    }
        
    $cardDataHtml .= '</tbody>
</table>';
echo $cardDataHtml;
}
/* Show card details in My account page */

/* Remove Card from the system */
// register new endpoint to use for My Account page
add_action( 'init', 'rd_removecard_endpoints' );
function rd_removecard_endpoints() {
add_rewrite_endpoint( 'removecard', EP_ROOT | EP_PAGES );
} // end function

// add new query vars.
add_filter( 'query_vars', 'rd_removecard_query_vars', 0 );
function rd_removecard_query_vars( $vars ) {
$vars[] = 'removecard';
return $vars;
} // end function  

add_action( 'woocommerce_account_removecard_endpoint', 'rd_removecard_content' );
function rd_removecard_content() {

global $wpdb;
$redirect = get_permalink( get_option('woocommerce_myaccount_page_id') );
$cardTable = REDDOT_CARD_DETAILS_TABLE;    
$id = $_REQUEST['id'];
if(isset($id) && $id != ''){

    $reddpt = new WC_Reddot();

    $sql = $wpdb->get_results("SELECT * FROM ".$cardTable." WHERE (id = '".$id."')");
    $cardData = $sql[0];

    $orderData = $reddpt->getReddotTableData($cardData->order_id);     

    $params = array(
        "request_mid" => $cardData->mid,
        "transaction_id" => $cardData->transaction_id
    );
    $signature = $reddpt->_sign_generic($params,$orderData->secret_key);

    $args = array(
        "mid" => $cardData->mid,
        "order_id" => $cardData->order_id,
        "api_mode" => "direct_token_api",
        "transaction_type" => "R",
        "payer_email" => "jessy.loran@gmail.com",
        "payer_id" => $cardData->payer_id,
        "signature" => $signature
    );
    $args['signature'] = $reddpt->_sign_generic($args,$orderData->secret_key);

    $data = json_encode($args);
    $url = $reddpt->get_token_api_url();

    $response = $reddpt->_callApi($url,$data);
    $response = json_decode($response);

    if($response->response_code == 0){
        $wpdb->query("DELETE FROM ".$cardTable." WHERE id = '".$id."'" );
        wc_add_notice( __( 'Card details removed successfully.', 'woocommerce' ) );     
    } else {
        wc_add_notice( __( $response->response_msg, 'woocommerce' ), 'error' );
    }
}      
// wp_safe_redirect( $redirect.'carddetails/' ); 
?>
<script>window.location.replace("<?php echo $redirect.'carddetails/'; ?>");</script>
<?php
exit;
}
/* Remove Card from the system */