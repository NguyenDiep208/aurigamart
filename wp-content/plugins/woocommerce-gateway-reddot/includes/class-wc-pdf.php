<?php
// require_once(reddot_plugin_path().'/lib/MPDF60/mpdf.php');

class RD_Pdf extends WC_Payment_Gateway
{   
    /* Create pdf using MPDF */
    // public function generatePdf($html,$filename,$location)
    // {
    //     $mpdf=new mPDF('+aCJK','A4',20,'',10,10,25,25,15,15); 
    //     $mpdf->SetDisplayMode('fullpage');
    //     $mpdf->autoLangToFont = true;
    //     $mpdf->WriteHTML($html);
    //     $mpdf->Output($location.$filename,"F");
    // }

    public function generatePdf($html,$filename,$location)
    {
        $file = fopen($location.$filename,"w");
        fwrite($file,$html);
        fclose($file);
    }

    /* Create pdf file for Platform Details*/
    public function generatePlatformDetails($platformdata)
    {        
        
        include_once reddot_plugin_path() . '/function.php';
        $emailDetails = new RD_Function();
        $html = $emailDetails->getPlatformDetails(json_decode($platformdata)); 
        $filename = 'platformdetails.html';
        $location = reddot_plugin_path().'/orderdata/';  
        $this->generatePdf($html,$filename,$location);      
    }

    /* Create pdf file for Order notes */
    public function generateOrderNotesPdf($orderId)
    { 
        $orderNotes = wc_get_order_notes([
            'order_id' => $orderId,
            'type' => 'system'
        ]);

        include_once reddot_plugin_path() . '/function.php';
        $emailDetails = new RD_Function();
        $orderNotesHtml = $emailDetails->getOrderNotesDetails($orderNotes);
        $notesFilename = $orderId.'_notes.html';
        $noteslocation = reddot_plugin_path().'/orderdata/notes/'; 
        $this->generatePdf($orderNotesHtml,$notesFilename,$noteslocation);   
    }

    /* Create pdf file for order request / response*/
    public function generateRequestResponsePdf($data,$orderId)
    {        
        $response_path = reddot_plugin_path() . '/orderdata/requestresponse/';     
        $responseFilename = $orderId.'_request_response.html';
        $html = $this->generateRequestResponseHtml($data);
        $this->generatePdf($html,$responseFilename,$response_path);        
    }

    /* Create order request response html for pdf */
    public function generateRequestResponseHtml($data)
    {           
        $requestresponseHtml = '';
        $request = '';
        $request = '';

        $requestresponseHtml .='<div style="margin-bottom: 40px;"><table class="td" cellspacing="0" cellpadding="6" border="1" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; width: 100%; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;">
                <thead>
                    <tr>
                        <th class="td" scope="col" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;" colspan="3">Order Request / Response Data</th>
                    </tr>
                </thead>
                <tbody>';
        foreach ($data as $dk => $dval) {

            $request = $this->pretty_print($dval->request);
            $response = $this->pretty_print($dval->response);

            $requestresponseHtml .='<tr class="order_item">
            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: \'Helvetica Neue, Helvetica, Roboto, Arial, sans-serif\'; word-wrap: break-word;min-width:200px;">'.date('F d, Y h:i:s',strtotime($dval->created_date)).'</td>
            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: \'Helvetica Neue, Helvetica, Roboto, Arial, sans-serif\'; word-wrap: break-word;">Request</td>
            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: \'Helvetica Neue, Helvetica, Roboto, Arial, sans-serif\'; word-wrap: break-word;">'.$request.'</td>
            <tr>';
            $requestresponseHtml .='<tr class="order_item">
            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: \'Helvetica Neue, Helvetica, Roboto, Arial, sans-serif\'; word-wrap: break-word;width:300px;">'.date('F d, Y h:i:s',strtotime($dval->created_date)).'</td>
            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: \'Helvetica Neue, Helvetica, Roboto, Arial, sans-serif\'; word-wrap: break-word;">Response</td>
            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: \'Helvetica Neue, Helvetica, Roboto, Arial, sans-serif\'; word-wrap: break-word;">'.$response.'</pre></td>
            <tr>';
            $requestresponseHtml .='<tr class="order_item"><td colspan="3" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: \'Helvetica Neue, Helvetica, Roboto, Arial, sans-serif\'; word-wrap: break-word;"></td><tr>';
        }
        
        $requestresponseHtml .= '</tbody>
                </table></div>';

        return $requestresponseHtml;
    }

    //Declare the custom function for formatting
    public function pretty_print($json_data)
    {
        $jsonhtml = '';
        //Initialize variable for adding space
        $space = 0;
        $flag = false;

        //Using <pre> tag to format alignment and font
        $jsonhtml .= "<pre>";

        //loop for iterating the full json data
        for($counter=0; $counter<strlen($json_data); $counter++)
        {

            //Checking ending second and third brackets
            if ( $json_data[$counter] == '}' || $json_data[$counter] == ']' )
            {
                $space--;
                $jsonhtml .= "\n";
                $jsonhtml .= str_repeat(' ', ($space*2));
            }
         

            //Checking for double quote(“) and comma (,)
            if ( $json_data[$counter] == '"' && ($json_data[$counter-1] == ',' ||
             $json_data[$counter-2] == ',') )
            {
                $jsonhtml .= "\n";
                $jsonhtml .= str_repeat(' ', ($space*2));
            }
            if ( $json_data[$counter] == '"' && !$flag )
            {
                if ( $json_data[$counter-1] == ':' || $json_data[$counter-2] == ':' )

                //Add formatting for question and answer
                $jsonhtml .= '<span style="color:blue;font-weight:bold">';
                else

                //Add formatting for answer options
                $jsonhtml .= '<span style="color:red;">';
            }
            $jsonhtml .= $json_data[$counter];
            //Checking conditions for adding closing span tag
            if ( $json_data[$counter] == '"' && $flag )
                $jsonhtml .= '</span>';
            if ( $json_data[$counter] == '"' )
                $flag = !$flag;

            //Checking starting second and third brackets
            if ( $json_data[$counter] == '{' || $json_data[$counter] == '[' )
            {
                $space++;
                $jsonhtml .= "\n";
                $jsonhtml .= str_repeat(' ', ($space*2));
            }
        }
        $jsonhtml .= "</pre>";

        return $jsonhtml;
    }
}