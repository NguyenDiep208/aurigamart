<?php
/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'reddot_init_gateway_class' );
function reddot_init_gateway_class() {
    

    if ( ! class_exists( 'WooCommerce' ) ) {
        add_action( 'admin_notices', 'woocommerce_reddot_missing_wc_notice' );
        return;
    }

    class WC_Reddot extends WC_Payment_Gateway {
        const SESSION_KEY = 'reddot_wc_order_id';
        const SALE = 'S';
        const AUTHORIZE = 'A';
        const AUTHORIZE_CAPTURE = 'AC';
        const REFUND_ACCEPTED = 'accepted';
        const REFUND_FAILED = 'failed';

        const DIRECT_TEST_PAYMENT_API_URL = 'https://secure-dev.reddotpayment.com/service/payment-api';
        const DIRECT_LIVE_PAYMENT_API_URL = 'https://secure.reddotpayment.com/service/payment-api';
        const DIRECT_TEST_TOKEN_API_URL = 'https://secure-dev.reddotpayment.com/service/token-api';
        const DIRECT_LIVE_TOKEN_API_URL = 'https://secure.reddotpayment.com/service/token-api';
        const DIRECT_TEST_QUERY_REDIRECTION_URL = 'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_redirection';
        const DIRECT_LIVE_QUERY_REDIRECTION_URL = 'https://secure.reddotpayment.com/service/Merchant_processor/query_redirection';
        const REFUND_TEST_PAYMENT_API_URL = 'https://test.reddotpayment.com/instanpanel/api/payment';
        const REFUND_LIVE_PAYMENT_API_URL = 'https://connect.reddotpayment.com/instanpanel/api/payment';


        private $mid;
        private $secret_key;
        private $ccy;
        private $payment_type;
        private $base_url;
        private $back_url = "?a=back";
        private $redirect_url = "a=".RESPONSE_TYPE_SUCCESS;
        private $notify_url = "a=".RESPONSE_TYPE_NOTIFY;
        private $payment_api_url = '';
        private $token_api_url = '';
        private $query_redirection_url = '';
        private $refund_payment_url = '';
        private $payment_url_mode = '';
        private $notifyDelayFlag = false;
        
        public $site_url;
        
        public $id = 'reddot';

        /**
         * Title of the payment method shown on the admin page.
         * @var string
         */
        public $method_title = 'Red Dot Payment';

        /**
         * Description of the payment method shown on the admin page.
         * @var  string
         */
        public $method_description = 'Securely pay through Red Dot payment gateway page using Credit Card.';
        
        /**
         * Can be set to true if you want payment fields
         * to show on the checkout (if doing a direct integration).
         * @var boolean
         */
        public $has_fields = false;
        
        /**
         * Icon URL, set in constructor
         * @var string
         */
        public $icon;        
        
         /**
          * Class constructor, more about it in Step 3
          */
        public function __construct() {
            global $woocommerce,$wpdb;
            
            $this->icon =  plugins_url('images/logo.png' , __FILE__);
            $this->site_url = get_site_url(); 

            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments    
            $this->supports = array(
                'products',
                'refunds'
            );
         
            // Method with all the options fields
            $this->init_form_fields();
            // Method to Allows to register multiple test mid 
            $this->generate_multiple_test_mid_details_html();
            // Method to Allows to register multiple  Production  mid 
            $this->generate_multiple_production_mid_details_html();

            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
            $this->mid = '';
            $this->rdp_email_address = $this->get_option( 'rdp_email_address' );
            $this->secret_key = '';
            $this->payment_type = $this->api_payment_type = $this->get_option( 'payment_type' );            
            if($this->payment_type==self::AUTHORIZE || $this->payment_type==self::AUTHORIZE_CAPTURE) {
                $this->api_payment_type = self::AUTHORIZE;
            }
            /* set api url base on the Test mode Or live mode */
            if($this->testmode){
                $this->payment_api_url = self::DIRECT_TEST_PAYMENT_API_URL;
                $this->token_api_url = self::DIRECT_TEST_TOKEN_API_URL;
                $this->query_redirection_url = self::DIRECT_TEST_QUERY_REDIRECTION_URL;
                $this->refund_payment_url = self::REFUND_TEST_PAYMENT_API_URL;
            } else {
                $this->payment_api_url = self::DIRECT_LIVE_PAYMENT_API_URL;
                $this->token_api_url = self::DIRECT_LIVE_TOKEN_API_URL;
                $this->query_redirection_url = self::DIRECT_LIVE_QUERY_REDIRECTION_URL;
                $this->refund_payment_url = self::REFUND_LIVE_PAYMENT_API_URL;
            }

            // BACS Allows to register multiple test mid fields.
            $this->multiple_test_mid_details = get_option(
                'woocommerce_bacs_test_mids',
                array(
                    array(
                        'mid_label'   => $this->get_option( 'mid_label' ),
                        'mid' => $this->get_option( 'mid' ),
                        'secret_key'      => $this->get_option( 'secret_key' ),
                        'currency'      => $this->get_option( 'currency' ),
                    ),
                )
            );

            // BACS Allows to register multiple Production mid fields.
            $this->multiple_production_mid_details = get_option(
                'woocommerce_bacs_production_mids',
                array(
                    array(
                        'mid_label'   => $this->get_option( 'mid_label' ),
                        'mid' => $this->get_option( 'mid' ),
                        'secret_key'      => $this->get_option( 'secret_key' ),
                        'currency'      => $this->get_option( 'currency' ),
                    ),
                )
            );

            /* add custom css file for reddot plugin */
            wp_register_style( 'reddot_style', plugins_url( '/css/reddot.css', __DIR__ ) );
            wp_enqueue_style( 'reddot_style' );
            /* add custom js file for reddot plugin */
            wp_enqueue_script( 'reddot_script', plugins_url( '/js/reddot.js', __DIR__ ), array('jquery') );
            wp_localize_script( 'reddot_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
            /* add custom js file for reddot plugin */
            // We need custom JavaScript to obtain a token
            add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
            
            add_action('admin_enqueue_scripts', array( $this, 'reddot_admin_style' ));

            if ( is_admin() ) {
                $cb = array($this, 'process_admin_options');

                if (version_compare(WOOCOMMERCE_VERSION, '2.0.0', '>='))
                {
                    add_action("woocommerce_update_options_payment_gateways_{$this->id}", $cb);
                }
                else
                {
                    add_action('woocommerce_update_options_payment_gateways', $cb);
                }
            } 

            $this->responseType  = (isset($_REQUEST['a']) && $_REQUEST['a']) ? $_REQUEST['a'] : "";
            $this->logOrderID = '';
            // Here we are checking response with notify_url or success 
            if(!empty($_REQUEST['a'])) {  
                if($_REQUEST['a']!='success') {  
                    $this->_generateLog('Request Url a :=> '.$_REQUEST['a']);                    
                    $_notifyResponse = (array)(json_decode(file_get_contents( 'php://input' )));     
                    $orderId = (isset($_notifyResponse['order_id']) && $_notifyResponse['order_id']) ? $_notifyResponse['order_id'] : "";
                    $this->logOrderID = $orderId;
                    $sucessFlag = get_post_meta( $orderId, 'sucessFlag', true );
                    $this->_generateLog('sucessFlag :=> '.$sucessFlag);
                    if(!empty($sucessFlag) && $sucessFlag == 'on'){
                        $this->_generateNotifyDelayLog('Notify Delay Log Start ');
                        $this->_generateNotifyDelayLog($_notifyResponse);
                        exit; // order is updated in redirect case so nothing to do in notify.
                    } else {                        
                        $this->check_reddot_response();                              
                    }
                    
                } else {
                    $this->_generateLog('Request Url a :=> '.$_REQUEST['a']);
                    $orderId = $woocommerce->session->get(self::SESSION_KEY);
                    $this->logOrderID = $orderId;
                    $order = new WC_Order($orderId);

                    $notifyFlag = get_post_meta( $orderId, 'notifyFlag', true );
                    $this->_generateLog('notifyFlag :=> '.$notifyFlag);
                    $this->_generateLog('Success Response :=> ');
                    $this->_generateLog($_REQUEST);

                    if(isset($notifyFlag) && $notifyFlag == 'on'){
                        if($order && $order->get_status() == 'pending' ){
                            $this->verifyReddotResponse($orderId);
                        } else if($order && ($order->get_status() == 'processing' || $order->get_status() == 'review') ){
                            $this->redirectUser($order);
                            exit;
                        } else { // if notify response delay and return in site
                            $this->_generateLog('Unable to process order.');
                            $_REQUEST['order_id'] = $orderId;
                            $this->_generateLog($_REQUEST);
                            wc_add_notice('We are unable to process your order. Please contact to administrator.', 'error');
                            wp_redirect(wc_get_page_permalink( 'checkout' ));
                            exit; 
                        }
                    } else {
                        $this->check_reddot_response();
                    }
                                                                                     
                }                
            }

            $this->initHooks();

            //Actions 
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'save_multiple_mid_details' ) );          
         }
 
        /**
          * Plugin options, we deal with it in Step 3 too
          */
         public function init_form_fields(){
 
            $this->form_fields = array(
            'enabled' => array(
                'title'       => 'Enable/Disable',
                'label'       => 'Enable Red Dot Gateway',
                'type'        => 'checkbox',
                'description' => '',
                'default'     => 'no'
            ),
            'title' => array(
                'title'       => 'Title',
                'type'        => 'text',
                'description' => 'This controls the title which the user sees during checkout.',
                'default'     => 'Online Payment',
                'desc_tip'    => true,
            ),
            'description' => array(
                'title'       => 'Description',
                'type'        => 'textarea',
                'description' => 'This controls the description which the user sees during checkout.',
                'default'     => 'Pay with your credit card via Red Dot payment gateway.',
            ),
            'testmode' => array(
                'title'       => 'Test Mode',
                'label'       => 'Enable Test Mode',
                'type'        => 'checkbox',
                'description' => 'Place the payment gateway in test mode using test API keys.',
                'default'     => 'yes',
                'desc_tip'    => true,
            ),
            'payment_type' => array(
                'title'       => 'Payment Type',
                'type'        => 'select',
                'default' => self::SALE,
                'options' => array(
                        self::SALE => 'Direct Sales',
                        self::AUTHORIZE => 'Pre-Authorization'
                        // self::AUTHORIZE_CAPTURE => 'Authorize and Capture'
                    )
            ),
            'multiple_test_mid_details' => array(
                'type' => 'multiple_test_mid_details',
            ),
            'multiple_production_mid_details' => array(
                'type' => 'multiple_production_mid_details',
            ),
            'rdp_email_address' => array(
                'title'       => 'RDP Plugin Support Email Address(s)',
                'type'        => 'text',
                'placeholder' => 'support@reddotpay.com',
                'default'     => 'support@reddotpay.com',
                'description' => '(Email address for plugins and transactions troubleshoot)'
            ),
            );    
 
         }
        

        /**
         * Generate Allows to register multiple test mid html.
         *
         * @return string
         */
        public function generate_multiple_test_mid_details_html() {
            ob_start();

            $tcurrency_code_options = get_woocommerce_currencies();
            $tcurrencyOptions = '';
            foreach ( $tcurrency_code_options as $tcode => $tname ) {
                $tcurrencyOptions .= '<option value="'.$tcode.'">'.$tname.'('.get_woocommerce_currency_symbol( $tcode ).')'.'</option>'; 
            }

            $tcreditCardTypeOptions = '';
            foreach ( PAYMENT_MODE_VALUE as $ttype => $tcname ) {
                $tcreditCardTypeOptions .= '<option value="'.$ttype.'">'.$tcname.'</option>';
            }

            ?>
            <tr valign="top">
                <th scope="row" class="titledesc"><?php esc_html_e( 'Test MID', 'woocommerce'); ?></th>
                <td class="forminp" id="testbacs_mids">
                    <div class="wc_input_table_wrapper">
                        <table class="widefat wc_input_table sortable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="sort">&nbsp;</th>
                                    <th><?php esc_html_e( 'MID Label', 'woocommerce' ); ?></th>
                                    <th><?php esc_html_e( 'MID', 'woocommerce' ); ?></th>
                                    <th><?php esc_html_e( 'Secret Key', 'woocommerce' ); ?></th>
                                    <th><?php esc_html_e( 'Currency', 'woocommerce' ); ?></th>
                                </tr>
                            </thead>
                            <tbody class="mids">
                                <?php
                                $i = -1;                  

                                if ( $this->multiple_test_mid_details ) {
                                    foreach ( $this->multiple_test_mid_details as $tmid ) {
                                        $i++;

                                        $tcurrencyOptionsSelected = '';
                                        foreach ( $tcurrency_code_options as $tcode => $tname ) {
                                            if(esc_attr( $tmid['currency'] ) == $tcode){
                                                $tcurrencyOptionsSelected .= '<option value="'.$tcode.'" selected="selected">'.$tname.'('.get_woocommerce_currency_symbol( $tcode ).')'.'</option>';
                                            } else {
                                                $tcurrencyOptionsSelected .= '<option value="'.$tcode.'">'.$tname.'('.get_woocommerce_currency_symbol( $tcode ).')'.'</option>';    
                                            }
                                            
                                        }

                                        echo '<tr class="mid">
                                            <td class="sort"></td>
                                            <td class="mid-label"><input type="text" value="' . esc_attr( $tmid['mid_label'] ) . '" name="tbacs_mid_label[' . esc_attr( $i ) . ']" /></td>
                                            <td><input type="text" value="' . esc_attr( $tmid['mid'] ) . '" name="tbacs_mid[' . esc_attr( $i ) . ']" /></td>
                                            <td><input type="text" value="' . esc_attr( wp_unslash( $tmid['secret_key'] ) ) . '" name="tbacs_secret_key[' . esc_attr( $i ) . ']" /></td>
                                            <td><select name="tbacs_currency[' . esc_attr( $i ) . ']" id="woocommerce_currency" class="wc-enhanced-select">
                                            '.$tcurrencyOptionsSelected.'</select></td>
                                        </tr>';
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5"><a href="#" class="add button"><?php esc_html_e( '+ Add MID', 'woocommerce' ); ?></a> <a href="#" class="remove_rows button"><?php esc_html_e( 'Remove selected MID(s)', 'woocommerce' ); ?></a></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <script type="text/javascript">
                        jQuery(function() {
                            jQuery('#testbacs_mids').on( 'click', 'a.add', function(){

                                var tsize = jQuery('#testbacs_mids').find('tbody .mid').length;
                                var tcurrency = '<?php echo $tcurrencyOptions; ?>';

                                jQuery('<tr class="mid">\
                                        <td class="sort"></td>\
                                        <td><input type="text" name="tbacs_mid_label[' + tsize + ']" /></td>\
                                        <td><input type="text" name="tbacs_mid[' + tsize + ']" /></td>\
                                        <td><input type="text" name="tbacs_secret_key[' + tsize + ']" /></td>\
                                        <td><select name="tbacs_currency[' + tsize + ']" class="wc-enhanced-select">'+tcurrency+'</select></td>\
                                    </tr>').appendTo('#testbacs_mids table tbody');

                                jQuery( document.body ).trigger( 'wc-enhanced-select-init' );
                                return false;
                            });
                        });
                    </script>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

        /**
         * Generate Allows to register multiple  Production  mid html.
         *
         * @return string
         */
        public function generate_multiple_production_mid_details_html() {
            ob_start();

            $pcurrency_code_options = get_woocommerce_currencies();
            $pcurrencyOptions = '';
            foreach ( $pcurrency_code_options as $pcode => $pname ) {
                $pcurrencyOptions .= '<option value="'.$pcode.'">'.$pname.'('.get_woocommerce_currency_symbol( $pcode ).')'.'</option>'; 
            }

            $pcreditCardTypeOptions = '';
            foreach ( PAYMENT_MODE_VALUE as $ptype => $pcname ) {
                $pcreditCardTypeOptions .= '<option value="'.$ptype.'">'.$pcname.'</option>';
            }

            ?>
            <tr valign="top">
                <th scope="row" class="titledesc"><?php esc_html_e( 'Production MID', 'woocommerce'); ?></th>
                <td class="forminp" id="pbacs_mids">
                    <div class="wc_input_table_wrapper">
                        <table class="widefat wc_input_table sortable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="sort">&nbsp;</th>
                                    <th><?php esc_html_e( 'MID Label', 'woocommerce' ); ?></th>
                                    <th><?php esc_html_e( 'MID', 'woocommerce' ); ?></th>
                                    <th><?php esc_html_e( 'Secret Key', 'woocommerce' ); ?></th>
                                    <th><?php esc_html_e( 'Currency', 'woocommerce' ); ?></th>
                                </tr>
                            </thead>
                            <tbody class="mids">
                                <?php
                                $j = -1;                  

                                if ( $this->multiple_production_mid_details ) {
                                    foreach ( $this->multiple_production_mid_details as $pmid ) {
                                        $j++;

                                        $pcurrencyOptionsSelected = '';
                                        foreach ( $pcurrency_code_options as $pcode => $pname ) {
                                            if(esc_attr( $pmid['currency'] ) == $pcode){
                                                $pcurrencyOptionsSelected .= '<option value="'.$pcode.'" selected="selected">'.$pname.'('.get_woocommerce_currency_symbol( $pcode ).')'.'</option>';
                                            } else {
                                                $pcurrencyOptionsSelected .= '<option value="'.$pcode.'">'.$pname.'('.get_woocommerce_currency_symbol( $pcode ).')'.'</option>';    
                                            }
                                            
                                        }

                                        echo '<tr class="mid">
                                            <td class="sort"></td>
                                            <td class="mid-label"><input type="text" value="' . esc_attr( $pmid['mid_label'] ) . '" name="pbacs_mid_label[' . esc_attr( $j ) . ']" /></td>
                                            <td><input type="text" value="' . esc_attr( $pmid['mid'] ) . '" name="pbacs_mid[' . esc_attr( $j ) . ']" /></td>
                                            <td><input type="text" value="' . esc_attr( wp_unslash( $pmid['secret_key'] ) ) . '" name="pbacs_secret_key[' . esc_attr( $j ) . ']" /></td>
                                            <td><select name="pbacs_currency[' . esc_attr( $j ) . ']" id="woocommerce_currency" class="wc-enhanced-select">
                                            '.$pcurrencyOptionsSelected.'</select></td>
                                        </tr>';
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5"><a href="#" class="add button"><?php esc_html_e( '+ Add MID', 'woocommerce' ); ?></a> <a href="#" class="remove_rows button"><?php esc_html_e( 'Remove selected MID(s)', 'woocommerce' ); ?></a></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <script type="text/javascript">
                        jQuery(function() {
                            jQuery('#pbacs_mids').on( 'click', 'a.add', function(){

                                var psize = jQuery('#pbacs_mids').find('tbody .mid').length;
                                var pcurrency = '<?php echo $pcurrencyOptions; ?>';

                                jQuery('<tr class="mid">\
                                        <td class="sort"></td>\
                                        <td><input type="text" name="pbacs_mid_label[' + psize + ']" /></td>\
                                        <td><input type="text" name="pbacs_mid[' + psize + ']" /></td>\
                                        <td><input type="text" name="pbacs_secret_key[' + psize + ']" /></td>\
                                        <td><select name="pbacs_currency[' + psize + ']" class="wc-enhanced-select">'+pcurrency+'</select></td>\
                                    </tr>').appendTo('#pbacs_mids table tbody');

                                jQuery( document.body ).trigger( 'wc-enhanced-select-init' );
                                return false;
                            });
                        });
                    </script>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

        /**
         * Save account details table.
         */
        public function save_multiple_mid_details() {
            
            $emailFlag = false;
            if(empty($_REQUEST["woocommerce_reddot_rdp_email_address"])) {
                // $emailFlag = true;
                // $emailErr = "Email is required";
                $reddotSettings = get_option('woocommerce_reddot_settings');
                $reddotSettings['rdp_email_address'] = 'support@reddotpay.com';
                update_option( 'woocommerce_reddot_settings', $reddotSettings );
            } else {
                $email = htmlspecialchars(stripslashes(trim($_REQUEST["woocommerce_reddot_rdp_email_address"])));
                // check if e-mail address is well-formed
                $email = (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email)) 
            ? FALSE : TRUE; 
                if(!$email){
                    $emailFlag = true;
                    $emailErr = "Invalid email format.";
                    $reddotSettings = get_option('woocommerce_reddot_settings');
                    $reddotSettings['rdp_email_address'] = '';
                    update_option( 'woocommerce_reddot_settings', $reddotSettings );
                }
            }
            
            $tmids = array();            
            $pmids = array();            

            if ( isset( $_POST['tbacs_mid_label'] ) && isset( $_POST['tbacs_mid'] ) && isset( $_POST['tbacs_secret_key'] )
                 && isset( $_POST['tbacs_currency'] ) ) {

                $tmid_label   = wc_clean( wp_unslash( $_POST['tbacs_mid_label'] ) );
                $tmid = wc_clean( wp_unslash( $_POST['tbacs_mid'] ) );
                $tsecret_key      = wc_clean( wp_unslash( $_POST['tbacs_secret_key'] ) );
                $tcurrency      = wc_clean( wp_unslash( $_POST['tbacs_currency'] ) );

                foreach ( $tmid_label as $i => $tlabel ) {
                    if ( ! isset( $tmid_label[ $i ] ) ) {
                        continue;
                    }

                    $tmids[] = array(
                        'mid_label'   => $tmid_label[ $i ],
                        'mid' => $tmid[ $i ],
                        'secret_key'      => $tsecret_key[ $i ],
                        'currency'      => $tcurrency[ $i ],
                    );
                }
            }

            if ( isset( $_POST['pbacs_mid_label'] ) && isset( $_POST['pbacs_mid'] ) && isset( $_POST['pbacs_secret_key'] )
                 && isset( $_POST['pbacs_currency'] ) ) {

                $pmid_label   = wc_clean( wp_unslash( $_POST['pbacs_mid_label'] ) );
                $pmid = wc_clean( wp_unslash( $_POST['pbacs_mid'] ) );
                $psecret_key      = wc_clean( wp_unslash( $_POST['pbacs_secret_key'] ) );
                $pcurrency      = wc_clean( wp_unslash( $_POST['pbacs_currency'] ) );

                foreach ( $pmid_label as $i => $plabel ) {
                    if ( ! isset( $pmid_label[ $i ] ) ) {
                        continue;
                    }

                    $pmids[] = array(
                        'mid_label'   => $pmid_label[ $i ],
                        'mid' => $pmid[ $i ],
                        'secret_key'      => $psecret_key[ $i ],
                        'currency'      => $pcurrency[ $i ],
                    );
                }
            }

            $tmids = array_unique($tmids, SORT_REGULAR);
            $pmids = array_unique($pmids, SORT_REGULAR);

            update_option( 'woocommerce_bacs_test_mids', $tmids );
            update_option( 'woocommerce_bacs_production_mids', $pmids );

            if($emailFlag){                
                ?>
                <div class="notice notice-success  is-dismissible">
                     <p><?php echo $emailErr; ?></p>
                 </div>
                <?php
            } 
            
        }

        public function getMIDbyCard($mkey)
        {
            
            $reddotSettings = get_option('woocommerce_reddot_settings');
            if($reddotSettings['testmode'] == 'yes'){
                $midsData = get_option('woocommerce_bacs_test_mids');
            } else {
                $midsData = get_option('woocommerce_bacs_production_mids');
            }                           
            
            $currentCurrency = get_option('woocommerce_currency');
            $creditCardType = PAYMENT_MODE_VALUE;

            $cardDropdownArray = array();   

            foreach ($midsData as $key => $value) {    
                $midlabel = '';                          
                if($value['currency'] == $currentCurrency){
                    $cardDropdownArray[$value['mid_label']] = $value['mid'];
                }               
            }

            $cnt = 1;
            $MIDARRAY = array();
            foreach ($cardDropdownArray as $midkey => $midvalue) { 
                $MIDARRAY[$cnt] = $midvalue;
                $cnt++;
            }         

            if(isset($MIDARRAY[$mkey])){
                return $MIDARRAY[$mkey];    
            } else {
                return;
            }            
        }

        /**
         * You will need it if you want your custom credit card form, Step 4 is about it
         */
        public function payment_fields() {
 
        }
 
        /*
         * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
         */
        public function payment_scripts() {
 
        }
        
        // Update CSS within in Admin
        public function reddot_admin_style() {
          wp_enqueue_style('admin-styles', plugins_url( '/css/reddot_admin.css', __FILE__ ));
        }

        /*
          * Fields validation, more in Step 5
         */
        public function validate_fields() {
 
            /*if( empty( $_POST[ 'billing_first_name' ]) ) {
                wc_add_notice(  'First name is required!', 'error' );
                return false;
            }*/
            return true;
        }
        
        public function getTransactionTypes($opt='') {
            $options = array(
                        self::SALE => 'Sale',
                        self::AUTHORIZE => 'Authorize',
                        self::AUTHORIZE_CAPTURE => 'Authorize and Capture'
                        );
            return (!empty($opt) && isset($options[$opt])) ? $options[$opt] : $options;
        }
 
        /*
         * We're processing the payments here, everything about it is in Step 5
         */
        public function process_payment( $order_id ) {
    
            global $woocommerce;

            // we need it to get any order detailes
            $order = wc_get_order( $order_id );
            
            $orderKey = $order->order_key;
            
            $woocommerce->session->set(self::SESSION_KEY, $order_id);
            $this->ccy = $order->get_currency();

            /* Update MID base on selected card type and current currency */
            $cardType = $_REQUEST['cardType'];
            add_post_meta( $order_id, '_card_type', $cardType, true );
            $currentCurrency = get_option('woocommerce_currency');

            $reddotSettings = get_option('woocommerce_reddot_settings');
            if($reddotSettings['testmode'] == 'yes'){
                $midsData = get_option('woocommerce_bacs_test_mids');
            } else {
                $midsData = get_option('woocommerce_bacs_production_mids');
            }            

            if(isset($_REQUEST['saved_card']) && $_REQUEST['saved_card'] != ''){
                foreach ($midsData as $key => $value) {
                    if($value['currency'] == $currentCurrency && $value['mid'] == $_REQUEST['savedcard_mid']){
                        $this->mid = $value['mid'];
                        $this->secret_key = $value['secret_key'];
                    }
                }
            } else {

                foreach ($midsData as $key => $value) {
                    // if($value['currency'] == $currentCurrency && $value['mid_label'].$key == $cardType){
                    if($value['currency'] == $currentCurrency && $value['mid'] == $this->getMIDbyCard($cardType)){
                        $this->mid = $value['mid'];
                        $this->secret_key = $value['secret_key'];
                    }
                }    
            }

            /* Update MID base on selected card type and current currency */

            $structure = get_option( 'permalink_structure' );
            
            if(empty($structure)){
                $this->redirect_url = "&".$this->redirect_url;
            } else {
                $this->redirect_url = "?".$this->redirect_url;
            }

            /*
              * Array with parameters for API interaction
             */
            if(isset($_REQUEST['saved_card']) && $_REQUEST['saved_card'] != ''){
                
                $params = array($this->mid, $order_id, $this->api_payment_type, $order->total, $this->ccy,$_REQUEST['saved_card']);
                $signature = $this->_sign_generic($params,$this->secret_key);
                $this->base_url = $this->get_return_url();
                $args = array(
                    "redirect_url" => $this->base_url.$this->redirect_url."&pt=".$this->payment_type,
                    "notify_url" => $this->base_url.$this->notify_url."&pt=".$this->payment_type,
                    "back_url" => wc_get_checkout_url().$this->back_url,
                    "mid" => $this->mid,
                    "order_id" => $order_id,
                    "amount" => $order->total,
                    "ccy" => $this->ccy,
                    "api_mode" => "redirection_hosted",
                    "payment_type" => $this->api_payment_type,
                    //"merchant_reference" => "",
                    "payer_id" => $_REQUEST['saved_card'],
                    "secret_key" => $this->secret_key,
                    "signature" => $signature
                );
                
            } else {
                $params = array($this->mid, $order_id, $this->api_payment_type, $order->total, $this->ccy);
                $signature = $this->_sign_generic($params,$this->secret_key);
                $this->base_url = $this->get_return_url();
                $args = array(
                    "redirect_url" => $this->base_url.$this->redirect_url."&pt=".$this->payment_type,
                    "notify_url" => $this->base_url.$this->notify_url."&pt=".$this->payment_type,
                    "back_url" => wc_get_checkout_url().$this->back_url,
                    "mid" => $this->mid,
                    "order_id" => $order_id,
                    "amount" => $order->total,
                    "ccy" => $this->ccy,
                    "api_mode" => "redirection_hosted",
                    "payment_type" => $this->api_payment_type,
                    //"merchant_reference" => "",
                    "signature" => $signature
                );
            }

            $tbacs_mids = apply_filters( 'woocommerce_bacs_test_mids', $this->multiple_test_mid_details );
            $pbacs_mids = apply_filters( 'woocommerce_bacs_production_mids', $this->multiple_production_mid_details );

            $response = $this->_callApi($this->payment_api_url, json_encode($args));

            $redirect_payment_url = $this->_response_handling($response,$this->secret_key);
                return array(
                'result' => 'success',
                'redirect' => add_query_arg('key', $orderKey, $redirect_payment_url)
            );

         
             if( !is_wp_error( $response ) ) {
         
                 $body = json_decode( $response['body'], true );
         
                 // it could be different depending on your payment processor
                 if ( $body['response']['responseCode'] == 'APPROVED' ) {
         
                    // we received the payment
                    $order->payment_complete();
                    $order->reduce_order_stock();
         
                    // some notes to customer (replace true with false to make it private)
                    $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
         
                    // Empty cart
                    $woocommerce->cart->empty_cart();
         
                    // Redirect to the thank you page
                    return array(
                        'result' => 'success',
                        'redirect' => $this->get_return_url( $order )
                    );
         
                 } else {
                    wc_add_notice(  'Please try again.', 'error' );
                    return;
                }
         
            } else {
                wc_add_notice(  'Connection error.', 'error' );
                return;
            }
 
        }
        
        protected function initHooks()
        {
            add_action('init', array(&$this, 'check_reddot_response'));
            add_action('woocommerce_api_' . $this->id, array(&$this, 'check_reddot_response'));
            add_action( 'init', array(&$this, 'reddot_reset_permalinks'));
        }
        
        private function _compare_sign($resp_array,$secretKey)
        {
            // (See Generic Signature section,
            // Especially for the sign_generic() function definition)
            if ( isset($resp_array['signature']) ) {
                $calculated_signature = $this->_sign_generic($resp_array,$secretKey);
                if ($calculated_signature != $resp_array['signature']) {
                    $this->_generateLog("Signature : signature wrong! invalid response!");
                    throw new Exception('signature wrong! invalid response!');
                }
                return true;
                //echo "signature is fine, continue processing the request";
            } else {
            // zero response_code means a successful transaction, and definitely has signature
            if ($resp_array['response_code'] == 0)
                $this->_generateLog("Signature : signature not found! invalid response!");
                throw new Exception('signature not found! invalid response!');
                // error / reject transactions might not have any signature in it
                return false;
                //echo "signature not found! Must be an error/invalid request";
            }
            return false;
        }

        public function _sign_generic($params,$secretKey)
        {   
            global $woocommerce,$wpdb;
            // a copy-passing, so it's not altering the original 
            unset($params['signature']);

            $data_to_sign = "";
            $this->_recursive_generic_array_sign($params, $data_to_sign);

            $this->secret_key = $secretKey;
            $data_to_sign .= $this->secret_key;

            return hash('sha512', $data_to_sign);
        }

        private function _recursive_generic_array_sign(&$params, &$data_to_sign)
        {
            //    sort the parameters based on its key 
            ksort($params);
            
            //    Traverse through each component
            //    And generate the concatenated string to sign 
            foreach ($params as $v) {

                if (is_array($v)) {
                    //    in case of array traverse inside
                    //    and build further the string to sign
                    $this->_recursive_generic_array_sign($v, $data_to_sign);
                }
                else {
                    //    Not an array means this is a key=>value map,
                    //    Concatenate the value to data to sign
                    $data_to_sign .= $v;
                }
            }
        }

        public function calculateSignature(&$params, $secretKey, $chosenFields=NULL)
        {
            if ($chosenFields == NULL) {
              $chosenFields = array_keys($params);
            }

            // Sort the key fields //
            sort($chosenFields);
            $requestsString = '';

            // Assemble them by using '&' as a separator string //
            foreach ($chosenFields as $field) {
              if (isset($params[$field])) {
                $requestsString .= $field . '=' . ($params[$field]) .'&';
              }
            }

            // Embed the secret key //
            $requestsString .= 'secret_key=' . $secretKey; 

            // Finally hash the request string //
            $signature = md5($requestsString);

            return $signature;
        }

        public function refund_signature($secret_key, $params) 
        {
            ksort($params);
            $pre_signature_string = '';

            foreach($params as $key => $value) {
              $pre_signature_string .= $key .'='. $value .'&';
            }
            $pre_signature_string .= 'secret_key=' .$secret_key;

            $signature = md5($pre_signature_string);
            return $signature;
        }

        private function _response_handling($json_response,$secretKey)
        {
            $array_response = json_decode($json_response, true);        
            if ($array_response['response_code'] == 0)
            {
                // Calculate signature using sign generic function //
                $calculated_signature = $this->_sign_generic($array_response,$secretKey);

                // Validate the received transaction signature //
                if ($calculated_signature == $array_response['signature'])
                {
                    if (!empty($array_response['payment_url']))
                    {
                        // Redirect customers to payment page //
                        //header ('Location: ' . $array_response['payment_url']);
                        return $array_response['payment_url'];
                        exit;
                    }
                    else
                    {
                        // Empty payment URL in succesful txn (should not happen) //
                        throw new Exception('Invalid response, no payment_url');
                    }
                }
                else
                {
                    // Invalid signature, the response might not come from RDP //
                    throw new Exception('Invalid signature!');
                }
            }
            else
            {
                throw new Exception('Invalid request : '.$array_response['response_msg']);
            }
        }

        public function _generateLog($data, $fileName = "")
        {   
            global $woocommerce;

            if( !function_exists('get_plugin_data') ){
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            }

            $plugin_data = get_plugin_data( plugin_dir_path( __DIR__ ).'woocommerce-gateway-reddot.php');

            $plugin_version = $plugin_data['Version'];

            $log_path = plugin_dir_path(__FILE__);
            //for orderwise log
            $orderId = '';
            if(is_admin()){
                if(isset($_REQUEST['order_id'])){
                    $orderId = $_REQUEST['order_id'];
                } else {
                    $orderId = $_REQUEST['post_ID'];    
                }
                
            } else {
                $orderId = $this->logOrderID;             
            }
            
            if(isset($orderId) && $orderId != '') {
                $dir = "/log/".date("Ymd");
                if (!file_exists($log_path.$dir)) {
                    mkdir($log_path.$dir);
                }

                $logData = "\n" . '==== Red Dot Payment Gateway Version: '.$plugin_version.' ====' . "\n";
                $logData .= date('Y-m-d H:i:s').PHP_EOL;
                $orderfileName = dirname( __FILE__ ) .$dir."/RD_payment_".$orderId."_log";   //for order log
                $orderfile = fopen($orderfileName.".log", "a+");
                if(!is_scalar($data))
                {
                    $logData .= print_r($data, true).PHP_EOL;
                }
                else {
                    $logData .= $data.PHP_EOL;
                }

                fwrite($orderfile, $logData);
                fclose($orderfile);
            }
            
            $logData = "\n" . '==== Red Dot Payment Gateway Version: '.$plugin_version.' ====' . "\n";                        
            $logData .= date('Y-m-d H:i:s').PHP_EOL;
            if(!$fileName)
            {
                $fileName = dirname( __FILE__ ) ."/log/RD_payment_log_".date("Ymd");
            }
            $file = fopen($fileName.".log", "a+");

            if(!is_scalar($data))
            {
                $logData .= print_r($data, true).PHP_EOL;
            }
            else {
                $logData .= $data.PHP_EOL;
            }

            fwrite($file, $logData);
            fclose($file);
        }

        public function _generateNotifyDelayLog($data, $fileName = "")
        {
            $log_path = plugin_dir_path(__FILE__);
            if(!$fileName)
            {
                $fileName = dirname( __FILE__ ) ."/Notifylog/RD_payment_log_".date("Ymd");
            }
            $file = fopen($fileName.".log", "a+");

            if(!is_scalar($data))
            {
                $data = print_r($data, true).PHP_EOL;
            }
            else {
                $data = $data.PHP_EOL;
            }

            fwrite($file, $data);
            fclose($file);
        }
        

        public function _callApi($url, $data)
        {
            $curl = curl_init($url);
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_POST => 1, // using POST method
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,

                // JSON Request Parameters is put in the BODY of request
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json')
            )); 

            //This is the JSON response containing transaction information // 
            $json_response = curl_exec($curl);
            $curl_errno = curl_errno($curl);
            $curl_err = curl_error($curl);
            curl_close($curl);

            /* Insert Request and response into the table For order */
            global $wpdb;
            $table_name = REDDOTTABLE;
            $reqData = json_decode($data);            
            $orderResData = json_decode($json_response);

            $this->_generateLog('========Debug Call Api fun=======');

            $pt = '';    
            if(isset($reqData->payment_type)){
                $pt = $reqData->payment_type;    
            }else {
                $pt = $orderResData->transaction_type;    
            }
            if(!empty($orderResData)){
                $tabledata=array(
                    'orderid' => $orderResData->order_id, 
                    'mid' => $this->mid,
                    'secret_key' => $this->secret_key,
                    'pt' => $pt,
                    'request' => $data,
                    'response' => $json_response,
                    'response_code' => $orderResData->response_code,
                    'response_type' => $this->responseType,
                    'response_msg' => $orderResData->response_msg,
                    'created_timestamp' => $orderResData->created_timestamp
                );
                $wpdb->insert( $table_name, $tabledata);
            }            
            /* Insert Request and response into the table For order */    

            $this->_generateLog("Error No: ".$curl_errno);
            $this->_generateLog("Curl Error: ".$curl_err);
            $this->_generateLog("Request :");
            $this->_generateLog(json_decode($data,true));
            $this->_generateLog("Response :");
            $this->_generateLog(json_decode($json_response,true));

            return $json_response;
        } 

        public function _callApiVerify($url, $data)
        {
            $curl = curl_init($url);
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_POST => 1, // using POST method
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,

                // JSON Request Parameters is put in the BODY of request
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json')
            )); 

            //This is the JSON response containing transaction information // 
            $json_response = curl_exec($curl);
            $curl_errno = curl_errno($curl);
            $curl_err = curl_error($curl);
            curl_close($curl);   

            $this->_generateLog("Verify Reddot Response in _callApiVerify Fun");
            $this->_generateLog("Error No: ".$curl_errno);
            $this->_generateLog("Curl Error: ".$curl_err);
            $this->_generateLog("Request :");
            $this->_generateLog(json_decode($data,true));
            $this->_generateLog("Response :");
            $this->_generateLog(json_decode($json_response,true));

            return $json_response;
        }        
        
        /* change permalinks */
        private function reset_permalinks() {
            global $wp_rewrite;
            $wp_rewrite->set_permalink_structure( '/%postname%/' );
        }

        /**
         * Check for valid razorpay server callback
         **/
        private function check_reddot_response()
        {
            global $woocommerce,$wpdb;

            if($_REQUEST['a']=='notify') {
                $pType = $_REQUEST['pt'];
                $_REQUEST = (array)(json_decode(file_get_contents( 'php://input' )));
                $_REQUEST['pt'] = (!empty($pType))?$pType:$_REQUEST['transaction_type'];
                $this->_generateLog("=====notify Request=====" .date('Y-m-d H:i:s'));
                $this->_generateLog($_REQUEST);
                $orderId = (isset($_REQUEST['order_id']) && $_REQUEST['order_id']) ? $_REQUEST['order_id'] : "";
                update_post_meta( $orderId, 'notifyFlag', 'on');
                if(empty($orderId)){
                    $this->_generateLog('Notify Error : Order id not found.');    
                }                
                $order = new WC_Order($orderId);
            } else {
                $orderId = $woocommerce->session->get(self::SESSION_KEY);
                $order = new WC_Order($orderId);                
                update_post_meta( $orderId, 'sucessFlag', 'on');
            }            
            //
            // If the order has already been paid for
            // redirect user to success page
            //
            if ($order->needs_payment() === false)
            {
                $this->redirectUser($order);
                return;
            }

            $orderData = $this->getReddotTableData($orderId);
            $this->mid = $orderData->mid;
            $this->secret_key = $orderData->secret_key;
                        
            $transactionId = "";
            $success = false;
            $error = "";
            if($_REQUEST['a']=='back') {
                $error = 'User has cancelled the payment';
                wc_add_notice($error, 'error');
                wp_redirect(wc_get_checkout_url());
                exit;
            }
            else {                
                $requestMid = (isset($_REQUEST['request_mid']) && $_REQUEST['request_mid']) ? $_REQUEST['request_mid'] : "";
                $transactionId = (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id']) ? $_REQUEST['transaction_id'] : "";
                
                $payment_type = (isset($_REQUEST['pt']) && $_REQUEST['pt']) ? $_REQUEST['pt'] : "";
                $params = array(
                    "request_mid" => $this->mid,
                    "transaction_id" => $transactionId
                );                 
                $this->_signature = $this->_sign_generic($params,$this->secret_key);
                $params['signature'] = $this->_signature;
                
                if($payment_type=='HT') {
                    if($this->testmode){
                        $this->_query_redirect_api_url = 'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_token_redirection';
                    } else {
                        $this->_query_redirect_api_url = 'https://secure.reddotpayment.com/service/Merchant_processor/query_token_redirection';
                    }
                    
                }

                if($payment_type==self::SALE) {
                    if($_REQUEST['a']=='notify') {
                        $this->_generateLog('===Notify Arg===');
                        $this->_generateLog($params);
                        $this->_generateLog('secret_key => '.$this->secret_key);
                    }
                    $this->_generateLog("Under sale method Params :");
                    
                    $response = $this->_callApi($this->query_redirection_url, json_encode($params));   

                    if($response) {
                        $response = json_decode($response, true);                       

                        if($response) {
                            if($this->_compare_sign($response,$this->secret_key)) {
                                $success = true;
                                $this->updateOrder($order, $success, $error, $transactionId, $response);
                            }
                        }
                    }
                }
                elseif($payment_type==self::AUTHORIZE_CAPTURE) {
                    $success = true;
                    include_once dirname( __FILE__ ) . '/class-wc-capture.php';
                    $capture = new RD_Capture();
                    $paymentGatewayObject = $this;
                    $order->set_transaction_id($transactionId);
                    $order->save();
                    $capture->doCapture($order,$transactionId,$paymentGatewayObject,$payment_type);
                }
                elseif($payment_type==self::AUTHORIZE) {    
                    $this->_generateLog("Pre-Authorization : ");
                    $response = $this->_callApi($this->query_redirection_url, json_encode($params)); 
                    $response = json_decode($response, true);                    
                    if($response) { 
                        $success = true;
                        $this->updateOrder($order, $success, $error, $transactionId, $response);
                    }
                    
                }
            }
            if(!$success) {
                $success = false;
                $error = "Payment Failed.";
                //$woocommerce->add_error($error);
                wc_add_notice($error, 'error');

                //  Insert Request and response into the table 
                global $wpdb;
                $table_name = REDDOTTABLE;
                if(isset($orderId)){    
                    $wpdb->query( "UPDATE `".$table_name."` SET `response` = '".json_encode($response)."' WHERE `wp_reddot`.`orderid` = ".$orderId." ");       
                }
                //  Insert Request and response into the table 
                $this->updateOrder($order, $success, $error, $transactionId, $response);
                if($_REQUEST['a']!='notify') {
                    wp_redirect(wc_get_checkout_url());
                }
                exit;
            }            
        }
        
        public function payment_success() {
            // Getting POST data
            $postData = file_get_contents( 'php://input' );
            $response  = json_decode( $postData );
            $orderId = $_GET['orderId'];
            $order = wc_get_order( $orderId );
            
            if ($order && $response) {
                $order->update_meta_data( 'reddot_callback_payload', $postData );
                if ( $response->event === 'CHECKOUT_SUCCEEDED' ) {
                    $order->update_meta_data( 'reddot_event', $response->event );
                    if ($response->payload->reservations && $response->payload->reservations[0] && $response->payload->reservations[0]->reservationId) {
                        $order->update_meta_data( 'reddot_reservation_id', $response->payload->reservations[0]->reservationId );
                        $reservation_note = "Red Dot ReservationID: " . $response->payload->reservations[0]->reservationId;
                        $order->add_order_note( $reservation_note );
                        update_post_meta( $orderId, '_reddot_reservation_id', $response->payload->reservations[0]->reservationId );
                    }
                    $order->update_status( 'completed');
                    $order->payment_complete();
                    $order->reduce_order_stock();
                } else {
                    $order->update_meta_data( 'reddot_event', $response->event );
                    if ($response->payload->reservations && $response->payload->reservations[0] && $response->payload->reservations[0]->reservationId) {
                        $order->update_meta_data( 'reddot_reservation_id', $response->payload->reservations[0]->reservationId );
                    }
                    $order->update_status( 'failed');
                }
            }
        }
        
        public function payment_failure() {
            // Getting POST data
            $postData = file_get_contents( 'php://input' );
            $response  = json_decode( $postData );
            $orderId = $_GET['orderId'];
            $order = wc_get_order( $orderId );

            if ($order && $response) {
                $order->update_meta_data( 'reddot_callback_payload', $postData );
                $order->update_meta_data( 'reddot_event', $response->event );
                if ($response->payload->reservations && $response->payload->reservations[0] && $response->payload->reservations[0]->reservationId) {
                    $order->update_meta_data( 'reddot_reservation_id', $response->payload->reservations[0]->reservationId );
                }
                $order->update_status( 'failed');
            }
        }
        
        /**
         * Modifies existing order and handles success case
         *
         * @param $success, & $order
         */
        public function updateOrder(& $order, $success, $errorMessage, $transactionId, $response='', $webhook = false)
        {
            global $woocommerce;

            $this->_generateLog("=====Update Order Fun Response Start=====");            

            $orderId = $order->get_order_number();            
            if (($success === true) and ($order->needs_payment() === true))
            {                
                if($response['response_code']=='0') {

                    /* Get order request and response data */
                    global $wpdb;

                    $orderData = $this->getReddotTableData($orderId);
                    $responseData = json_decode($orderData->response);
                    $cardTable = REDDOT_CARD_DETAILS_TABLE;
                    $currentUserID = get_post_meta( $orderId, '_customer_user', true );
                    
                    $sqlcardData = $wpdb->get_results("SELECT * FROM ".$cardTable." WHERE (first_6 = '".$responseData->first_6."' AND last_4 = '".$responseData->last_4."' AND mid = '".$responseData->mid."' AND customer_id = '".$currentUserID."' AND payment_type = '".$responseData->transaction_type."')");
                    
                    /* Save card data into the card table */
                    $this->saveCardData($currentUserID,$sqlcardData,$responseData,$responseData->transaction_type);
                    /* Save card data into the card table */

                    if($response['transaction_type'] == 'S'){
                        $this->saveSuccessMetaByMethod($orderId,$response['transaction_type']);
                        // $order->payment_complete($transactionId);
                        $order->set_transaction_id($transactionId);
                        $order->add_order_note("<b>Red Dot Payment</b><br>
                            Payment success <br>
                            Transaction Id: ".$transactionId
                        );

                        $order->update_status( 'processing');                        
                        $order->save();

                        $msg = 'Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be processing your order soon.';
                        //$woocommerce->add_message($msg);
                        wc_add_notice($msg, 'success');
                    } else if ($response['transaction_type'] == 'A') {

                        $this->saveSuccessMetaByMethod($orderId,$response['transaction_type']);

                        // Trigger transactional email to client
                        // add_filter('woocommerce_email_recipient_new_order', 'reddot_add_customer_email_new_order_email_recipient', 1, 2);
                        $email = WC()->mailer()->emails['WC_Email_New_Order'];
                        $email->trigger( $orderId );
                        $this->_generateLog("Pre-auth New order email sent Email IDs!");
                        $this->_generateLog($email->get_recipient());
                        $this->_generateLog("Pre-auth New order email sent Successful!");

                        $order->add_order_note("
                            <b>Red Dot Payment</b><br>
                            Authorization success, please send capture action to collect the payment <br>
                            Transaction Id: ".$transactionId
                        );
                        $order->set_transaction_id($transactionId);
                        $order->update_status( 'review');
                        $order->save();
                        wc_add_notice("Thank you for your order. We will update order status when we capture order amount.", 'success');                        
                    }
                    
                    if (isset($woocommerce->cart) === true)
                    {
                        $woocommerce->cart->empty_cart();
                    }

                    $this->redirectUser($order);
                    exit;
                }
                else {
                    // $msg = $response['acquirer_response_msg'];
                    $msg = "Your payment has failed, please try again.";
                    //$woocommerce->add_message($msg);
                    wc_add_notice($msg, 'error');

                    $order->set_transaction_id($transactionId);

                    if($response['transaction_type'] == 'S'){
                        $order->add_order_note(" 
                            <b>Red Dot Payment</b><br>
                            Payment failed <br>
                            Transaction Id: ".$transactionId
                        );
                    } else if($response['transaction_type'] == 'A'){
                        $order->add_order_note(" 
                            <b>Red Dot Payment</b><br>
                            Authorization failed <br>
                            Transaction Id: ".$transactionId
                        );
                    }
                    
                    $order->save();
                    wp_redirect(wc_get_page_permalink( 'checkout' ));
                    exit;                    
                }
            }
            else
            {
                wc_add_notice($errorMessage, 'error');
                if ($transactionId)
                {          
                    $order->set_transaction_id($transactionId);          
                    $order->add_order_note("Payment Failed. <br/>Transaction Id: $transactionId");
                    $order->add_order_note('Failure Response: '.json_encode($response));
                    $order->save();
                }
                $order->update_status('failed');
            }            

            $this->_generateLog("=====Update Order Fun Response End=====");
        }

        /* Function which save meta value for all Transaction Type (Direct,pre-auth,and capture) 
        Like for sale = sale_success , pre-auth = auth_success, auth-capture = capture_succcess */
        public function saveSuccessMetaByMethod($orderId,$transactionType)
        {
            $tType = '';
            if(isset($transactionType) && $transactionType == 'S'){
                $tType = 'sale';
            } else if(isset($transactionType) && $transactionType == 'A'){
                $tType = 'authorize';
            }
            update_post_meta( $orderId, $tType.'_success', 'success');

            return;
        }

        protected function verifyReddotResponse($orderId)
        {
            global $wpdb,$woocommerce;

            $this->_generateLog('VerifyReddotResponse of Order ID :'.$orderId);

            $order = new WC_Order($orderId);
            $orderData = $this->getReddotTableData($orderId);

            /* Call api and verify response for transaction_id */
            $transactionId = (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id']) ? $_REQUEST['transaction_id'] : "";
            $requestMid = $orderData->mid;

            $params = array(
                "request_mid" => $requestMid,
                "transaction_id" => $transactionId
            );                 
            $this->_signature = $this->_sign_generic($params,$orderData->secret_key);
            $params['signature'] = $this->_signature;

            $response = $this->_callApiVerify($this->query_redirection_url, json_encode($params));

            $responseData = json_decode($response,true);

            $this->_generateLog('VerifyReddotResponse of ResponseData :=>');
            $this->_generateLog($responseData['acquirer_response_msg']);
            /* Call api and verify response for transaction_id */

            if(isset($responseData['response_code']) && $responseData['response_code'] != '0'){
                // wc_add_notice($responseData['acquirer_response_msg'], 'error');
                wc_add_notice("Payment failed, please retry", 'error');
                wp_redirect(wc_get_page_permalink( 'checkout' ));
                exit;    
            } else {
                $this->redirectUser($order);
                exit;
            }              
        }

        // Get Reddot Table data from order id. // 
        public function getReddotTableData($orderID)
        {
            global $wpdb;
            $reddotTable = REDDOTTABLE;
            $sql = $wpdb->get_results("SELECT * FROM ".$reddotTable." WHERE (orderid = '".$orderID."' AND pt != 'AC' AND pt != 'R' ) ORDER BY id DESC LIMIT 0,1");
            if(isset($sql[0]) && !empty($sql[0])){
                $orderData = $sql[0];                
                return $orderData;    
            }            
        }

        // Get Payment Information From Reddot Table to shwo at admin side and in email  
        public function getPaymentInformationFromReddotTable($orderID)
        {
            global $wpdb;
            $reddotTable = REDDOTTABLE;

            $sql = $wpdb->get_results("SELECT * FROM ".$reddotTable." WHERE (orderid = '".$orderID."' AND pt != 'AC' AND pt != 'R' AND (response_type='".RESPONSE_TYPE_NOTIFY."' || response_type='".RESPONSE_TYPE_SUCCESS."')) ORDER BY id DESC LIMIT 0,1 ");

            return $sql;
        }

        /* Save card data into the card table */ 
        protected function saveCardData($CurrentUserID,$sqlcardData,$responseData,$payment_type)
        {            
            $this->_generateLog('saveCardData Current user id =>'.$CurrentUserID);
            global $wpdb;
            
            $cardTable = REDDOT_CARD_DETAILS_TABLE;

            if($CurrentUserID != 0){
                $payer_id = (isset($responseData->payer_id) && $responseData->payer_id) ? $responseData->payer_id : "";
                if(!empty($sqlcardData[0]) && $payer_id != ''){
                    $wpdb->query( "UPDATE `".$cardTable."` SET 
                        `transaction_id` = '".$responseData->transaction_id."',
                        `order_id` = '".$responseData->order_id."',
                        `payer_id` = '".$payer_id."',
                        `created_date` = '".date('Y-m-d h:i:s')."'
                        WHERE `".$cardTable."`.`id` = ".$sqlcardData[0]->id." "); 
                } else {
                    if(isset($responseData->payer_id)){
                        $carddata=array(
                            'customer_id' => $CurrentUserID, 
                            'mid' => $responseData->mid,
                            'transaction_id' => $responseData->transaction_id,
                            'order_id' => $responseData->order_id,
                            'first_6' => $responseData->first_6,
                            'last_4' => $responseData->last_4,
                            'payment_mode' => $responseData->payment_mode,
                            'payer_id' => $payer_id,
                            'payment_type' => $payment_type);
                        $wpdb->insert( $cardTable, $carddata);  
                    }                
                }                        
            }
            return;
        }

        protected function redirectUser($order)
        {
            $redirectUrl = $this->get_return_url($order);
            $this->_generateLog('redirectUser =>'.$redirectUrl);
            $this->_generateLog('========Debug end redirectUrl=======');
            wp_redirect($redirectUrl);
            exit;
        }
        
        public function order_received_text( $text, $order ) {
            if ($order && $this->id === $order->get_payment_method() ) {
                return esc_html__( 'Thank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. Log into your PayPal account to view transaction details.', 'woocommerce' );
            }
            return $text;
        }   

        public function get_platform_information() {            
            
            $platformData = array();
            if( !function_exists('get_plugin_data') ){
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            }
            
            $plugin_data = get_plugin_data( plugin_dir_path( __DIR__ ).'woocommerce-gateway-reddot.php');

            $plugin_version = $plugin_data['Version'];

            $platformData['php_version'] = phpversion();
            $platformData['cms_name'] = 'wordpress';
            $platformData['cms_version'] = get_bloginfo('version');
            $platformData['plugin_version'] = $plugin_version;
             
            return json_encode($platformData);
        }     

        public function get_token_api_url() {
            return $this->token_api_url;
        }

        public function get_refund_payment_url() {
            return $this->refund_payment_url;
        }

        public function process_refund( $order_id, $amount = null, $reason = '' ) 
        {            
            $this->_generateLog('======== Debug Call Api Refund fun =======');
            $order = new WC_Order($order_id);
            $orderData = $this->getReddotTableData($order_id);
            $responseData = json_decode($orderData->response);

            if(empty($_REQUEST['refund_amount']) || $_REQUEST['refund_amount'] <= 0) {
                throw new Exception( __( 'Refund amount must be greater than 0. Please try again.', 'woocommerce' ) );
                return false;
            }

            $args = array(
                "mid"=> $responseData->mid,
                "order_number"=> $responseData->order_id,
                "transaction_id"=> $responseData->transaction_id,
                "action_type"=>"refund",
                "response_type"=>"json",
                "currency"=> $responseData->request_ccy,
                "amount"=> $_REQUEST['refund_amount']
            );

            $args['signature'] = $this->refund_signature($orderData->secret_key,$args);
            $reqdata = json_encode($args);
            $refund_api_url = $this->get_refund_payment_url();
            $postfields = substr((http_build_query($args) . "&"), 0, -1);

            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $refund_api_url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

            $json_response = curl_exec($ch);
            $curl_errno = curl_errno($ch);
            $curl_err = curl_error($ch);
            curl_close($ch);

            $response = json_decode($json_response, true);

            $this->_generateLog('Refund Request :');
            $this->_generateLog($reqdata);

            /* Insert Request and response into the table For order */
            global $wpdb;
            $table_name = REDDOTTABLE;
            $orderResData = $response;            

            $pt = 'R';    
            if(!empty($orderResData)){
                $tabledata=array(
                    'orderid' => $orderResData['order_number'], 
                    'mid' => $orderResData['mid'],
                    'secret_key' => $orderData->secret_key,
                    'pt' => $pt,
                    'request' => $reqdata,
                    'response' => $json_response,
                    'response_code' => $orderResData['reason_code'],
                    'response_msg' => $orderResData['result_status']
                );                

                $wpdb->insert( $table_name, $tabledata);
            }
            /* Insert Request and response into the table For order */

            $this->_generateLog("Refund Response: ");
            $this->_generateLog($response);

            if(!empty($response['result_status']) && $response['result_status'] == self::REFUND_FAILED){                
                $orderNoteMessage = "<b>Red Dot Payment</b><br>Refund request Failed";
                $order->add_order_note($orderNoteMessage, true);
                $order->save();
                update_post_meta( $order_id, 'refund_status', json_encode($response));
                return false;
            } else if(!empty($response['result_status']) && $response['result_status'] == self::REFUND_ACCEPTED){
                if (!empty($order)) {
                    $orderNoteMessage = "<b>Red Dot Payment</b><br>";
                    $orderNoteMessage .= "Refund request successful <br>";    
                    $orderNoteMessage .= "Refund amount: ".$_REQUEST['refund_amount']." ";   
                    $order->add_order_note($orderNoteMessage);                
                    $order->save();   
                }
                update_post_meta( $order_id, 'refund_status', json_encode($response));
                return true;
            } else {
                $this->_generateLog("Refund result_status not found");
                throw new Exception( __($curl_err, 'woocommerce' ) );
                return false;   
            }   
        }
    }
}
/* End of plugin class and plugins_loaded action hook */