<?php 
/* add new sub menu of RDP Plugin Support Form to send email*/
add_action('admin_menu', 'register_my_custom_submenu_page',99);
function register_my_custom_submenu_page() {
    add_submenu_page( 'woocommerce', 'RDP Plugin Support', 'RDP Plugin Support', 'manage_options', 'rdp-support-submenu-page', 'rdp_support_submenu_page_callback'); 
}
function rdp_support_submenu_page_callback() {
    echo '<h3>RDP Plugin Support</h3>';

    $current_user = wp_get_current_user();
    $fromEmail = esc_html( $current_user->user_email );
    $fromName = esc_html( $current_user->display_name );
    
    $reddotSettings = get_option('woocommerce_reddot_settings');  
    $toEmail = $reddotSettings['rdp_email_address'];

    $emailFlag = false;

    if(isset($_REQUEST['rdp_support_submit'])){
        if(empty($_REQUEST["rdp_support_to_email"])) {
            $emailFlag = true;
            $emailErr = "Email is required";
        } else {
            $email = htmlspecialchars(stripslashes(trim($_REQUEST["rdp_support_to_email"])));
            // check if e-mail address is well-formed
            $email = (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email)) 
        ? FALSE : TRUE; 
            if(!$email){
                $emailFlag = true;
                $emailErr = "Invalid email format.";
            }
        }

        if(empty($_REQUEST["rdp_support_to_subject"])){
            $emailFlag = true;
            $emailErr = "Subject is required";
        }

        if($emailFlag){
            ?>
            <div class="notice notice-success  is-dismissible">
                 <p><?php echo $emailErr; ?></p>
             </div>
            <?php
        } else {

            $redDot = new WC_Reddot();
            $platformData = $redDot->get_platform_information();
            include_once reddot_plugin_path() . '/function.php';
            $emailDetails = new RD_Function();
            $platformhtml = $emailDetails->getPlatformDetails(json_decode($platformData)); 

            $headers = array('Content-Type: text/html; charset=UTF-8');
            $arr = explode(',', $_REQUEST['rdp_support_to_email']);
            foreach ($arr as $value) {
                $multiple_recipients[] = trim($value);
            }
            
            $_REQUEST['rdp_support_to_body'] = nl2br(htmlspecialchars($_REQUEST['rdp_support_to_body']));
            $_REQUEST['rdp_support_to_body'] .= '<br><br>'.$platformhtml;

            if ( wp_mail($multiple_recipients, $_REQUEST['rdp_support_to_subject'], $_REQUEST['rdp_support_to_body'] , $headers ) ) {
                ?>
                <div class="notice notice-success  is-dismissible">
                     <p>Email sent successfully to RDP Plugin support.</p>
                 </div>
                <?php
            } else {
                ?>
                <div class="notice notice-success  is-dismissible">
                     <p>Something went wrong!</p>
                 </div>
                <?php
            }                
        }
        
    }
    ?>
    <form name="rdp_support" action="" method="post" id="rdp_support">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="rdp_support_from_email">From Email*<span class="woocommerce-help-tip"></span></label>
                    </th>
                    <td class="forminp forminp-text">
                        <input name="rdp_support_from_email" id="rdp_support_from_email" type="text" style="width: 470px;" value="<?php echo $fromEmail; ?>" class="" placeholder="" readonly>                           
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="rdp_support_from_name">From Name*<span class="woocommerce-help-tip"></span></label>
                    </th>
                    <td class="forminp forminp-text">
                        <input name="rdp_support_from_name" id="rdp_support_from_name" type="text" style="width: 470px;" value="<?php echo $fromName; ?>" class="" placeholder="" readonly>                           
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="rdp_support_to_email">To Email*<span class="woocommerce-help-tip"></span></label>
                    </th>
                    <td class="forminp forminp-text">
                        <input name="rdp_support_to_email" id="rdp_support_to_email" type="text" style="width: 470px;" value="<?php echo $toEmail; ?>" class="" placeholder="">                           
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="rdp_support_to_subject">Subject*<span class="woocommerce-help-tip"></span></label>
                    </th>
                    <td class="forminp forminp-text">
                        <input name="rdp_support_to_subject" id="rdp_support_to_subject" type="text" style="width: 470px;" value="RDP Plugin Support" class="" placeholder="" readonly>                           
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="rdp_support_to_body">Body<span class="woocommerce-help-tip"></span></label>
                    </th>
                    <td class="forminp forminp-text">   
                        <textarea name="rdp_support_to_body" id="rdp_support_to_body" style="min-width: 50%; height: 100px;" class="" placeholder=""></textarea>                        
                    </td>
                </tr>
                <tr valign="top">
                    <td class="forminp forminp-text">
                        <button name="rdp_support_submit" class="button-primary woocommerce-save-button" type="submit" value="Send Email">Send Email</button>                         
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    <?php
}
/* add new sub menu of RDP Plugin Support Form to send email*/