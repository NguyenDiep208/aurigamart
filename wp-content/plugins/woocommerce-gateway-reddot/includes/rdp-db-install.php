<?php 

/* Create table to save reddot request and response */
register_activation_hook( reddot_plugin_path().'/woocommerce-gateway-reddot.php', 'reddot_plugin_create_db' );
function reddot_plugin_create_db() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $reddotTable = REDDOTTABLE;
    $cardTable = REDDOT_CARD_DETAILS_TABLE;
    $logTable = REDDOT_LOG_TABLE;

    $reddotsql = "CREATE TABLE $reddotTable (
        id int(11) NOT NULL AUTO_INCREMENT,
        orderid varchar(255) NOT NULL,
        mid varchar(255) NOT NULL,
        secret_key varchar(255) NOT NULL,
        pt varchar(255) NOT NULL,
        request text NOT NULL,
        response text NOT NULL,
        response_code varchar(255) NOT NULL,
        response_type varchar(255) NOT NULL,
        response_msg varchar(255) NOT NULL,
        created_timestamp datetime NOT NULL,
        created_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
        UNIQUE KEY id (id)
    ) $charset_collate;";

    $cardsql = "CREATE TABLE $cardTable (
        id int(11) NOT NULL AUTO_INCREMENT,
        customer_id varchar(255) NOT NULL,
        mid varchar(255) NOT NULL,
        transaction_id varchar(255) NOT NULL,
        order_id varchar(255) NOT NULL,
        first_6 varchar(255) NOT NULL,
        last_4 varchar(255) NOT NULL,
        payment_mode varchar(255) NOT NULL,
        payer_id varchar(255) NOT NULL,        
        payment_type varchar(255) NOT NULL,
        created_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
        UNIQUE KEY id (id)
    ) $charset_collate;";

    $logTable = "CREATE TABLE $logTable (
        id int(11) NOT NULL AUTO_INCREMENT,
        order_id varchar(255) NOT NULL,
        mid varchar(255) NOT NULL,
        transaction_id varchar(255) NOT NULL,  
        response_code varchar(255) NOT NULL,
        response_msg varchar(255) NOT NULL,
        response text NOT NULL,
        created_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
        UNIQUE KEY id (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $reddotsql );
    dbDelta( $cardsql );
    dbDelta( $logTable );

}
/* Create table to save reddot request and response */