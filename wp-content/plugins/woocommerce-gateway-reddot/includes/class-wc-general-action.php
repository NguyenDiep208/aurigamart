<?php 
/* ajax fuction to check reddot notify flag set or not */
add_action( 'wp_ajax_check_reddot_notifyFlag', 'check_reddot_notifyFlag' );
add_action( 'wp_ajax_nopriv_check_reddot_notifyFlag', 'check_reddot_notifyFlag' );
function check_reddot_notifyFlag(){
    
    $redirectURL = '';        
    if (isset($_POST["orderid"])) {

        $notifyFlag = get_post_meta( $_POST["orderid"], 'notifyFlag', true );        
        if(isset($notifyFlag) && $notifyFlag == 'on'){

            $reddpt = new WC_Reddot();
            $orderData = $reddpt->getReddotTableData($_POST["orderid"]);
            $responseData = json_decode($orderData->response, true);

            $order = new WC_Order($_POST["orderid"]);            

            if(isset($responseData['response_code']) && $responseData['response_code'] != '0'){ 
                $result['type'] = "error";               
                wc_add_notice($responseData['acquirer_response_msg'], 'error');
                $redirectURL = wc_get_page_permalink( 'checkout' );
            } else {
                $result['type'] = "success";
                $redirectURL = $reddpt->get_return_url($order);
            }             
            
            $result['url'] = $redirectURL;
            $result = json_encode($result);
            echo $result;
            die;
        } else {
            echo 'still check for notify';
            die;
        }
    }
}
/* ajax fuction to check reddot notify flag set or not */

// Register new order status " Payment Review " */ 
add_filter( 'woocommerce_register_shop_order_post_statuses', 'reddot_register_review_order_status' );
 
function reddot_register_review_order_status( $order_statuses ){
    
   // Status must start with "wc-"
   $order_statuses['wc-review'] = array(                                 
   'label'                     => _x( 'Payment Review', 'Order status', 'woocommerce' ),
   'public'                    => false,                                 
   'exclude_from_search'       => false,                                 
   'show_in_admin_all_list'    => true,                                 
   'show_in_admin_status_list' => true,                                 
   'label_count'               => _n_noop( 'Payment Review <span class="count">(%s)</span>', 'Payment Review <span class="count">(%s)</span>', 'woocommerce' ),                              
   );      
   return $order_statuses;
}
 
// ---------------------
// 2. Show Order Status in the Dropdown @ Single Order and "Bulk Actions" @ Orders
 
add_filter( 'wc_order_statuses', 'reddot_show_review_order_status' );
 
function reddot_show_review_order_status( $order_statuses ) {      
   $order_statuses['wc-review'] = _x( 'Payment Review', 'Order status', 'woocommerce' );       
   return $order_statuses;
}
 
// ---------------------
// 3. Set Custom Order Status @ WooCommerce Checkout Process
 
// End Register new order status " Payment Review " */ 

/* add message on thankyou page for guest user to register in site if you want to save your card details */
add_filter('woocommerce_thankyou_order_received_text', 'reddot_change_order_received_text', 10, 2 );
function reddot_change_order_received_text( $str, $order ) {

    if(!empty($order)){
        $orderPaymentType = get_post_meta( $order->get_id(), '_payment_method', true );
        if($orderPaymentType == 'reddot'){
            $reddpt = new WC_Reddot();    
            $orderData = $reddpt->getReddotTableData($order->get_id());
            $responseData = json_decode($orderData->response, true);
            $payerID = (isset($responseData['payer_id']) && $responseData['payer_id']) ? $responseData['payer_id'] : "";
            $reddpt->_generateLog('Payer ID is => '.$payerID);
            $reddpt->_generateLog('Current user id is => '.get_current_user_id());
            if(get_current_user_id() == 0 && isset($responseData['payer_id']) && $responseData['payer_id'] != ''){
                $new_str = $str . ' <br /> Your card has been tokenize, and to be able to use it for the subsequent transaction, Please register <a href="'.wp_registration_url().'">click here</a>';
            } else {
                $new_str = $str;
            }
            return $new_str;
        }
    }

    return $str;
    
}
/* add message on thankyou page for guest user to register in site if you want to save your card details */

add_action('save_post', 'capture_save_order', 10, 3);
function capture_save_order($orderId, $post, $update){
    if(is_admin()){
            if(isset($_POST['captureReddotPayment']) && $_POST['captureReddotPayment'] == true){
                $order = wc_get_order( $orderId );
                // echo $order->get_transaction_id();die;
                include_once dirname( __FILE__ ) . '/class-wc-capture.php';
                $paymentGatewayObject = new WC_Reddot();
                $capture = new RD_Capture();
                $capture->doCapture($order,$order->transaction_id,$paymentGatewayObject,'A');
            }
    }
}

function register_shipping_order_status() {
    register_post_status( 'wc-shipping', array(
        'label'                     => 'Shipping',
        'public'                    => true,
        'show_in_admin_status_list' => true,
        'show_in_admin_all_list'    => true,
        'exclude_from_search'       => false,
        'label_count'               => _n_noop( 'Shipping <span class="count">(%s)</span>', 'Shipping <span class="count">(%s)</span>' )
    ) );
    register_post_status( 'wc-packing', array(
        'label'                     => 'Packing',
        'public'                    => true,
        'show_in_admin_status_list' => true,
        'show_in_admin_all_list'    => true,
        'exclude_from_search'       => false,
        'label_count'               => _n_noop( 'Packing <span class="count">(%s)</span>', 'Packing <span class="count">(%s)</span>' )
    ) );
}
add_action( 'init', 'register_shipping_order_status' );
function add_shipping_to_order_statuses( $order_statuses ) {
    $new_order_statuses = array();
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-shipping'] = 'Shipping';
            $new_order_statuses['wc-packing'] = 'Packing';
        }
    }
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_shipping_to_order_statuses' );

// Adding Meta container admin shop_order pages
add_action( 'add_meta_boxes', 'reddot_add_meta_boxes' );
if ( ! function_exists( 'reddot_add_meta_boxes' ) )
{
    function reddot_add_meta_boxes()
    {
        global $post;
        $orderPaymentType = get_post_meta( $post->ID, '_payment_method', true );
        if($orderPaymentType == 'reddot'){
            add_meta_box( 'reddot_other_fields', __('Payment Information','woocommerce'), 'reddot_add_other_fields_for_generic_information', 'shop_order', 'normal', 'core' );            
        }
    }
}

// Adding Meta field in the meta container admin shop_order pages for Generic Information of the payment response
if ( ! function_exists( 'reddot_add_other_fields_for_generic_information' ) )
{
    function reddot_add_other_fields_for_generic_information()
    {        
        global $post,$wpdb;
        $paymentGateway = new WC_Reddot();

        $table_name = REDDOTTABLE;
        $content = '<style>
                    .loader {
                      border: 8px solid #808080;
                      border-radius: 50%;
                      border-top: 8px solid #804040;
                      width: 16px;
                      height: 16px;
                      -webkit-animation: spin 2s linear infinite; /* Safari */
                      animation: spin 2s linear infinite;
                    }

                    /* Safari */
                    @-webkit-keyframes spin {
                      0% { -webkit-transform: rotate(0deg); }
                      100% { -webkit-transform: rotate(360deg); }
                    }

                    @keyframes spin {
                      0% { transform: rotate(0deg); }
                      100% { transform: rotate(360deg); }
                    }
                    .tooltip {
                      position: relative;
                      display: inline-block;
                      border-bottom: 1px dotted black;
                    }

                    .tooltip .tooltiptext {
                      visibility: hidden;
                      width: 220px;
                      background-color: #555;
                      color: #fff;
                      text-align: center;
                      border-radius: 6px;
                      padding: 2px 0;
                      position: absolute;
                      z-index: 1;
                      bottom: 125%;
                      left: -10%;
                      margin-left: -60px;
                      opacity: 0;
                      transition: opacity 0.3s;
                    }

                    .tooltip .tooltiptext::after {
                      content: "";
                      position: absolute;
                      top: 100%;
                      left: 50%;
                      margin-left: -5px;
                      border-width: 5px;
                      border-style: solid;
                      border-color: #555 transparent transparent transparent;
                    }

                    .tooltip:hover .tooltiptext {
                      visibility: visible;
                      opacity: 1;
                    }
                    </style>';

        $tokenized = 'No';
        $timestamp = '';
        $cardNo = '';
        $responseMsg = '';
        $caedType = '';
        $transactionType = '';
        $transactionId = '';
        $acquirerResponseCode = '';
        $acquirerResponseMsg = '';

        $sql = $paymentGateway->getPaymentInformationFromReddotTable($post->ID);
        if(!empty($sql[0])){
            $orderData = $sql[0];
            $requestData = json_decode($orderData->request);
            $responseData = json_decode($orderData->response);
            $cardTypeArray = PAYMENT_MODE_VALUE;    
        }        

        $order = wc_get_order( $post->ID );
        $status =  $order->get_status();
        $authorizeSuccess = get_post_meta( $order->get_id(), 'authorize_success', true );
        $captureSuccess = get_post_meta( $order->get_id(), 'capture_success', true );
        
        $isAllowWithPendingStatus = false;
        $isAllowWithProcessingStatus = true;
        $processingStatusArray = array('processing','packing','shipping','completed');

        if(isset($authorizeSuccess) && $authorizeSuccess == 'success') {
            $isAllowWithPendingStatus = true;
        }
        if(isset($captureSuccess) && $captureSuccess == 'success' && isset($authorizeSuccess) && $authorizeSuccess == 'success') {
            $isAllowWithProcessingStatus = false;
        }

        $showButton = '';
        if(isset($responseData->transaction_type) && ($responseData->transaction_type == 'A' && $status == 'review' && $isAllowWithPendingStatus) || 
            (isset($responseData->transaction_type) && $responseData->transaction_type == 'A' && in_array($status,$processingStatusArray) && $isAllowWithProcessingStatus)
            && $isAllowWithPendingStatus){
            $showButton .= '<button type="button" onclick=" document.getElementById(\'captureReddotPayment\').value=true;document.post.submit();" class="button generate-items">' . __( 'Capture Payment' ) . '</button>
            <input type="hidden" id="captureReddotPayment" name="captureReddotPayment" />';
        }
        
        $reddotSettings = get_option('woocommerce_reddot_settings'); 
        $to_email = explode(',', trim($reddotSettings['rdp_email_address']));

        $showButton .= '<button type="button" class="button generate-items tooltip" title="" onclick="getConfirmation('.$post->ID.');">Send logs<span class="tooltiptext">Send logs to RDP Plugin Support</span></button>
        <input type="hidden" class="sendlogemailids" value="'.implode(',', $to_email).'">';

        if(isset($responseData->transaction_type) && ($responseData->transaction_type == 'A' && in_array($status,$processingStatusArray) && $isAllowWithProcessingStatus && $isAllowWithPendingStatus)){
            $showButton .= '<div style="font-size:14px;margin-top:10px;"><i><strong>Note:</strong> It seems that order payment is yet not captured.</i></div>';
        }        

        $content .='<div style="width:90%;display:flex;padding:20px;">
            <div style="width:60%">';

        if(isset($responseData) && $responseData != '' ){
            
            if(isset($responseData->payer_id)){
                $tokenized = 'Yes ('.substr($responseData->payer_id,-4).')';
            }
            
            if(isset($responseData->created_timestamp)){
                $timestamp = date("Y-m-d H:i:s", strtotime($responseData->created_timestamp));
            }
            
            if(isset($responseData->response_msg)){
                $responseMsg = $responseData->response_msg;
            }
            
            if(isset($responseData->transaction_id)){
                $transactionId = $responseData->transaction_id;   
            }

            if(isset($responseData->transaction_type)){
                $transactionType = $paymentGateway->getTransactionTypes($responseData->transaction_type);   
            }

            if(isset($responseData->acquirer_response_code)){
                $acquirerResponseCode = '<tr>
                    <td style="padding-left:10px;">Acquirer Response Code</td>
                    <td style="padding-left:10px;">'.$responseData->acquirer_response_code.'</td>
                </tr>';  
            }

            if(isset($responseData->acquirer_response_msg)){
                $acquirerResponseMsg = '<tr>
                    <td style="padding-left:10px;">Acquirer Response Msg</td>
                    <td style="padding-left:10px;">'.$responseData->acquirer_response_msg.'</td>
                </tr>';  
            }

            if(isset($responseData->last_4)){
                $cardNo = $responseData->last_4;   
            }

            foreach ($cardTypeArray as $ck => $cv) {
                if(isset($responseData->payment_mode) && $ck == $responseData->payment_mode){
                    $caedType = $cv;
                }
            }    
        }
            
        $content .='<div id="postcustomstuff"><table id="list-table" style="width:100%">
            <thead>
            <tr>
                <th class="left">Name</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody id="the-list" data-wp-lists="list:meta">
            <tr>
                <td style="padding-left:10px;">Timestamp</td>
                <td style="padding-left:10px;">'.$timestamp.'</td>
            </tr>
            <tr>
                <td style="padding-left:10px;">Result</td>
                <td style="padding-left:10px;">'.$responseMsg.'</td>
            </tr>
            <tr>
                <td style="padding-left:10px;">Card Number</td>
                <td style="padding-left:10px;">'.$cardNo.'</td>
            </tr>
            <tr>
                <td style="padding-left:10px;">Card Type</td>
                <td style="padding-left:10px;">'.$caedType.'</td>
            </tr>
            <tr>
                <td style="padding-left:10px;">Tokenized</td>
                <td style="padding-left:10px;">'.$tokenized.'</td>
            </tr>
            <tr>
                <td style="padding-left:10px;">Transaction Type</td>
                <td style="padding-left:10px;">'.$transactionType.'</td>
            </tr>
            <tr>
                <td style="padding-left:10px;">Transaction Id</td>
                <td style="padding-left:10px;">'.$transactionId.'</td>
            </tr>
            '.$acquirerResponseCode.'
            '.$acquirerResponseMsg.'
            </tbody>
            </table></div>';
             
        $content .='</div>
            <div style="width:40%;text-align:right;">'.$showButton.'</div>
            <div class="loader" style="margin-left:10px;display:none;"></div>
            </div>';
        
        echo $content;
    }
}

add_action("wp_ajax_reddot_send_request_details", "reddot_send_request_details");
add_action("wp_ajax_nopriv_reddot_send_request_details", "reddot_send_request_details");

function reddot_send_request_details(){

    global $wpdb;

    if (isset($_POST["orderid"])) { 
        $order = wc_get_order( $_POST["orderid"] );

        $table_name = REDDOTTABLE;

        $sql = $wpdb->get_results("SELECT id,request,response,created_date FROM ".$table_name." WHERE (orderid = '".$_POST["orderid"]."') ORDER BY id DESC ");
        $orderId = $_POST["orderid"];

        /* Create pdf file for order request response*/
        include_once dirname( __FILE__ ).'/class-wc-pdf.php';
        $responsePdf = new RD_Pdf();
        $responsePdf->generateRequestResponsePdf($sql,$orderId);
        /* Create pdf file for order response*/

        /* Create pdf for order notes */            
        $notesPdf = new RD_Pdf();
        $notesPdf->generateOrderNotesPdf($orderId);
        /* Create pdf for order notes */
        
        /* Create pdf file for Platform Details */            
        $redDot = new WC_Reddot();
        $platformData = $redDot->get_platform_information();
        $platformPdf = new RD_Pdf();
        $platformPdf->generatePlatformDetails($platformData);
        /* Create pdf file for order request*/

        add_filter( 'woocommerce_email_attachments', 'attach_terms_conditions_pdf_to_email', 10, 3);
        
        $reddotSettings = get_option('woocommerce_reddot_settings'); 
        $to_email = explode(',', trim($reddotSettings['rdp_email_address']));
        // Trigger transactional email to client
        $email = WC()->mailer()->emails['WC_Reddot_Order_Email'];
        $email->trigger( $_POST["orderid"] ,$to_email);

        /* Remove files Request, Response, Notes, Platform Details File from the system */
        $requestresponseFilename = $_POST["orderid"].'_request_response.html';
        $notesFilename = $_POST["orderid"].'_notes.html';
        $requestresponse_pdf_path = reddot_plugin_path() . '/orderdata/requestresponse/'.$requestresponseFilename;
        $notes_pdf_path = reddot_plugin_path() . '/orderdata/notes/'.$notesFilename;

        unlink($requestresponse_pdf_path);
        unlink($notes_pdf_path);
        /* Remove files Request, Response, Notes, Platform Details File from the system */

        $result['type'] = "success";
        $result = json_encode($result);
        echo $result;
        die;
    }  
}

/* Add custom reddot order email template to send request details email */
function add_reddot_order_woocommerce_email( $email_classes ) {

    // include our custom email class
    require_once( 'class-wc-reddot-order-email.php' );

    // add the email class to the list of email classes that WooCommerce loads
    $email_classes['WC_Reddot_Order_Email'] = new WC_Reddot_Order_Email();
    return $email_classes;

}
add_filter( 'woocommerce_email_classes', 'add_reddot_order_woocommerce_email' );

/* Add request , response , platformdetails in the custom reddot order email template to send request details email */
function attach_terms_conditions_pdf_to_email ( $attachments , $id, $object ) {

    if (isset($_POST["orderid"])) { 
        $requestresponseFilename = $_POST["orderid"].'_request_response.html';
        $notesFilename = $_POST["orderid"].'_notes.html';
        $platformFilename = 'platformdetails.html';
        $requestresponse_pdf_path = reddot_plugin_path() . '/orderdata/requestresponse/'.$requestresponseFilename;
        $notes_pdf_path = reddot_plugin_path() . '/orderdata/notes/'.$notesFilename;
        $platform_pdf_path = reddot_plugin_path() . '/orderdata/'.$platformFilename;

        $attachments[] = $requestresponse_pdf_path;
        $attachments[] = $notes_pdf_path;
        $attachments[] = $platform_pdf_path;

        return $attachments;
    }    
}

// Adding to admin order list bulk dropdown a custom action 'Capture'
add_filter( 'bulk_actions-edit-shop_order', 'capture_bulk_actions_edit_product', 20, 1 );
function capture_bulk_actions_edit_product( $actions ) {
    $actions['capture'] = __( 'Capture', 'woocommerce' );
    return $actions;
}

// Make the action from selected orders
add_filter( 'handle_bulk_actions-edit-shop_order', 'capture_handle_bulk_action_edit_shop_order', 10, 3 );
function capture_handle_bulk_action_edit_shop_order( $redirect_to, $action, $post_ids ) {
    if ( $action !== 'capture' )
        return $redirect_to; // Exit

    global $attach_download_dir, $attach_download_file; // ???

    $processed_ids = array();

    foreach ( $post_ids as $post_id ) {
        $paymentGatewayObject = new WC_Reddot();
        $order = wc_get_order( $post_id );

        $sql = $paymentGatewayObject->getPaymentInformationFromReddotTable($post_id);

        if(!empty($sql[0])){
            $orderData = $sql[0];
            $responseData = json_decode($orderData->response);
        }

        
        $orderPaymentType = get_post_meta( $post_id, '_payment_method', true );
        
        $status = $order->get_status();
        $authorizeSuccess = get_post_meta( $order->get_id(), 'authorize_success', true );
        $captureSuccess = get_post_meta( $order->get_id(), 'capture_success', true );

        $isAllowWithPendingStatus = false;
        $isAllowWithProcessingStatus = true;
        $processingStatusArray = array('processing','packing','shipping','completed');

        if(isset($authorizeSuccess) && $authorizeSuccess == 'success') {
            $isAllowWithPendingStatus = true;
        }
        if(isset($captureSuccess) && $captureSuccess == 'success' && isset($authorizeSuccess) && $authorizeSuccess == 'success') {
            $isAllowWithProcessingStatus = false;
        }

        if($orderPaymentType == 'reddot' && isset($responseData->transaction_type) && ($responseData->transaction_type == 'A' && $status == 'review' && $isAllowWithPendingStatus) || (isset($responseData->transaction_type) && $responseData->transaction_type == 'A' && in_array($status,$processingStatusArray) && $isAllowWithProcessingStatus) && $isAllowWithPendingStatus){            

            $order_data = $order->get_data();
            // Your code to be executed on each selected order
            include_once dirname( __FILE__ ) . '/class-wc-capture.php';
            
            $capture = new RD_Capture();
            $isProcessed = $capture->doCapture($order,$order->transaction_id,$paymentGatewayObject,'A');
            if($isProcessed)
                $processed_ids[] = $post_id;
            else
                $failed_ids[] = $post_id;
        }        

    }

    return $redirect_to = add_query_arg( array(
        'capture' => '1',
        'processed_count' => count( $processed_ids ),
        'failed_count' => count( $failed_ids ),
        'processed_ids' => implode( ',', $processed_ids ),
        'failed_ids' => implode( ',', $failed_ids ),
    ), $redirect_to );
}

// The results notice from bulk action on orders for capture
add_action( 'admin_notices', 'capture_bulk_action_admin_notice' );
function capture_bulk_action_admin_notice() {
    if ( empty( $_REQUEST['capture'] ) ) return; // Exit

    $countprocessed = intval( $_REQUEST['processed_count'] );
    $countfailed = intval( $_REQUEST['failed_count'] );

    printf( '<div id="message" class="updated fade"><p>' .
        _n( 'Orders (IDs: %s ) have been successfully captured.',
        'Processed %s Orders for capture.',
        $_REQUEST['processed_ids'],
        'capture'
    ) . '</p></div>', $_REQUEST['processed_ids'] );

    if($countfailed > 0){
        printf( '<div id="message" class="updated fade"><p>' .
        _n( 'Orders (IDs: %s ) have been failed to capture.',
        'Failed %s Orders for capture.',
        $_REQUEST['failed_ids'],
        'capture'
    ) . '</p></div>', $_REQUEST['failed_ids'] );
    }    
}
// The results notice from bulk action on orders for capture

function reddot_admin_notice(){

    $refundStatus = '';
    if(isset($_REQUEST['post'])){
        $refundStatus = get_post_meta( $_REQUEST['post'], 'refund_status', true );    
    }
    
    $refundStatus = json_decode($refundStatus,true);
    if($refundStatus['result_status'] == 'failed'){
        echo '<div class="notice notice-warning is-dismissible">
         <p>Refund is failed please check notes for more information.</p>
     </div>';
    }
    if(isset($_REQUEST['post'])){
        delete_post_meta($_REQUEST['post'],'refund_status');    
    }
    
     
}
add_action('admin_notices', 'reddot_admin_notice');

// Function to change email address
function reddot_sender_email( $original_email_address ) {
    global $current_user;
    $current_user = wp_get_current_user();
    
    return $current_user->user_email;
}
 
// Function to change sender name
function reddot_sender_name( $original_email_from ) {
    global $current_user;
    $current_user = wp_get_current_user();

    return $current_user->display_name;
}
 
// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from', 'reddot_sender_email' );
add_filter( 'wp_mail_from_name', 'reddot_sender_name' );

function woocommerce_reddot_missing_wc_notice() {
    /* translators: 1. URL link. */
    echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Red Dot requires WooCommerce to be installed and active. You can download %s here.', 'woocommerce-gateway-reddot' ), '<a href="https://woocommerce.com/" target="_blank">WooCommerce</a>' ) . '</strong></p></div>';
}

add_action( 'woocommerce_cancel_unpaid_orders', 'reddot_wc_cancel_unpaid_orders' );

function reddot_wc_cancel_unpaid_orders() {
    global $wpdb;

    $held_duration = get_option( 'woocommerce_hold_stock_minutes' );

    if ( $held_duration < 1 || get_option( 'woocommerce_manage_stock' ) != 'yes' )
        return;

    $date = date( "Y-m-d H:i:s", strtotime( '-' . absint( $held_duration ) . ' MINUTES', current_time( 'timestamp' ) ) );
    // write your own code here.... 
    $statusArray = array('wc-pending','wc-review');
    $unpaid_orders = $wpdb->get_col( $wpdb->prepare( "
        SELECT posts.ID
        FROM {$wpdb->posts} AS posts
        WHERE   posts.post_type   IN ('" . implode( "','", wc_get_order_types() ) . "')
        AND     posts.post_status IN ('".implode( "','", $statusArray )."')
        AND     posts.post_modified < %s
    ", $date ) );

    if ( $unpaid_orders ) {
        foreach ( $unpaid_orders as $unpaid_order ) {
            $order = wc_get_order( $unpaid_order );

            if ( apply_filters( 'woocommerce_cancel_unpaid_order', 'checkout' === get_post_meta( $unpaid_order, '_created_via', true ), $order ) ) {
                $order->update_status( 'cancelled', __( 'Unpaid order cancelled - time limit reached.', 'woocommerce' ) );
            }
        }
    }

    wp_clear_scheduled_hook( 'woocommerce_cancel_unpaid_orders' );
    // do whatever you want here as well...
    wp_schedule_single_event( time() + ( absint( $held_duration ) * 60 ), 'woocommerce_cancel_unpaid_orders' );
}

add_action( 'woocommerce_email_footer', 'ts_email_footer', 10, 1 );
function ts_email_footer( $email ) {
    
    $footerHtml = '';
    $orderId = (isset($_REQUEST['orderid']) && $_REQUEST['orderid']) ? $_REQUEST['orderid'] : "";

    if(!empty($email) && $email->id == 'reddt_email'){

        include_once reddot_plugin_path() . '/function.php';
        $paymentInfo = new RD_Function();
        $paymentInfoHtml = $paymentInfo->getPaymentInfoForEmail($orderId);
        $footerHtml .= $paymentInfoHtml;

        $footerHtml .= "Thanks for using ".get_bloginfo( 'name' )."!"; 
    }

    echo $footerHtml;
}

function reddot_review_to_processing_notifications($order_id, $checkout=null) {
    global $woocommerce;
    $order = new WC_Order( $order_id );

    $reddpt = new WC_Reddot();

    $authorizeSuccess = get_post_meta( $order_id, 'authorize_success', true );

    $captureSuccess = get_post_meta( $order_id, 'capture_success', true );
    $reddpt->_generateLog("Status From Review to processing email notifications");
    if( isset($captureSuccess) && $captureSuccess == 'success' && $order->get_status() == 'processing' ) {

        // Trigger transactional email to client
        $email = WC()->mailer()->emails['WC_Email_Customer_Processing_Order'];
        $email->trigger( $order_id );
        $reddpt->_generateLog("email sent!");
    }
}
add_action("woocommerce_order_status_changed", "reddot_review_to_processing_notifications");