<?php 
function reddot_log_admin_scripts() {
	wp_enqueue_style( 'admin-css',  plugins_url(). '/woocommerce/assets/css/admin.css' );
}
add_action( 'admin_enqueue_scripts', 'reddot_log_admin_scripts' );
add_action('admin_menu', 'logs_menu',99);
function logs_menu() { 
    add_submenu_page('woocommerce','RDP Transaction Logs','RDP Transaction Logs','manage_options','view_logs','view_logs',100);
    // add_submenu_page('woocommerce','','','manage_options','view_logs_all','view_logs_all',100);
}
function get_log_path() {
	return __DIR__ .'/log';

}
function get_log_dir() {

	$log_path = get_log_path();
	$log_dir = array_filter(array_diff(scandir($log_path), array('..', '.')), function($item) {
    	return is_dir(get_log_path() . "/" . $item);
	});
	return $log_dir;
}
function view_logs() {
	$log_path = get_log_path();
	if(isset($_REQUEST['handle'])) {
		if(file_exists(get_log_path()."/".$_REQUEST['handle'])) {
			unlink(get_log_path()."/".$_REQUEST['handle']);
			$dir = substr($_REQUEST['handle'], 0, strpos($_REQUEST['handle'], '/') );
			$log_file = array_diff(scandir($log_path . '/'. $dir), array('..', '.'));
			
			if(empty($log_file)){
				rmdir($log_path . '/'. $dir);
			}

		}

	}
	$log_path = get_log_path();
	$log_dir = get_log_dir();

	$curr_dir = (isset($_REQUEST['log_dir'])) ? (isset($log_dir[$_REQUEST['log_dir']])? $log_dir[$_REQUEST['log_dir']]: '') : reset($log_dir);
	$log_file = array_diff(scandir($log_path . '/'. $curr_dir), array('..', '.'));
	$curr_file = (isset($_REQUEST['log_file'])) ? $log_file[$_REQUEST['log_file']] : reset($log_file);
	$viewed_log = $curr_dir . "/" . $curr_file;
	$_POST['log_file'] = $curr_file;
	if ( $log_dir ) : ?>
		<div class="wrap woocommerce">
		<div id="log-viewer-select">
			<div class="alignleft" >
				<h2>
					<div id="viewed_log"><?php echo esc_html( $viewed_log ); ?></div>
					<?php if ( ! empty( $viewed_log ) ) : ?>
						<a class="page-title-action" href="admin.php?page=view_logs&handle=<?php echo $viewed_log; ?>" class="button"><?php esc_html_e( 'Delete log', 'woocommerce' ); ?></a>
					<?php endif; ?>
				</h2>
			</div>
			<div class="loader-log" style="margin-left:10px;border: 8px solid #808080;border-radius: 50%;border-top: 8px solid #804040;width: 16px;height: 16px;-webkit-animation: spin 2s linear infinite;animation: spin 2s linear infinite;float:right;display:none;"></div>
			<div class="alignright">				
				<form action="<?php echo esc_url( admin_url( 'admin.php?page=view_logs' ) ); ?>" method="post">
					<select name="log_dir" class="log_dir">
						
						<?php foreach ( $log_dir as $log_key => $log_file ) : ?>
							<?php $selected = ($curr_dir == $log_file) ? 'selected' : '' ?>
							<option value="<?php echo esc_attr( $log_key ); ?>" <?php echo $selected; ?>><?php echo esc_html( $log_file ); ?> </option>
						<?php endforeach; ?>
					</select>
					<select name="log_file" class="log_file">
					</select>					
					<button type="submit" class="button" value="<?php esc_attr_e( 'View', 'woocommerce' ); ?>"><?php esc_html_e( 'View', 'woocommerce' ); ?></button>
				</form>
			</div>
			<div class="clear"></div>
		</div>
		<div id="log-viewer">
			<pre><?php echo esc_html( file_get_contents( get_log_path()."/".$viewed_log ) ); ?></pre>
		</div>
	<?php else : ?>
		<div class="updated woocommerce-message inline"><p><?php esc_html_e( 'There are currently no logs to view.', 'woocommerce' ); ?></p></div>
	<?php endif; ?>
		</div>
<?php
}

function view_logs_all() {
	$log_path = get_log_path();
	$log_dir = get_log_dir();

	$curr_dir = (isset($_REQUEST['log_dir'])) ? (isset($log_dir[$_REQUEST['log_dir']])? $log_dir[$_REQUEST['log_dir']]: '') : reset($log_dir);
	$log_file = glob($log_path.'/*.log');
	
	if(isset($_REQUEST['log_file'])){
		$viewed_log = $_REQUEST['log_file'];
	} else {
		$viewed_log = $log_file[0];	
	}
	

	if ( $log_file ) : ?>
		<div class="wrap woocommerce">
			<div class="alignright">
				<form action="<?php echo esc_url( admin_url( 'admin.php?page=view_logs_all' ) ); ?>" method="post">
					<select name="log_file" class="log_file">
						<?php foreach ( $log_file as $k => $val ) : ?>
							<?php //$selected = ($curr_dir == $log_file) ? 'selected' : '' ?>
							<option value="<?php echo esc_attr( $val ); ?>" ><?php echo esc_html( $val ); ?> </option>
						<?php endforeach; ?>
					</select>
					<button type="submit" class="button" value="<?php esc_attr_e( 'View', 'woocommerce' ); ?>"><?php esc_html_e( 'View', 'woocommerce' ); ?></button>
				</form>
			</div>
			<div class="clear"></div>
		</div>
		<div id="log-viewer">
			<pre><?php echo esc_html( file_get_contents( $viewed_log ) ); ?></pre>
		</div>
	<?php else : ?>
		<div class="updated woocommerce-message inline"><p><?php esc_html_e( 'There are currently no logs to view.', 'woocommerce' ); ?></p></div>
	<?php endif; ?>
<?php
}

// add_action( 'admin_footer', 'reddot_action_javascript' ); // Write our JS below here
add_action('admin_enqueue_scripts', 'reddot_action_javascript');

function reddot_action_javascript($hook) {

    if('woocommerce_page_view_logs' == $hook){
    	wp_enqueue_script( 'reddot_admin_js', plugins_url( '/js/reddot-admin.js', __DIR__ ), array('jquery') );
    }
}

add_action( 'wp_ajax_reddot_action', 'reddot_action' );

function reddot_action() {
	$log_dir = get_log_dir()[$_POST['log_dir']];
	$log_files = array_diff(scandir(get_log_path()."/".$log_dir), array('..', '.'));
	$options = null;
	$selectedValue = (isset($_POST['log_file'])) ? $_POST['log_file'] : '';
	foreach ($log_files as $key => $value) {
		$selected = ($selectedValue == $value) ? 'selected' : '';
		$options .= '<option value='.$key.' '.$selected.'>'.$value;
	}
	print_r($options);
	wp_die(); // this is required to terminate immediately and return a proper response
}
?>
