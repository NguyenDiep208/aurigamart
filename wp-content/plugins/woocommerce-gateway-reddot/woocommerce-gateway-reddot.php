<?php
/**
 * Plugin Name: WooCommerce Red Dot Payment Gateway
 * Plugin URI: https://wordpress.org/plugins/woocommerce-gateway-reddot/
 * Description: Simplifying online payments for a local and global world.
 * Author: RedDot
 * Author URI: https://reddotpayment.com/
 * Version: 2.1.4
 * Requires at least: 4.4
 * Tested up to: 5.3.0
 * WC requires at least: 2.6
 * WC tested up to: 3.8
 * Text Domain: woocommerce-gateway-reddot
 * Domain Path: /languages
 *
 */
if ( ! defined( 'ABSPATH' ) )
{
    exit; // Exit if accessed directly
}
/**
 * Required constants
 */
define("RESPONSE_TYPE_NOTIFY","notify");
define("RESPONSE_TYPE_SUCCESS","success");
define("PAYMENT_MODE_VALUE", 
    array('1'=>'Visa','2'=>'Mastercard','3'=>'Amex','4'=>'JCB','5'=>'UnionPay','6'=>'Diners','7'=>'Discover','8'=>'Laser',
          '9'=>'Maestro','10'=>'PayPal','11'=>'Alipay','12'=>'TenPay','13'=>'99Bill','14'=>'eNETS','15'=>'DBS PayLah!',
          '16'=>'FlashPay','17'=>'EZ-Link','18'=>'CashCard','19'=>'Voucher','20'=>'Others','21'=>'WeChat','22'=>'Konbini',
          '23'=>'VA BNI','24'=>'VA Permata','25'=>'VA Danamon','26'=>'VA Maybank','27'=>'VA Mandiri','28'=>'VA Sinarmas')
);
/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter( 'woocommerce_payment_gateways', 'reddot_add_gateway_class' );
function reddot_add_gateway_class( $gateways ) {
    $gateways[] = 'WC_Reddot'; // your class name is here
    return $gateways;
}
function reddot_plugin_path() {
  // gets the absolute path to this plugin directory
  return untrailingslashit( plugin_dir_path( __FILE__ ) );
}
add_filter( 'woocommerce_locate_template', 'reddot_woocommerce_locate_template', 10, 3 );
function reddot_woocommerce_locate_template( $template, $template_name, $template_path ) {
  global $woocommerce;
  $_template = $template;

  if ( ! $template_path ) $template_path = $woocommerce->template_url;
  $plugin_path  = reddot_plugin_path() . '/woocommerce/';
  // Look within passed path within the theme - this is priority
  $template = locate_template(
    array(
      $template_path . $template_name,
      $template_name
    )
  );

  // Modification: Get the template from this plugin, if it exists
  if ( ! $template && file_exists( $plugin_path . $template_name ) )
    $template = $plugin_path . $template_name;

  // Use default template
  if ( ! $template )
    $template = $_template;

  // Return what we found
  return $template;
}

global $wpdb;
define("REDDOTTABLE",$wpdb->prefix . 'reddot');
define("REDDOT_CARD_DETAILS_TABLE",$wpdb->prefix . 'reddot_card_detail');
define("REDDOT_LOG_TABLE",$wpdb->prefix . 'reddot_log');

/* include reddot class */
require('includes/rdp-db-install.php');

add_action('activated_plugin','reddot_save_error');
function reddot_save_error()
{
  global $wp_rewrite;
  $wp_rewrite->set_permalink_structure('/%postname%/');
  $wp_rewrite->flush_rules();
file_put_contents(dirname(__file__).'/error_activation.txt', ob_get_contents());
}

/* include reddot class */
require('includes/class-wc-reddot.php');

/* include all general action hook / filter */
require('includes/class-wc-general-action.php');

/* add new sub menu of RDP Plugin Support Form to send email */
require('includes/class-wc-rdp-support.php');

/* Manage my save cards and remove */
require('includes/class-wc-rdp-savecards.php');

/* View Transactional Logs */
require('includes/class-wc-transaction-logs.php');
?>
