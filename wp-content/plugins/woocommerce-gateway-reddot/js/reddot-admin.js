jQuery(document).ready(function($) {
    function getOptionsOnLoad() {

      var log_file = jQuery('#viewed_log').html();
      log_file = (log_file.split('/'))[1];
      var log_dir = jQuery('.log_dir').val();
      var data = {
        'action': 'reddot_action',
        'log_dir': log_dir, 
        'log_file': log_file
      };
      getOptions(data, log_dir);

    }
    function getOptionsOnChange() {
      var log_dir = jQuery('.log_dir').val();
      var data = {
        'action': 'reddot_action',
        'log_dir': log_dir
      };

      getOptions(data, log_dir);

    }
    function getOptions(data, log_dir) {
		jQuery('.loader-log').show();    		
      // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
      if(log_dir){
        jQuery.post(ajaxurl, data, function(response) {
           jQuery(".log_file").html(response);
           jQuery('.loader-log').hide();    		
        });
      }
    }
    getOptionsOnLoad();
    jQuery('.log_dir').change(getOptionsOnChange);
  });