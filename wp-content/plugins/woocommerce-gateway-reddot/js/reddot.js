function getConfirmation(orderid) {
    var sendlogemailids = jQuery(".sendlogemailids").val();
   var retVal = confirm("You are about to send the log detail to RDP Plugin support ("+sendlogemailids+"). Would you like to proceed?");
   if( retVal == true ) {
      jQuery('.loader').show();
      jQuery.ajax({
         type : "post",
         dataType : "json",
         url : myAjax.ajaxurl,
         data : {action: "reddot_send_request_details", orderid : orderid},
         success: function(response) {
            if(response.type == "success") {
               jQuery('.loader').hide();
               alert('Email sent successfully to RDP Plugin support.');
            }
            else {
               alert("Your orderid not found !");
            }
         }
      })   
   } else {
      return;
   }
}

function checkNotifyFlag(orderid) {
  jQuery('.delayloader').show();
  jQuery.ajax({
    type : "post",
    dataType : "json",
    url : myAjax.ajaxurl,
    data : {action: "check_reddot_notifyFlag", orderid : orderid},
    success: function(response) {
      jQuery('.delayloader').hide();
      if(response.type == "success") {
        // jQuery('.loader').hide();
        location.replace(response.url);
      } else {
        location.replace(response.url);
      }
    },
    complete: function() {
        // Schedule the next request when the current one's complete
        setInterval(checkNotifyFlag(orderid), 5000); // The interval set to 5 seconds
    }
  });  
}

jQuery('.removecard').click(function(){
    return confirm('Are you sure you want to delete?');
});
 