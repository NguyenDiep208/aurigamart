<?php

class RD_Function extends WC_Payment_Gateway
{

    public function getOrderNotesDetails($orderNotes)
    {
        
        $orderNotesHtml ='<div style="margin-bottom: 40px;"><table class="td" cellspacing="0" cellpadding="6" border="1" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; width: 100%; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;">
            <thead>
                <tr>
                    <th class="td" scope="col" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Order Status History</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($orderNotes as $notekey => $notevalue) {
                
                $orderNotesHtml .= '<tr class="order_item">
                        <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word;">'.$notevalue->content.'
                        <p class="meta" style="text-align:right;">
                            <abbr class="exact-date" title="'.esc_attr( $notevalue->date_created->date( "Y-m-d H:i:s" )).'">
                                '.$notevalue->date_created->date_i18n( wc_date_format()).'
                            </abbr>                        
                        </p></td>
                    </tr>';
            }
            $orderNotesHtml .= '</tbody>
            </table></div>';

            return $orderNotesHtml;
    }

    public function getPlatformDetails($platformData)
    {   
        $platformDataHtml ='<div style="margin-bottom: 40px;"><table class="td" cellspacing="0" cellpadding="6" border="1" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; width: 100%; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;">
            <thead>
                <tr>
                    <th class="td" scope="col" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;" colspan="2">Platform Information</th>
                </tr>
            </thead>
            <tbody>';

            foreach ($platformData as $platformkey => $platformvalue) {
                    
                    $platformDataHtml .= '<tr class="order_item">
                            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word;">'.$platformkey.'
                            </td>
                            <td class="td" style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word;">'.$platformvalue.'
                            </td>
                        </tr>';
                }
            $platformDataHtml .= '</tbody>
            </table></div>';

            return $platformDataHtml;
    }

    public function getPaymentInfoForEmail($orderId)
    {
        global $post,$wpdb;
        $paymentGateway = new WC_Reddot();

        $table_name = REDDOTTABLE;
        $content = '';
    
        $sql = $paymentGateway->getPaymentInformationFromReddotTable($orderId);

        $orderData = $sql[0];
        $requestData = json_decode($orderData->request);
        $responseData = json_decode($orderData->response);

        
        $cardTypeArray = PAYMENT_MODE_VALUE;

        $tokenized = 'No';
        $timestamp = '';
        $cardNo = '';
        $responseMsg = '';
        $caedType = '';
        $transactionType = '';
        $transactionId = '';
        $acquirerResponseCode = '';
        $acquirerResponseMsg = '';

        if(isset($responseData) && $responseData != '' ){
            
            if(isset($responseData->payer_id)){
                $tokenized = 'Yes ('.substr($responseData->payer_id,-4).')';
            }
            
            if(isset($responseData->created_timestamp)){
                $timestamp = date("Y-m-d H:i:s", strtotime($responseData->created_timestamp));
            }

            
            if(isset($responseData->response_msg)){
                $responseMsg = $responseData->response_msg;
            }
            
            if(isset($responseData->transaction_id)){
                $transactionId = $responseData->transaction_id;   
            }

            if(isset($responseData->transaction_type)){
                $transactionType = $paymentGateway->getTransactionTypes($responseData->transaction_type);   
            }

            if(isset($responseData->acquirer_response_code)){
                $acquirerResponseCode = '<tr>
                    <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Acquirer Response Code</th>
                    <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$responseData->acquirer_response_code.'</td>
                </tr>';  
            }

            if(isset($responseData->acquirer_response_msg)){
                $acquirerResponseMsg = '<tr>
                    <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Acquirer Response Msg</th>
                    <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$responseData->acquirer_response_msg.'</td>
                </tr>';  
            }

            if(isset($responseData->last_4)){
                $cardNo = $responseData->last_4;   
            }

            foreach ($cardTypeArray as $ck => $cv) {
                if(isset($responseData->payment_mode) && $ck == $responseData->payment_mode){
                    $caedType = $cv;
                }
            }    
        }

        $content .='<h2 style="color: #96588a; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 0 0 18px; text-align: left;">Payment Information</h2><div style="margin-bottom: 40px;"><table class="td" cellspacing="0" cellpadding="6" border="1" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; width: 100%; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;">
            <thead>
            <tr>
                <th class="td" scope="col" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Name</th>
                <th class="td" scope="col" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Value</th>
            </tr>
            </thead>
            <tbody>
            <tr class="order_item">
                <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Timestamp</th>
                <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$timestamp.'</td>
            </tr>
            <tr>
                <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Result</th>
                <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$responseMsg.'</td>
            </tr>
            <tr>
                <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Card Number</th>
                <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$cardNo.'</td>
            </tr>
            <tr>
                <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Card Type</th>
                <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$caedType.'</td>
            </tr>
            <tr>
                <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Tokenized</th>
                <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$tokenized.'</td>
            </tr>
            <tr>
                <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Transaction Type</th>
                <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$transactionType.'</td>
            </tr>
            <tr>
                <th class="td" scope="row" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">Transaction Id</th>
                <td class="td" style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">'.$transactionId.'</td>
            </tr>
            '.$acquirerResponseCode.'
            '.$acquirerResponseMsg.'
            </tbody>
            </table></div>';

        return $content;           
    }
}

