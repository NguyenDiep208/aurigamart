<?php
/**
 * Exit if accessed directly
 */
/*  Popup Style */
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}
?>
<style type="text/css">
    
    .mwb-pr-popup-tab-wrapper .tab-link:after {
      background-color: <?php echo $this->mwb_wpr_notification_addon_get_color();?> !important;
      border: 1px solid <?php echo $this->mwb_wpr_notification_addon_get_color();?> !important;
    }

    .mwb-pr-popup-tab-wrapper .tab-link {
      color: <?php echo $this->mwb_wpr_notification_addon_get_color();?>;
    }

    .mwb-pr-popup-tab-wrapper .tab-link.active {
      border: 1px solid <?php echo $this->mwb_wpr_notification_addon_get_color();?> !important;
      color: #ffffff !important;
    }

    .mwb-pr-popup-rewards-right-content a {
      color: <?php echo $this->mwb_wpr_notification_addon_get_color();?>;
    }

    .mwb-pr-close-btn:hover {
      color: #ffffff !important;
      background-color: <?php echo $this->mwb_wpr_notification_addon_get_color();?>;
    }

    .mwb-pr-popup-rewards-right-content a:hover {
      
      color: <?php echo $this->mwb_wpr_notification_addon_get_color();?>;
      opacity: .75;
    }    

</style>
<?php

$is_mwb_wpr_enabled = $this->mwb_wpr_general_setting_enable(); 
$get_points = 0;
$position_class = $this->mwb_wpr_enable_notification_addon_button_position();

if( $position_class ){
  $position = 'left: 10px !important';
}else{
  $position = 'right: 10px !important';
}


if( $this->mwb_wpr_check_enabled_notification_addon() && $is_mwb_wpr_enabled ) {

  $user_ID = get_current_user_ID();
  if(is_user_logged_in()){
    $mwb_guest_user = false;
    if(isset($user_ID) && !empty($user_ID)){
      $get_points = (int)get_user_meta($user_ID , 'mwb_wpr_points', true);
    }
  }else{
    $mwb_guest_user = true;
  }
  $order_conversion_rate = $this->mwb_wpr_order_conversion_rate();
  $referral_link = $this->mwb_wpr_get_referral_link($user_ID);
  $site_url = apply_filters('mwb_wpr_referral_link_url',site_url() );
  $purchase_product_using_pnt_rate = $this->mwb_wpr_purchase_product_using_pnt_rate();
  $apply_point_on_cart_rate = $this->mwb_wpr_apply_point_on_cart_rate();
  $convert_points_to_coupons_rate = $this->mwb_wpr_convert_points_to_coupons_rate();
  
  $button_text = $this->mwb_wpr_notification_button_text();
?>
    <div id="modal" class="modal modal__bg">
      <div class="modal__dialog mwb-pr-dialog">
        <div class="modal__content">
          <div class="modal__header">
            <div class="modal__title">
              <h2 class="modal__title-text"><?php esc_html_e('How to Earn More!','ultimate-woocommerce-points-and-rewards');?></h2>
            </div>
            <span class="mdl-button mdl-button--icon mdl-js-button  material-icons  modal__close"></span>
          </div>
          <div class="modal__text mwb-pr-popup-text">
            <div class="mwb-pr-popup-body">
              <?php ?>
                <div class="mwb-popup-points-label">
                  <h6><?php esc_html_e('Total Point: ','ultimate-woocommerce-points-and-rewards');?><?php echo $get_points; ?></h6>
                </div>
              <?php ?>
              <div class="mwb-pr-popup-tab-wrapper">
                <a class="tab-link active" id ="notify_user_gain_tab" href="javascript:;"><?php esc_html_e('Gain Points','ultimate-woocommerce-points-and-rewards');?></a>
                <a class="tab-link" href="javascript:;" id="notify_user_redeem"><?php esc_html_e('Redeem Points','ultimate-woocommerce-points-and-rewards');?></a>
              </div>
              <!--mwb-pr-popup- rewards tab-container-->
              <div class="mwb-pr-popup-tab-container active" id="notify_user_earn_more_section">
                <ul class="mwb-pr-popup-tab-listing mwb-pr-popup-rewards-tab-listing">
                  <?php if($this->mwb_wpr_check_signup_enable() && !is_user_logged_in()) {  ?>
                  <li>
                    <div class="mwb-pr-popup-left-rewards">
                      <div class="mwb-pr-popup-rewards-icon">
                        <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
                      </div>
                      
                        <div class="mwb-pr-popup-rewards-left-content">
                          <h6><?php esc_html_e('Register Your Self and Earn','ultimate-woocommerce-points-and-rewards');?></h6>
                          <span><?php echo $this->mwb_wpr_get_signup_value(); esc_html_e(' Points','ultimate-woocommerce-points-and-rewards');?></span>
                        </div>
                    </div>
                    <div class="mwb-pr-popup-right-rewards">
                      <div class="mwb-pr-popup-rewards-right-content">
                      </div>
                    </div>
                  </li>
                  <?php } 
                  if($this->mwb_wpr_is_order_conversion_enabled()){ ?>
                  <li>
                    <div class="mwb-pr-popup-left-rewards">
                      <div class="mwb-pr-popup-rewards-icon">
                        <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/shopping-cart.png';?>" alt="">
                      </div>
                      <div class="mwb-pr-popup-rewards-left-content">
                      <?php esc_html_e('Place The Order and Earn Points','ultimate-woocommerce-points-and-rewards');?>         
                        <span><?php esc_html_e('Earn ','ultimate-woocommerce-points-and-rewards'); echo $order_conversion_rate['Points']; esc_html_e(' Points on every ','ultimate-woocommerce-points-and-rewards');echo $this->mwb_wpr_value_return_in_currency($order_conversion_rate['Value']); esc_html_e(' spent','ultimate-woocommerce-points-and-rewards');?></span>
                      </div>
                    </div>
                    <div class="mwb-pr-popup-right-rewards">
                      <div class="mwb-pr-popup-rewards-right-content">
                      </div>
                    </div>
                  </li>
                  <?php } 
                  if($this->mwb_wpr_is_review_reward_enabled()){?>
                  <li>
                    <div class="mwb-pr-popup-left-rewards">
                      <div class="mwb-pr-popup-rewards-icon">
                        <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
                      </div>
                      <div class="mwb-pr-popup-rewards-left-content">
                        <h6><?php esc_html_e('Write Review ','ultimate-woocommerce-points-and-rewards');?></h6>
                        <span><?php esc_html_e('Reward is: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_get_reward_review(); esc_html_e(' Point','ultimate-woocommerce-points-and-rewards');?></span>
                      </div>
                    </div>
                    <div class="mwb-pr-popup-right-rewards">
                      <div class="mwb-pr-popup-rewards-right-content">
                      </div>
                    </div>
                  </li>
                  <?php } 
                  if($this->mwb_wpr_check_referal_enable()){?>
                  <li>
                    <div class="mwb-pr-popup-left-rewards">
                      <div class="mwb-pr-popup-rewards-icon">
                        <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
                      </div>
                      <?php if(!$this->mwb_wpr_is_only_referral_purchase_enabled()){ ?>
                      <div class="mwb-pr-popup-rewards-left-content">
                        <h6><?php esc_html_e('Refer Someone','ultimate-woocommerce-points-and-rewards');?></h6>
                        <span><?php esc_html_e('Reward is: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_get_referral_reward(); esc_html_e(' Point','ultimate-woocommerce-points-and-rewards');?></span>
                      </div>
                    <?php } else{ ?>
                      <div class="mwb-pr-popup-rewards-left-content">
                        <h6><?php esc_html_e('Referral Link','ultimate-woocommerce-points-and-rewards');?></h6>
                        <span><?php esc_html_e('Share this link and get reward on their purchase only ','ultimate-woocommerce-points-and-rewards');?></span>
                      </div>
                    <?php }?>
                    </div>
                    <?php if(!$mwb_guest_user){ ?>
                      <div class="mwb-pr-popup-right-rewards mwb-pr-popup-right-rewards--login">
                        <div class="mwb-pr-popup-rewards-right-content">
                          <span id="mwb_notify_user_copy"><?php echo $site_url.'?pkey='.$referral_link; ?></span>
                        </div>
                        <div class="mwb-pr-popup-rewards-right-content">
                          <button class="mwb_wpr_btn_copy mwb_tooltip" data-clipboard-target="#mwb_notify_user_copy" aria-label="copied">
                          <span class="mwb_tooltiptext"><?php esc_html_e('Copy','ultimate-woocommerce-points-and-rewards') ;?></span>
                          <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/copy.png';?>" alt="Copy to clipboard"></button>
                        </div>
                      </div>
                    <?php }?>
                  </li>
                  <?php } 
                  if($this->mwb_wpr_is_referral_purchase_enabled()) { ?>
                    <li>
                    <div class="mwb-pr-popup-left-rewards">
                      <div class="mwb-pr-popup-rewards-icon">
                        <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
                      </div>
                      <div class="mwb-pr-popup-rewards-left-content">
                        <h6><?php esc_html_e('Earn on Someonelse Purchasing ','ultimate-woocommerce-points-and-rewards');?></h6>
                        <span><?php esc_html_e('Reward is: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_get_referral_purchase_reward(); esc_html_e(' Point','ultimate-woocommerce-points-and-rewards');?></span>
                      </div>
                    </div>
                    <div class="mwb-pr-popup-right-rewards">
                      <div class="mwb-pr-popup-rewards-right-content">
                      </div>
                    </div>
                  </li>
                <?php } 
                if(!$this->mwb_wpr_check_signup_enable() && !$this->mwb_wpr_is_order_conversion_enabled() && !$this->mwb_wpr_is_review_reward_enabled() && !$this->mwb_wpr_is_referral_purchase_enabled() && !$this->mwb_wpr_check_referal_enable()){ ?>
                  <li>
                    <div class="mwb-pr-popup-left-rewards">
                      <div class="mwb-pr-popup-rewards-icon">
                      </div>
                      <div class="mwb-pr-popup-rewards-left-content">
                        <h6><?php esc_html_e('No Features Are Available Right Now! ','ultimate-woocommerce-points-and-rewards');?></h6>
                      </div>
                    </div>
                    <div class="mwb-pr-popup-right-rewards">
                      <div class="mwb-pr-popup-rewards-right-content">
                      </div>
                    </div>
                  </li>
                <?php }
                ?>
                </ul>
              </div><!--mwb-pr-popup- rewards tab-container-->
              <!--mwb-pr-popup- earn more tab-container-->
              <div class="mwb-pr-popup-tab-container" id="notify_user_redeem_section">
                <ul class="mwb-pr-popup-tab-listing">
                  <?php if($this->mwb_wpr_is_purchase_product_using_points_enabled()) { ?>
                  <li>
                    <div class="mwb-pr-tab-popup-icon">
                       <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/shopping-cart.png';?>" alt="">
                    </div>
                    <div class="mwb-pr-tab-popup-content">
                      <h6><?php esc_html_e('Redeem Points on Particluar Products','ultimate-woocommerce-points-and-rewards');?></h6>
                      <p><?php esc_html_e('Conversion Rule: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_return_conversion_rate($purchase_product_using_pnt_rate['Points'],$purchase_product_using_pnt_rate['Currency']);?></p>
                    </div>
                  </li>
                  <?php } 
                  if($this->mwb_wpr_is_apply_point_on_cart_enabled()){ ?>
                    <li>
                      <div class="mwb-pr-tab-popup-icon">
                        <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/shopping-cart.png';?>" alt="">
                      </div>
                      <div class="mwb-pr-tab-popup-content">
                        <h6><?php esc_html_e('Apply Points on Cart Total','ultimate-woocommerce-points-and-rewards');?></h6>
                        <p><?php esc_html_e('Conversion Rule: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_return_conversion_rate($apply_point_on_cart_rate['Points'],$apply_point_on_cart_rate['Currency']);?></p>
                      </div>
                    </li>
                  <?php } 
                  if($this->mwb_wpr_is_convert_points_to_coupon_enabled()) {?>
                  <li>
                    <div class="mwb-pr-tab-popup-icon">
                      <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
                    </div>
                    <div class="mwb-pr-tab-popup-content">
                      <h6><?php esc_html_e('Convert Points into Coupons','ultimate-woocommerce-points-and-rewards');?></h6>
                      <p><?php esc_html_e('Conversion Rule: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_return_conversion_rate($convert_points_to_coupons_rate['Points'], $convert_points_to_coupons_rate['Currency']); ?></p>
                    </div>
                  </li>
                  <?php }
                  if( !$this->mwb_wpr_is_convert_points_to_coupon_enabled() && !$this->mwb_wpr_is_apply_point_on_cart_enabled() && !$this->mwb_wpr_is_purchase_product_using_points_enabled() ){ ?>
                  <li>
                    <div class="mwb-pr-popup-left-rewards">
                      <div class="mwb-pr-popup-rewards-icon">
                      </div>
                      <div class="mwb-pr-popup-rewards-left-content">
                        <h6><?php esc_html_e('No Features Are Available Right Now! ','ultimate-woocommerce-points-and-rewards');?></h6>
                      </div>
                    </div>
                    <div class="mwb-pr-popup-right-rewards">
                      <div class="mwb-pr-popup-rewards-right-content">
                      </div>
                    </div>
                  </li>
                <?php }
                ?>
                </ul>
              </div>
            </div><!--mwb-pr-popup-body-->
          </div>
          <div class="modal__footer mwb-pr-footer">
            <a class="mdl-button mdl-button--colored mdl-js-button  modal__close mwb-pr-close-btn" style="border-color: <?php echo $this->mwb_wpr_notification_addon_get_color();?>; color:<?php echo $this->mwb_wpr_notification_addon_get_color();?>"> <?php esc_html_e('Close','ultimate-woocommerce-points-and-rewards');?>
            </a>
          </div>
        </div>
      </div>
    </div>
  <!--=====================================
  =            for mobile view            =
  ======================================-->
  <div class="mwb-pr-mobile-popup-main-container">
    <div class="mwb-pr-mobile-popup-wrapper">
     <div class="mwb-pr-popup-body">
       <?php  if(is_user_logged_in()){ ?>
          <div class="mwb-popup-points-label">
            <h6><?php esc_html_e('Total Point: ','ultimate-woocommerce-points-and-rewards');?><?php echo $get_points; ?></h6>
          </div>
        <?php } ?>
      <div class="mwb-pr-popup-tab-wrapper">
        <a class="tab-link active" id ="notify_user_gain_tab_mobile" href="javascript:;"><?php esc_html_e('Gain Points','ultimate-woocommerce-points-and-rewards');?></a>
        <a class="tab-link" href="javascript:;" id="notify_user_redeem_mobile"><?php esc_html_e('Redeem Points','ultimate-woocommerce-points-and-rewards');?></a>
      </div>
      <!--mwb-pr-popup- rewards tab-container-->
      <div class="mwb-pr-popup-tab-container active" id="notify_user_earn_more_section_mobile">
        <ul class="mwb-pr-popup-tab-listing mwb-pr-popup-rewards-tab-listing">
          <?php if($this->mwb_wpr_check_signup_enable() && !is_user_logged_in()) {  ?>
          <li>
            <div class="mwb-pr-popup-left-rewards">
              <div class="mwb-pr-popup-rewards-icon">
                <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
              </div>
              
                <div class="mwb-pr-popup-rewards-left-content">
                  <h6><?php esc_html_e('Register Your Self and Earn','ultimate-woocommerce-points-and-rewards');?></h6>
                  <span><?php echo $this->mwb_wpr_get_signup_value(); esc_html_e(' Points','ultimate-woocommerce-points-and-rewards');?></span>
                </div>
            </div>
            <div class="mwb-pr-popup-right-rewards">
              <div class="mwb-pr-popup-rewards-right-content">
              </div>
            </div>
          </li>
          <?php } 
          if($this->mwb_wpr_is_order_conversion_enabled()){ ?>
            <li>
              <div class="mwb-pr-popup-left-rewards">
                <div class="mwb-pr-popup-rewards-icon">
                  <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/shopping-cart.png';?>" alt="">
                </div>
                <div class="mwb-pr-popup-rewards-left-content">
                <?php esc_html_e('Place The Order and Earn Points','ultimate-woocommerce-points-and-rewards');?>
                  <span><?php esc_html_e('Earn ','ultimate-woocommerce-points-and-rewards'); echo $order_conversion_rate['Points']; esc_html_e(' Points on every ','ultimate-woocommerce-points-and-rewards');echo $this->mwb_wpr_value_return_in_currency($order_conversion_rate['Value']); esc_html_e(' spent','ultimate-woocommerce-points-and-rewards');?></span>
                </div>
              </div>
              <div class="mwb-pr-popup-right-rewards">
                <div class="mwb-pr-popup-rewards-right-content">
                </div>
              </div>
            </li>
          <?php } if($this->mwb_wpr_is_review_reward_enabled()) { ?>
          <li>
            <div class="mwb-pr-popup-left-rewards">
              <div class="mwb-pr-popup-rewards-icon">
                <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
              </div>
              <div class="mwb-pr-popup-rewards-left-content">
                <h6><?php esc_html_e('Write Review ','ultimate-woocommerce-points-and-rewards');?></h6>
                <span><?php esc_html_e('Write review and earn ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_get_reward_review(); esc_html_e(' Point','ultimate-woocommerce-points-and-rewards');?></span>
              </div>
            </div>
            <div class="mwb-pr-popup-right-rewards">
              <div class="mwb-pr-popup-rewards-right-content">
              <!--  <span>you need 350more poins</span> -->
              </div>
            </div>
          </li>
        <?php } if($this->mwb_wpr_check_referal_enable()) { ?>
        <li>
          <div class="mwb-pr-popup-left-rewards">
            <div class="mwb-pr-popup-rewards-icon">
              <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
            </div>
            <?php if(!$this->mwb_wpr_is_only_referral_purchase_enabled()){ ?>
            <div class="mwb-pr-popup-rewards-left-content">
              <h6><?php esc_html_e('Refer Someone','ultimate-woocommerce-points-and-rewards');?></h6>
              <span><?php esc_html_e('Reward is: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_get_referral_reward(); esc_html_e(' Point','ultimate-woocommerce-points-and-rewards');?></span>
            </div>
          <?php } else{ ?>
            <div class="mwb-pr-popup-rewards-left-content">
              <h6><?php esc_html_e('Refer Link','ultimate-woocommerce-points-and-rewards');?></h6>
              <span><?php esc_html_e('Share this link and get reward on their purchase only ','ultimate-woocommerce-points-and-rewards');?></span>
            </div>
          <?php }?>
          </div>
          <?php if(!$mwb_guest_user){ ?>
            <div class="mwb-pr-popup-right-rewards mwb-pr-popup-right-rewards--login">
              <div class="mwb-pr-popup-rewards-right-content">
                <span id="mwb_notify_user_copy"><?php echo $site_url.'?pkey='.$referral_link; ?></span>
              </div>
              <div class="mwb-pr-popup-rewards-right-content">
                <button class="mwb_wpr_btn_copy mwb_tooltip" data-clipboard-target="#mwb_notify_user_copy" aria-label="copied">
                <span class="mwb_tooltiptext"><?php esc_html_e('Copy','ultimate-woocommerce-points-and-rewards') ;?></span>
                <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/copy.png';?>" alt="Copy to clipboard"></button>
              </div>
            </div>
          <?php }?>
        </li>
        <?php } 
          if($this->mwb_wpr_is_referral_purchase_enabled()) { ?>
            <li>
            <div class="mwb-pr-popup-left-rewards">
              <div class="mwb-pr-popup-rewards-icon">
                <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
              </div>
              <div class="mwb-pr-popup-rewards-left-content">
                <h6><?php esc_html_e('Earn on Somenelse Purchasing ','ultimate-woocommerce-points-and-rewards');?></h6>
                <span><?php esc_html_e('Reward is: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_get_referral_purchase_reward(); esc_html_e(' Point','ultimate-woocommerce-points-and-rewards');?></span>
              </div>
            </div>
            <div class="mwb-pr-popup-right-rewards">
              <div class="mwb-pr-popup-rewards-right-content">
              <!--  <span>you need 350more poins</span> -->
              </div>
            </div>
          </li>
        <?php }
       if(!$this->mwb_wpr_check_signup_enable() && !$this->mwb_wpr_is_order_conversion_enabled() && !$this->mwb_wpr_is_review_reward_enabled() && !$this->mwb_wpr_is_referral_purchase_enabled() && !$this->mwb_wpr_check_referal_enable()){ ?>
          <li>
            <div class="mwb-pr-popup-left-rewards">
              <div class="mwb-pr-popup-rewards-icon">
              </div>
              <div class="mwb-pr-popup-rewards-left-content">
                <h6><?php esc_html_e('No Features Are Available Right Now! ','ultimate-woocommerce-points-and-rewards');?></h6>
              </div>
            </div>
            <div class="mwb-pr-popup-right-rewards">
              <div class="mwb-pr-popup-rewards-right-content">
              </div>
            </div>
          </li>
        <?php }
        ?>
        </ul>
      </div>
      <div class="mwb-pr-popup-tab-container" id="notify_user_redeem_section_mobile">
        <ul class="mwb-pr-popup-tab-listing">
          <?php if($this->mwb_wpr_is_purchase_product_using_points_enabled()) { ?>
          <li>
            <div class="mwb-pr-tab-popup-icon">
               <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/shopping-cart.png';?>" alt="">
            </div>
            <div class="mwb-pr-tab-popup-content">
              <h6><?php esc_html_e('Redeem Points on Particluar Products','ultimate-woocommerce-points-and-rewards');?></h6>
              <p><?php esc_html_e('Conversion Rule: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_return_conversion_rate($purchase_product_using_pnt_rate['Points'],$purchase_product_using_pnt_rate['Currency']);?></p>
            </div>
          </li>
          <?php } 
          if($this->mwb_wpr_is_apply_point_on_cart_enabled()){ ?>
            <li>
              <div class="mwb-pr-tab-popup-icon">
                <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/shopping-cart.png';?>" alt="image">
              </div>
              <div class="mwb-pr-tab-popup-content">
                <h6><?php esc_html_e('Apply Points on Cart Total','ultimate-woocommerce-points-and-rewards');?></h6>
                <p><?php esc_html_e('Conversion Rule: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_return_conversion_rate($apply_point_on_cart_rate['Points'],$apply_point_on_cart_rate['Currency']);?></p>
              </div>
            </li>
          <?php } 
          if($this->mwb_wpr_is_convert_points_to_coupon_enabled()) {?>
          <li>
            <div class="mwb-pr-tab-popup-icon">
              <img src="<?php echo ULTIMATE_WOOCOMMERCE_POINTS_AND_REWARDS_DIR_URL.'modal/images/voucher.png';?>" alt="">
            </div>
            <div class="mwb-pr-tab-popup-content">
              <h6><?php esc_html_e('Convert Points into Coupons','ultimate-woocommerce-points-and-rewards');?></h6>
              <p><?php esc_html_e('Conversion Rule: ','ultimate-woocommerce-points-and-rewards'); echo $this->mwb_wpr_return_conversion_rate($convert_points_to_coupons_rate['Points'], $convert_points_to_coupons_rate['Currency']); ?></p>
            </div>
          </li>
          <?php }
          if( !$this->mwb_wpr_is_convert_points_to_coupon_enabled() && !$this->mwb_wpr_is_apply_point_on_cart_enabled() && !$this->mwb_wpr_is_purchase_product_using_points_enabled() ){ ?>
            <li>
              <div class="mwb-pr-popup-left-rewards">
                <div class="mwb-pr-popup-rewards-icon">
                </div>
                <div class="mwb-pr-popup-rewards-left-content">
                  <h6><?php esc_html_e('No Features Are Available Right Now! ','ultimate-woocommerce-points-and-rewards');?></h6>
                </div>
              </div>
              <div class="mwb-pr-popup-right-rewards">
                <div class="mwb-pr-popup-rewards-right-content">
                </div>
              </div>
            </li>
          <?php }
          ?>
        </ul>
      </div>
    </div><!--mwb-pr-popup-body-->
  </div>
</div>
<span id="mwb-pr-mobile-open-popup" style="background-color: <?php echo $this->mwb_wpr_notification_addon_get_color(); ?>" class="mwb-pr-mobile-open-popup <?php echo $position_class;?>"><?php echo $button_text; ?></span>
<span id="mwb-pr-mobile-close-popup" style="background-color: <?php echo $this->mwb_wpr_notification_addon_get_color();?>" class="mwb-pr-mobile-open-popup close <?php echo $position_class;?>"><?php echo $button_text; ?></span>
<?php
}
